<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => '1',
            'name' => 'Super Admin',
            'username' => 'superadmin',
            'email' => 'superadmin@mail.com',
            'fake_name' => 'fakename',
            'gender' => 'Male',
            'title' => 'Chairman',
            'contact_email' => 'contact@mail.com',
            'contact_number' => '0123456785',
            'address' => 'address too long!!!',
            'whatsapp' => '0123456785',
            'facebook' => 'https://www.facebook.com',
            'linkedin' => 'https://www.linkedin.com',
            'skype' => 'skype.username',
            'password' => bcrypt('12345678'),
            'created_at' => now(),
            'updated_at' => now()
        ]);


        DB::table('users')->insert([
            'role_id' => '2',
            'name' => 'Administration',
            'username' => 'administration',
            'email' => 'administration@mail.com',
            'fake_name' => 'fakename',
            'gender' => 'Male',
            'title' => 'admin',
            'contact_email' => 'contact@mail.com',
            'contact_number' => '0123456785',
            'address' => 'address too long!!!',
            'whatsapp' => '0123456785',
            'facebook' => 'https://www.facebook.com',
            'linkedin' => 'https://www.linkedin.com',
            'skype' => 'skype.username',
            'password' => bcrypt('12345678'),
            'created_at' => now(),
            'updated_at' => now()
        ]);


        DB::table('users')->insert([
            'role_id' => '3',
            'name' => 'Finance',
            'username' => 'finance',
            'email' => 'finance@mail.com',
            'fake_name' => 'fakename',
            'gender' => 'Male',
            'title' => 'finance',
            'contact_email' => 'contact@mail.com',
            'contact_number' => '0123456785',
            'address' => 'address too long!!!',
            'whatsapp' => '0123456785',
            'facebook' => 'https://www.facebook.com',
            'linkedin' => 'https://www.linkedin.com',
            'skype' => 'skype.username',
            'password' => bcrypt('12345678'),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('users')->insert([
            'role_id' => '4',
            'name' => 'Chief Executive Campaigns Officer',
            'username' => 'ceco',
            'email' => 'ceco@mail.com',
            'fake_name' => 'fakename',
            'gender' => 'Male',
            'title' => 'ceco',
            'contact_email' => 'contact@mail.com',
            'contact_number' => '0123456785',
            'address' => 'address too long!!!',
            'whatsapp' => '0123456785',
            'facebook' => 'https://www.facebook.com',
            'linkedin' => 'https://www.linkedin.com',
            'skype' => 'skype.username',
            'password' => bcrypt('12345678'),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('users')->insert([
            'role_id' => '5',
            'name' => 'Campaign Manager',
            'username' => 'campm',
            'email' => 'campm@mail.com',
            'fake_name' => 'fakename',
            'gender' => 'Male',
            'title' => 'campm',
            'contact_email' => 'contact@mail.com',
            'contact_number' => '0123456785',
            'address' => 'address too long!!!',
            'whatsapp' => '0123456785',
            'facebook' => 'https://www.facebook.com',
            'linkedin' => 'https://www.linkedin.com',
            'skype' => 'skype.username',
            'password' => bcrypt('12345678'),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('users')->insert([
            'role_id' => '6',
            'name' => 'Designer',
            'username' => 'designer',
            'email' => 'designer@mail.com',
            'fake_name' => 'fakename',
            'gender' => 'Male',
            'title' => 'designer',
            'contact_email' => 'contact@mail.com',
            'contact_number' => '0123456785',
            'address' => 'address too long!!!',
            'whatsapp' => '0123456785',
            'facebook' => 'https://www.facebook.com',
            'linkedin' => 'https://www.linkedin.com',
            'skype' => 'skype.username',
            'password' => bcrypt('12345678'),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('users')->insert([
            'role_id' => '7',
            'name' => 'Content Writer',
            'username' => 'contw',
            'email' => 'contw@mail.com',
            'fake_name' => 'fakename',
            'gender' => 'Male',
            'title' => 'cnotw',
            'contact_email' => 'contact@mail.com',
            'contact_number' => '0123456785',
            'address' => 'address too long!!!',
            'whatsapp' => '0123456785',
            'facebook' => 'https://www.facebook.com',
            'linkedin' => 'https://www.linkedin.com',
            'skype' => 'skype.username',
            'password' => bcrypt('12345678'),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('users')->insert([
            'role_id' => '8',
            'name' => 'Posting Assistant',
            'username' => 'posta',
            'email' => 'posta@mail.com',
            'fake_name' => 'fakename',
            'gender' => 'Male',
            'title' => 'posta',
            'contact_email' => 'contact@mail.com',
            'contact_number' => '0123456785',
            'address' => 'address too long!!!',
            'whatsapp' => '0123456785',
            'facebook' => 'https://www.facebook.com',
            'linkedin' => 'https://www.linkedin.com',
            'skype' => 'skype.username',
            'password' => bcrypt('12345678'),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('users')->insert([
            'role_id' => '9',
            'name' => 'Guest Relation Manager',
            'username' => 'grm',
            'email' => 'grm@mail.com',
            'fake_name' => 'fakename',
            'gender' => 'Male',
            'title' => 'grm',
            'contact_email' => 'contact@mail.com',
            'contact_number' => '0123456785',
            'address' => 'address too long!!!',
            'whatsapp' => '0123456785',
            'facebook' => 'https://www.facebook.com',
            'linkedin' => 'https://www.linkedin.com',
            'skype' => 'skype.username',
            'password' => bcrypt('12345678'),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('users')->insert([
            'role_id' => '10',
            'name' => 'Client',
            'username' => 'client',
            'email' => 'client@mail.com',
            'fake_name' => 'fakename',
            'gender' => 'Male',
            'title' => 'client',
            'contact_email' => 'contact@mail.com',
            'contact_number' => '0123456785',
            'address' => 'address too long!!!',
            'whatsapp' => '0123456785',
            'facebook' => 'https://www.facebook.com',
            'linkedin' => 'https://www.linkedin.com',
            'skype' => 'skype.username',
            'password' => bcrypt('12345678'),
            'created_at' => now(),
            'updated_at' => now()
        ]);


    }
}

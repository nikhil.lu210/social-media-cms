<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('package_id')->unsigned();
            $table->foreign('package_id')->references('id')->on('packages')->onDelete('cascade');

            $table->bigInteger('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');


            $table->bigInteger('team_id')->unsigned()->nullable();
            $table->foreign('team_id')->references('id')->on('team_names')->onDelete('cascade');

            $table->string('type');
            $table->string('name');
            $table->string('email');
            $table->string('mobile', 15);
            $table->text('address');
            $table->string('website')->nullable();
            $table->string('moto')->nullable();
            $table->string('tags')->nullable();
            $table->string('currency',1);
            $table->text('description')->nullable();

            $table->tinyInteger('status')->default(1);

            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("ALTER TABLE businesses ADD logo MEDIUMBLOB after name");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');

        Schema::table("businesses", function ($table) {
            $table->dropSoftDeletes();
        });

    }
}

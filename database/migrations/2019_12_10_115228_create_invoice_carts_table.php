<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_carts', function (Blueprint $table) {
            $table->bigIncrements('id');

            //foreign key use from which invoice
            $table->bigInteger('invoice_id')->unsigned();
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');

            $table->string('item', 60);
            $table->integer('quantity');
            $table->double('price', 10, 2);
            $table->double('discount', 10, 2);
            $table->double('tax', 10, 2)->nullable();
            $table->text('note')->nullable();
            $table->double('total', 10, 2);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_carts');

        Schema::table("invoice_carts", function ($table) {
            $table->dropSoftDeletes();
        });

    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_authors', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('media_id')->unsigned();
            $table->foreign('media_id')->references('id')->on('social_media')->onDelete('cascade');

            $table->string('email');
            $table->string('password');
            $table->string('username')->nullable();
            $table->string('number', 15)->nullable();
            $table->text('note')->nullable();
            $table->tinyInteger('status')->default(1);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_authors');

        Schema::table("media_authors", function ($table) {
            $table->dropSoftDeletes();
        });

    }
}

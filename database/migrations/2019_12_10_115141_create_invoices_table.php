<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');

            //foreign key for client
            $table->bigInteger('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');

            //foreign key, provider_id, who provide the
            $table->bigInteger('provider_id')->unsigned();
            $table->foreign('provider_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('business_id')->unsigned()->nullable();
            $table->foreign('business_id')->references('id')->on('businesses')->onDelete('cascade');

            $table->string('invoice_number', 60);
            $table->date('invoice_date');
            $table->date('expire_date');
            // total amount without discount or advance
            $table->double('sub_total', 10,2);

            $table->string('currency', 1);
            //fixed or % discount type (fixed, percentage)
            $table->string('discount_type', 15)->nullable();
            $table->double('discount', 10, 2);
            $table->double('payable_amount', 10, 2);
            $table->double('total_paid', 10, 2)->nullable();
            $table->double('total_due', 10, 2)->nullable();
            $table->tinyInteger('invoice_status')->nullable();
            // invoice type (advance, due, full, ect )
            $table->string('invoice_type')->nullable();
            $table->text('client_note')->nullable();
            $table->text('term_condition')->nullable();
            $table->timestamp('seen_by_client')->nullable();


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');

        Schema::table("invoices", function ($table) {
            $table->dropSoftDeletes();
        });

    }
}

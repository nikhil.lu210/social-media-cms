<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('creator_id')->unsigned();
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('business_id')->unsigned();
            $table->foreign('business_id')->references('id')->on('businesses')->onDelete('cascade');

            $table->date('date');
            $table->text('post_caption');
            $table->text('tags');
            $table->text('designer_content');
            $table->text('designer_instruction');
            $table->text('note')->nullable();
            $table->tinyInteger('content_status')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('is_extra')->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');

        Schema::table("contents", function ($table) {
            $table->dropSoftDeletes();
        });

    }
}

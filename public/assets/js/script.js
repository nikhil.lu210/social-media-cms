$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});


$(document).ready(function(){
    $('.confirmation').confirm({
        type: 'dark',
        typeAnimated: true,
        title: 'Confirm!',
        content: 'Are You Sure...?',
        confirmButtonClass: 'btn-info',
        cancelButtonClass: 'btn-danger',
        confirmButton: 'Yes',
        cancelButton: 'NO',
        animation: 'zoom',
        closeAnimation: 'scale',
        confirm: function(){
            alert('Confirmed!');
        },
        cancel: function(){
            alert('Canceled!')
        }
    });
});

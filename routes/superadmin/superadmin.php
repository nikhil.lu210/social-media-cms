<?php

// superadmin Routes
Route::group([
    'prefix' => 'superadmin', // URL
    'as' => 'superadmin.', // Route
    'namespace' => 'SuperAdmin', // Controller
],
    function(){
        // Dashboard
        include_once 'dashboard/dashboard.php';

        // Profile
        include_once 'profile/profile.php';

        // Business
        include_once 'business/business.php';

        // Content
        include_once 'content/content.php';

        // Setting
        include_once 'setting/setting.php';

        // System
        include_once 'system/system.php';

        // client
        include_once 'client/client.php';

        // invoice
        include_once 'invoice/invoice.php';
    }
);

<?php

// Super Admin invoice Routes
Route::group([
    'prefix' => 'invoice', //URL
    'as' => 'invoice.', //Route
    'namespace' => 'Invoice', // Controller
],
    function(){
        Route::get('/all_invoice', 'InvoiceController@allInvoice')->name('all');
    }
);

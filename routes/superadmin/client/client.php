<?php

// Super Admin client Routes
Route::group([
    'prefix' => 'client', //URL
    'as' => 'client.', //Route
    'namespace' => 'Client', // Controller
],
    function(){
        Route::get('/all_client', 'ClientController@allClient')->name('all');

        Route::get('/active_client', 'ClientController@activeClient')->name('active');

        Route::get('/inactive_client', 'ClientController@inactiveClient')->name('inactive');

        Route::get('/left_client', 'ClientController@leftClient')->name('left');

        // Route::get('/all_client/show/{client_id}', 'ClientController@show')->name('show');

        // Route::get('/all_client/edit/{client_id}', 'ClientController@edit')->name('edit');
    }
);

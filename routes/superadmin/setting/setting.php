<?php

// Super Admin Setting Routes
Route::group([
    'prefix' => 'setting', //URL
    'as' => 'setting.', //Route
    'namespace' => 'Setting', // Controller
],
    function(){
        Route::get('/superadmin', 'SettingController@superadmin')->name('superadmin');

        Route::get('/administration', 'SettingController@administration')->name('administration');

        Route::get('/finance', 'SettingController@finance')->name('finance');

        Route::get('/ceco', 'SettingController@ceco')->name('ceco');

        Route::get('/campm', 'SettingController@campm')->name('campm');

        Route::get('/contw', 'SettingController@contw')->name('contw');

        Route::get('/designer', 'SettingController@designer')->name('designer');

        Route::get('/posta', 'SettingController@posta')->name('posta');

        Route::get('/grm', 'SettingController@grm')->name('grm');


        Route::post('/create_user/{role}', 'SettingController@createUser')->name('create_user');

        Route::get('/change_status/{user_id}', 'SettingController@changeStatus')->name('change_status');
    }
);

<?php

// Super Admin Profile Routes
Route::group([
    'prefix' => 'profile', //URL
    'as' => 'profile.', //Route
    'namespace' => 'Profile', // Controller
],
    function(){
        Route::get('/', 'ProfileController@index')->name('index');
    }
);

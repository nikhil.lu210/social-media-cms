<?php

// Super Admin Content Routes
Route::group([
    'prefix' => 'content', //URL
    'as' => 'content.', //Route
    'namespace' => 'Content', // Controller
],
    function(){
        Route::get('/all', 'ContentController@allContent')->name('all');

        Route::get('/written', 'ContentController@writtenContent')->name('written');

        Route::get('/awaiting_feedback', 'ContentController@awaitingFeedbackContent')->name('awaiting.feedback');

        Route::get('/in_progress', 'ContentController@inProgressContent')->name('progress');

        Route::get('/completed', 'ContentController@completedContent')->name('completed');

        Route::get('/create/new_content', 'ContentController@newContent')->name('new.content');

        Route::post('/create', 'ContentController@createContent')->name('create');

        Route::get('/show/{content_date}', 'ContentController@show')->name('show');

        Route::get('/details/{content_id}', 'ContentController@details')->name('details');
    }
);

<?php

// Super Admin Business Routes
Route::group([
    'prefix' => 'business', //URL
    'as' => 'business.', //Route
    'namespace' => 'Business', // Controller
],
    function(){
        Route::get('/all_business', 'BusinessController@allBusiness')->name('all');

        Route::get('/active_business', 'BusinessController@activeBusiness')->name('active');

        Route::get('/inactive_business', 'BusinessController@inactiveBusiness')->name('inactive');

        Route::get('/left_business', 'BusinessController@leftBusiness')->name('left');

        Route::get('/create', 'BusinessController@create')->name('create');

        // Route::post('/store', 'BusinessController@store')->name('store');

        Route::get('/all_business/show/{business_id}', 'BusinessController@show')->name('show');

        Route::get('/all_business/edit/{business_id}', 'BusinessController@edit')->name('edit');
    }
);

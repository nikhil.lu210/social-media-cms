<?php

// Super Admin System Routes
Route::group([
    'prefix' => 'system', //URL
    'as' => 'system.', //Route
    'namespace' => 'System', // Controller
],
    function(){
        /**
         * Savasaachi Team
         */
        Route::get('/team', 'TeamController@team')->name('team');

        Route::get('/team/change_status/{user_id}', 'TeamController@changeTeamStatus')->name('team.change_status');

        Route::post('/team', 'TeamController@createTeam')->name('team.create');

        Route::post('/team/add_member/{team_id}', 'TeamController@addTeamMember')->name('team.member.create');

        Route::get('/team/show_team/{team_id}', 'TeamController@showTeam')->name('team.show');

        Route::post('/team/update/{id}', 'TeamController@updateTeam')->name('team.update');

        Route::get('/team/update_employee_status/{user_id}/{team_id}', 'TeamController@updateEmployeeStatus')->name('team.update.employee_status');

        Route::get('/team/left_team/{business_id}', 'TeamController@leftFromBusiness')->name('team.business.left');

        Route::post('/team/add_team/{team_id}', 'TeamController@addTeamBusiness')->name('team.business.add');




        /**
         * Savasaachi Packages
         */
        Route::get('/package', 'PackageController@package')->name('package');

        Route::post('/package', 'PackageController@create')->name('package.create');

        Route::post('/package/update/{package_id}', 'PackageController@update')->name('package.update');

        Route::get('/package/show/{package_id}', 'PackageController@show')->name('package.show');

        Route::get('/package/show/change_status/{package_id}', 'PackageController@changeStatus')->name('package.change_status');




        /**
         * Social Medias
         */
        Route::get('/social', 'SocialController@social')->name('social');

        Route::post('/social', 'SocialController@create')->name('social.create');

        Route::post('/social/update', 'SocialController@update')->name('social.update');

        Route::get('/social/show/change_status/{social_id}', 'SocialController@changeStatus')->name('social.change_status');




        /**
         * Authors Vault
         */
        Route::get('/vault', 'VaultController@vault')->name('vault');




        /**
         * Message Settings
         */
        Route::get('/message', 'MessageController@message')->name('message');




        /**
         * Email Settings
         */
        Route::get('/email', 'EmailController@email')->name('email');
    }
);

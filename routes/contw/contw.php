<?php

// contw Routes
Route::group([
    'prefix' => 'contw', // URL
    'as' => 'contw.', // Route
    'namespace' => 'Contw', // Controller
],
    function(){
        // Dashboard
        include_once 'dashboard/dashboard.php';

        // Profile
        include_once 'profile/profile.php';
    }
);

<?php

// ceco Routes
Route::group([
    'prefix' => 'ceco', // URL
    'as' => 'ceco.', // Route
    'namespace' => 'Ceco', // Controller
],
    function(){
        // Dashboard
        include_once 'dashboard/dashboard.php';

        // Profile
        include_once 'profile/profile.php';
    }
);

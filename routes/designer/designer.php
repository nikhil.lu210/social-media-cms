<?php

// designer Routes
Route::group([
    'prefix' => 'designer', // URL
    'as' => 'designer.', // Route
    'namespace' => 'Designer', // Controller
],
    function(){
        // Dashboard
        include_once 'dashboard/dashboard.php';

        // Profile
        include_once 'profile/profile.php';
    }
);

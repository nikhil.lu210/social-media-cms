<?php

// posta Routes
Route::group([
    'prefix' => 'posta', // URL
    'as' => 'posta.', // Route
    'namespace' => 'Posta', // Controller
],
    function(){
        // Dashboard
        include_once 'dashboard/dashboard.php';

        // Profile
        include_once 'profile/profile.php';
    }
);

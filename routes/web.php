<?php

Auth::routes();

Route::get('/','HomeController@index');
Route::post('/loginwithuser', 'Auth\LoginController@loginWithEmailOrUsername')->name('loginWithUser');

/*===================================
===========< auth Routes >==========
all kind of role has been agreed in this route
===================================*/
Route::group(
    [
        'middleware' => ['auth'],
    ],
    function () {
        include_once 'common_auth/common_auth.php';
    }
);



/*===================================
===========< SuperAdmin Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'superadmin'],
    ],
    function () {
        include_once 'superadmin/superadmin.php';
    }
);


/*===================================
===========< Administration Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'administration'],
    ],
    function () {
        include_once 'administration/administration.php';
    }
);


/*===================================
===========< Finance Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'finance'],
    ],
    function () {
        include_once 'finance/finance.php';
    }
);




/*===================================
===========< ceco Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'ceco'],
    ],
    function () {
        include_once 'ceco/ceco.php';
    }
);


/*===================================
===========< campm Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'campm'],
    ],
    function () {
        include_once 'campm/campm.php';
    }
);


/*===================================
===========< designer Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'designer'],
    ],
    function () {
        include_once 'designer/designer.php';
    }
);


/*===================================
===========< contw Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'contw'],
    ],
    function () {
        include_once 'contw/contw.php';
    }
);



/*===================================
===========< posta Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'posta'],
    ],
    function () {
        include_once 'posta/posta.php';
    }
);




/*===================================
===========< grm Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'grm'],
    ],
    function () {
        include_once 'grm/grm.php';
    }
);


/*===================================
===========< Client Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'client'],
    ],
    function () {
        include_once 'client/client.php';
    }
);




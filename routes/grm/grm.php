<?php

// grm Routes
Route::group([
    'prefix' => 'grm', // URL
    'as' => 'grm.', // Route
    'namespace' => 'Grm', // Controller
],
    function(){
        // Dashboard
        include_once 'dashboard/dashboard.php';

        // Profile
        include_once 'profile/profile.php';
    }
);

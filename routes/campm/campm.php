<?php

// campm Routes
Route::group([
    'prefix' => 'campm', // URL
    'as' => 'campm.', // Route
    'namespace' => 'Campm', // Controller
],
    function(){
        // Dashboard
        include_once 'dashboard/dashboard.php';

        // Profile
        include_once 'profile/profile.php';
    }
);

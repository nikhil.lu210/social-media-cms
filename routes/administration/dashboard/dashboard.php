<?php

// Administration Dashboard Routes
Route::group([
    'prefix' => 'dashboard', //URL
    'as' => 'dashboard.', //Route
    'namespace' => 'Dashboard', // Controller
],
    function(){
        Route::get('/', 'DashboardController@index')->name('index');
    }
);

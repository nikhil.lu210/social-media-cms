<?php

// Contents Routes
Route::group([
    'prefix' => 'content', //URL
    'as' => 'content.', //Route
    'namespace' => 'Content', // Controller
],
    function(){
        Route::post('/store', 'ContentController@store')->name('store');

        Route::post('/update/{content_id}', 'ContentController@update')->name('update');

        Route::get('/change_status/{content_id}/{status}', 'ContentController@changeStatus')->name('change.status');

        Route::get('/get_all_content', 'ContentController@getAllContent')->name('get.content');
    }
);

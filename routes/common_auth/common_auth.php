<?php

// common Routes
Route::group([
    'prefix' => '{layout}', // URL
    'as' => 'common_auth.', // Route
    'namespace' => 'CommonAuth', // Controller
],
    function(){
        // Profile
        include_once 'profile/profile.php';

        // Business
        include_once 'business/business.php';

        // content
        include_once 'content/content.php';

        // client
        include_once 'client/client.php';

        // invoice
        include_once 'invoice/invoice.php';
    }
);

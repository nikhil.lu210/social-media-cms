<?php

// invoice Routes
Route::group([
    'prefix' => 'invoice', //URL
    'as' => 'invoice.', //Route
    'namespace' => 'Invoice', // Controller
],
    function(){
        Route::get('/show/{invoice_id}', 'InvoiceController@show')->name('show');

        Route::get('/create', 'InvoiceController@create')->name('create');

        Route::get('/download_pdf', 'InvoiceController@downloadInvoice')->name('download.pdf');

        Route::post('/store', 'InvoiceController@store')->name('store');

        Route::get('/change_status/{invoice_id}/{status}', 'InvoiceController@changeStatus')->name('change.status');
    }
);

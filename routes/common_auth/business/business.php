<?php

// Business Routes
Route::group([
    'prefix' => 'business', //URL
    'as' => 'business.', //Route
    'namespace' => 'Business', // Controller
],
    function(){
        // Route::get('/all_business', 'BusinessController@allBusiness')->name('all');

        // Route::get('/active_business', 'BusinessController@activeBusiness')->name('active');

        Route::get('/inactive_business/{business_id}/{status}', 'BusinessController@changeStatus')->name('change.status');

        // Route::get('/left_business', 'BusinessController@leftBusiness')->name('left');

        // Route::get('/create', 'BusinessController@create')->name('create');

        Route::post('/store', 'BusinessController@store')->name('store');

        Route::post('/update/{business_id}', 'BusinessController@update')->name('update');
    }
);

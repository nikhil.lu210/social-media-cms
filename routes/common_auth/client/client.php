<?php

// Business Routes
Route::group([
    'prefix' => 'client', //URL
    'as' => 'client.', //Route
    'namespace' => 'Client', // Controller
],
    function(){

        Route::get('/inactive_client/{client_id}/{status}', 'ClientController@changeStatus')->name('change.status');

        Route::post('/store', 'ClientController@store')->name('store');

        Route::post('/update/{client_id}', 'ClientController@update')->name('update');
    }
);

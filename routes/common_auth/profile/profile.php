<?php

// Profile Routes
Route::group([
    'prefix' => 'profile', //URL
    'as' => 'profile.', //Route
    'namespace' => 'Profile', // Controller
],
    function(){
        Route::post('/updatePassword', 'ProfileController@updatePassword')->name('update.password');
        Route::post('/updateProfile', 'ProfileController@updateProfile')->name('update.profile');
    }
);

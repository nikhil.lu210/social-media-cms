<?php

namespace App\Http\Controllers\Designer\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //designer dashboard index
    public function index(){
        return view('designer.dashboard.index');
    }
}

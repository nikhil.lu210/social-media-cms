<?php

namespace App\Http\Controllers\SuperAdmin\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Hash;

class SettingController extends Controller
{
    // ==============< Super Admin >===============
    public function superadmin()
    {
        $users = User::where('role_id', 1)->get();
        return view('superadmin.setting.superadmin', compact(['users']));
    }




    // ==============< Administrator >===============
    public function administration()
    {
        $users = User::where('role_id', 2)->get();
        return view('superadmin.setting.administration', compact(['users']));
    }




    // ==============< Finance >===============
    public function finance()
    {
        $users = User::where('role_id', 3)->get();
        return view('superadmin.setting.finance', compact(['users']));
    }




    // ==============< Chief Executive Campaign Officer >===============
    public function ceco()
    {
        $users = User::where('role_id', 4)->get();
        return view('superadmin.setting.ceco', compact(['users']));
    }




    // ==============< Campaign Manager >===============
    public function campm()
    {
        $users = User::where('role_id', 5)->get();
        return view('superadmin.setting.campm', compact(['users']));
    }




    // ==============< Graphics Designer >===============
    public function designer()
    {
        $users = User::where('role_id', 6)->get();
        return view('superadmin.setting.designer', compact(['users']));
    }




    // ==============< Content Writer >===============
    public function contw()
    {
        $users = User::where('role_id', 7)->get();
        return view('superadmin.setting.contw', compact(['users']));
    }




    // ==============< Posting Assistant >===============
    public function posta()
    {
        $users = User::where('role_id', 8)->get();
        return view('superadmin.setting.posta', compact(['users']));
    }




    // ==============< Guest Relation Manager >===============
    public function grm()
    {
        $users = User::where('role_id', 9)->get();
        return view('superadmin.setting.grm', compact(['users']));
    }



    public function createUser(Request $request, $role)
    {
        // dd($request);
        $this->validate($request, [
            'name'              => 'required | string | max:191',
            'email'             => 'required | string | max:60',
            'contact_number'    => 'required | string | max:20',
            'password'          => 'required | string | min:8',
        ]);
        // dd($request);

        $user = new User();


        if($role == 'superadmin'){
            $user->role_id = 1;
        } else if($role == 'administration'){
            $user->role_id = 2;
        } else if($role == 'finance'){
            $user->role_id = 3;
        } else if($role == 'ceco'){
            $user->role_id = 4;
        } else if($role == 'campm'){
            $user->role_id = 5;
        } else if($role == 'designer'){
            $user->role_id = 6;
        } else if($role == 'contw'){
            $user->role_id = 7;
        } else if($role == 'posta'){
            $user->role_id = 8;
        } else if($role == 'grm'){
            $user->role_id = 9;
        } else{
            //error flash message
            return redirect()->back();
        }
        // dd($user);

        $user->name                 =       $request->name;
        $user->email                =       $request->email;
        $user->contact_number       =       $request->contact_number;
        $user->password             =       Hash::make($request->password);


        if($request->send_mail == 'on'){
            # Mail to user...
        }

        //generate username
        $user->username = $this->usernameGenerate($request->name);

        // dd($user);

        $user->save();

        //confirmation message
        $txt = $role . " has been Created Successfully.";
        toast($txt, 'success');

        // Success Flash Message Here
        return redirect()->back();
    }


    /**
     * generate username unique
     * @var function
     */
    protected function usernameGenerate($name)
    {
        $name = str_replace(' ', '', $name);
        $temp = (strlen($name)>= 6) ?substr($name, 0, 6):$name;
        $username = "Sava_".(string)$temp;
        $username = Str::slug($username);
        $userRows  = User::where('username','LIKE',"$username%")->get();
        // dd($userRows);
        $countUser = count($userRows) + 1;
        // dd($countUser);

        return ($countUser > 1) ? "{$username}_{$countUser}" : "{$username}_1";
    }

    /**
     * generate user id unique
     * @var function
     */
    protected function userIdGenerate($role)
    {
        if($role == 'superadmin'){
            $tag = 'SA';
        } else if($role == 'admin'){
            $tag = 'A';
        } else if($role == 'finance'){
            $tag = 'F';
        } else if($role == 'project_manager'){
            $tag = 'PM';
        } else if($role == 'employee'){
            $tag = 'E';
        }

        $year = Carbon::now()->format('Y');
        $userid = Str::slug("VE-".$tag.'-'.$year);
        $user  = User::where('user_id_number', 'like', $userid.'%')->orderBy('id', 'desc')->first();
        if($user == null){
            $userid .='-0001';
        } else{
            $temp = explode('-', $user->user_id_number);
            $ab = (int)$temp[sizeof($temp)-1];
            ++$ab;
            if($ab < 10) $userid .='-000'. (string) $ab;
            else if($ab < 100) $userid .='-00'. (string) $ab;
            else if($ab < 1000) $userid .='-0'. (string) $ab;
            else $userid .='-'. (string) $ab;
        }

        return $userid;
    }




    public function changeStatus($user_id)
    {
        $user = User::find($user_id);

        $user->status = ($user->status == 1) ? 0:1;

        $user->save();

        //success flash message
        return redirect()->back();
    }
}

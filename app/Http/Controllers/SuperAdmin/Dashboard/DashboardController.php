<?php

namespace App\Http\Controllers\SuperAdmin\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //superadmin dashboard index
    public function index(){
        // toast('Success Toast', 'success')->autoclose(3000);
        // toast('Success Toast', 'error')->autoclose(3000);
        // alert()->html('Bhai','Hello','success')->autoclose(false);
        // toast('Your Post as been submited!','success','top-right');
        return view('superadmin.dashboard.index');
    }
}

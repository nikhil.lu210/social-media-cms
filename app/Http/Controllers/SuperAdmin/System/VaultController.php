<?php

namespace App\Http\Controllers\SuperAdmin\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VaultController extends Controller
{
    // ==============< Team >===============
    public function vault()
    {
        return view('superadmin.system.vault.index');
    }
}

<?php

namespace App\Http\Controllers\SuperAdmin\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// include models
use App\Models\Package\Package;

class PackageController extends Controller
{
    // ==============< Team >===============
    public function package()
    {
        $packages = Package::with('busniesses')->get();
        // dd($packages);
        return view('superadmin.system.package.index', compact(['packages']));
    }

    public function create(Request $request)
    {
        // dd($request);
        //validate here
        $this->validate($request, array(
            'name' => 'required | string | max:60',
        ));
        if($request->description != null){
            $this->validate($request, array(
                'description' => 'required | string',
            ));
        }

        $package = new Package();
        $package->name = $request->name;
        $package->description = $request->description;

        $package->status = (isset($request->status) && $request->status == 'on') ? 1:0;

        $package->save();

        //confirmation message
        toast('Package Has Been Created Succesfully.','success')->autoclose(5000);

        return redirect()->back();
    }

    public function show($package_id)
    {
        $package = Package::with('busniesses')->where('id', $package_id)->firstOrFail();
        return view('superadmin.system.package.show', compact('package'));
    }

    public function changeStatus($package_id){

        $package = Package::findOrFail($package_id);

        $prevStatus = $package->status;
        $package->status = ($package->status == 0) ? 1 : 0;
        $afterStatus = $package->status;

        $package->save();

        if($prevStatus != $afterStatus && $package->status == 0)
            toast('Package Has Been Deactivated.','error')->autoclose(5000);
        else
            toast('Package Has Been Activated.','success')->autoclose(5000);

        return redirect()->back();
    }

    /**
     * package update method
     */
    public function update(Request $request, $package_id)
    {
        //validation input fuilde
        $this->validate($request, array(
            'name' => 'required | string | max:60',
        ));
        if($request->description != null){
            $this->validate($request, array(
                'description' => 'required | string',
            ));
        }

        $package = Package::findOrFail($package_id);

        $package->name = $request->name;
        $package->description = $request->description;

        $package->status = (isset($request->status) && $request->status == 'on') ? 1:0;

        $package->save();

        //confirmation message

        toast('Your Package Has Been Updated Successfully','success')->autoclose(5000);

        return redirect()->back();
    }
}

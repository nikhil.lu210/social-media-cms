<?php

namespace App\Http\Controllers\SuperAdmin\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//import models
use App\Models\SocialMedia\SocialMedia;

class SocialController extends Controller
{
    // ==============< Team >===============
    public function social()
    {
        $socials = SocialMedia::all();
        return view('superadmin.system.social.index', compact(['socials']));
    }

    public function create(Request $request)
    {
        //validate input datas
        $this->validate($request, array(
            'name' => 'required | string | max:60',
            'url' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
        ));

        if($request->description){
            $this->validate($request, array(
                'description' => 'required | string',
            ));
        }

        $social = new SocialMedia();

        $social->name = $request->name;
        $social->url = $request->url;
        $social->description = $request->description;

        $social->status = (isset($request->status) && $request->status == 'on') ? 1:0;

        $social->save();

        //confirmation message
        toast('New Social Media Has Been Created Succesfully.','success')->autoclose(5000);

        return redirect()->back();
    }



    public function changeStatus($social_id){

        $social = SocialMedia::findOrFail($social_id);

        $prevStatus = $social->status;
        $social->status = ($social->status == 0) ? 1 : 0;
        $afterStatus = $social->status;

        $social->save();

        if($prevStatus != $afterStatus && $social->status == 0)
            toast('Social Media Has Been Deactivated.','error')->autoclose(5000);
        else
            toast('Social Media Has Been Activated.','success')->autoclose(5000);

        return redirect()->back();
    }

    /**
     * social update method
     */
    public function update(Request $request)
    {
        //validation input fuilde
        $this->validate($request, array(
            'name' => 'required | string | max:60',
            'url' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
        ));

        if($request->description){
            $this->validate($request, array(
                'description' => 'required | string',
            ));
        }

        $social = SocialMedia::findOrFail($request->social_id);

        $social->name = $request->name;
        $social->url = $request->url;
        $social->description = $request->description;

        $prevStatus = $social->status;
        $social->status = ($social->status == 0) ? 1 : 0;
        $afterStatus = $social->status;

        $social->save();

        if($prevStatus != $afterStatus && $social->status == 0){
            $msg = 'Social Media Has Been Updated Succesfully. But Has Been Deactivated.';
            $type = 'error';
        }else{
            $msg = 'Social Media Has Been Updated Succesfully. And Activated.';
            $type = 'success';
        }

        //confirmation message
        toast($msg, $type)->autoclose(5000);

        return redirect()->back();
    }
}

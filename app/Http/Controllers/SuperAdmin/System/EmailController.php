<?php

namespace App\Http\Controllers\SuperAdmin\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmailController extends Controller
{
    // ==============< Team >===============
    public function email()
    {
        return view('superadmin.system.email.index');
    }
}

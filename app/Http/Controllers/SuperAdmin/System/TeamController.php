<?php

namespace App\Http\Controllers\SuperAdmin\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

use App\Models\TeamName\TeamName;
use App\Models\Team\Team;
use App\Models\Business\Business;
use App\User;
use App\Role;

class TeamController extends Controller
{
    // ==============< Team >===============
    public function team()
    {
        $teamNames = TeamName::with('businesses')->get();
        // dd($teamNames);
        return view('superadmin.system.team.index', compact('teamNames'));
    }

    public function createTeam(Request $request)
    {
        $this->validate($request, array(
            'name'  => 'string | required | max:50'
        ));

        $teamName = new TeamName();

        $teamName->name = $request->name;
        $teamName->status = (isset($request->status) && $request->status == 'on') ? 1 : 0;

        $teamName->save();

        //confirmation message
        toast('New Team Has Been Created Succesfully.','success')->autoclose(5000);

        return redirect()->back();
    }

    public function changeTeamStatus($team_id)
    {
        $team = TeamName::find($team_id);



        $prevStatus = $team->status;
        $team->status = ($team->status == 1) ? 0 : 1;
        $afterStatus = $team->status;

        if($team->status == 0){
            $teams = Team::where('team_id', $team->id)->where('status', 1)->get();
            foreach($teams as $temp){
                $temp->status = 0;
                $temp->save();
            }
        }

        $team->save();

        if($prevStatus != $afterStatus && $team->status == 0)
            toast('Team Has Been Deactivated.','error')->autoclose(5000);
        else
            toast('Team Has Been Activated.','success')->autoclose(5000);

        return redirect()->back();
    }

    /**
     * New Team Member Add into Team
     */
    public function addTeamMember(Request $request, $team_id)
    {
        $member = Team::where('team_id', $team_id)->where('user_id', $request->user_id)->first();

        if($member == null){
            $member = new Team();

            $member->user_id = $request->user_id;
            $member->team_id = $team_id;

            $member->save();
        } else{
            $member->status = 1;
            $member->save();
        }

        toast('Team Member has been Assigned Succesfully.','success')->autoclose(5000);

        return redirect()->back();
    }

    public function showTeam($team_id)
    {
        $team = TeamName::with(['businesses.client', 'teams.user.role'])->where('id', $team_id)->firstOrFail();
        $disMembers = Team::where('team_id', $team->id)->where('status', 0)->count();
        $disBusinesses = Business::where('team_id', $team->id)->where('status', 0)->count();

        $allBusinesses = Business::select('name', 'id')->where('team_id', null)->where('status', 1)->get();

        $roles = Role::where('id', '>=', 5)->where('id', '<=', 9)->get();
        $tempEmployees = User::with('role')
                            ->where('role_id', '>=', '5')
                            ->where('role_id', '<=', '9')
                            ->where('status', 1)
                            ->select('id', 'role_id', 'name')
                            ->get();

        //filtering assigned employee
        $employees = collect([]);
        foreach($tempEmployees as $employee){
            $assigned = Team::where('user_id', $employee->id)->where('status', 1)->first();
            if($assigned == null)   $employees->push($employee);
        }
        // dd($employees);


        return view('superadmin.system.team.show', compact(['team', 'disMembers', 'disBusinesses', 'roles', 'employees', 'allBusinesses']));
    }


    public function updateTeam(Request $request, $id)
    {
        $this->validate($request, array(
            'name'  => 'string | required | max:50'
        ));

        $teamName = TeamName::find($id);

        $teamName->name = $request->name;
        $prevStatus = $teamName->status;
        $teamName->status = (isset($request->status) && $request->status == 'on') ? 1 : 0;
        $afterStatus = $teamName->status;


        if($teamName->status == 0){
            $teams = Team::where('team_id', $teamName->id)->where('status', 1)->get();
            foreach($teams as $temp){
                $temp->status = 0;
                $temp->save();
            }
        }

        // dd($teamName);
        $teamName->save();

        if($prevStatus != $afterStatus && $teamName->status == 0){
            $msg = 'Team Has Been Updated Succesfully. But Has Been Deactivated.';
            $type = 'error';
        }else{
            $msg = 'Team Has Been Updated Succesfully. And Activated.';
            $type = 'success';
        }

        //confirmation message
        toast($msg, $type)->autoclose(5000);

        return redirect()->back();
    }

    public function updateEmployeeStatus($user_id, $team_id)
    {
        $team = Team::where('team_id', $team_id)->where('user_id', $user_id)->firstOrFail();

        $prevStatus = $team->status;
        $team->status = ($team->status == 1) ? 0 : 1;
        $afterStatus = $team->status;

        $team->save();

        if($prevStatus != $afterStatus && $team->status == 0)
            toast('This Team Member Has Been Deactivated.','error')->autoclose(5000);
        else
            toast('This Team Member Has Been Activated.','success')->autoclose(5000);

        return redirect()->back();

    }

    /**
     * left from business method
     */

    public function leftFromBusiness($business_id){

        $business = Business::findOrFail($business_id);

        $business->team_id = null;

        $business->save();

        $msg = "Lefted From the \"".$business->name."\" Business";

        toast($msg, 'error')->autoclose(5000);

        return redirect()->back();
    }

    /**
     * add business in team
     */

    public function addTeamBusiness(Request $request, $team_id)
    {
        $business = Business::findOrFail($request->business_id);

        $business->team_id = $team_id;
        $business->save();

        $msg = "Added this Team to \"".$business->name."\" Business";
        toast($msg, 'success')->autoclose(5000);

        return redirect()->back();

    }

}

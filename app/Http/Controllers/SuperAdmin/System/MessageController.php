<?php

namespace App\Http\Controllers\SuperAdmin\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    // ==============< Team >===============
    public function message()
    {
        return view('superadmin.system.message.index');
    }
}

<?php

namespace App\Http\Controllers\SuperAdmin\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//import neccesary models
use App\User;

class ClientController extends Controller
{
    public function allClient(){
        $clients = User::where('role_id', 10)->get();
        return view('superadmin.client.all', compact(['clients']));
    }


    public function activeClient(){
        $clients = User::where('role_id', 10)->where('status', 1)->get();
        return view('superadmin.client.active', compact(['clients']));
    }


    public function inactiveClient(){
        $clients = User::where('role_id', 10)->where('status', 0)->get();
        return view('superadmin.client.inactive', compact(['clients']));
    }


    public function leftClient(){
        $clients = User::where('role_id', 10)->where('status', -1)->get();
        return view('superadmin.client.left', compact(['clients']));
    }
}

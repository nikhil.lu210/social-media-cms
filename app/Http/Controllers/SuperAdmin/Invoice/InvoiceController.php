<?php

namespace App\Http\Controllers\SuperAdmin\Invoice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Invoice\Invoice;
use App\Models\InvoiceCart\InvoiceCart;

class InvoiceController extends Controller
{
    public function allInvoice(){
        $invoices = Invoice::select('id', 'invoice_number', 'invoice_date', 'expire_date', 'payable_amount', 'invoice_status')->get();

        // dd($invoices);
        return view('superadmin.invoice.all', compact(['invoices']));
    }
}

<?php

namespace App\Http\Controllers\SuperAdmin\Content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//import neccesary Models
use App\Models\Content\Content;
use App\Models\Business\Business;
use DateTime;

class ContentController extends Controller
{
    public function allContent(){
        $allContents = Content::select('id', 'date')->get()->groupBy('date');
        return view('superadmin.content.all', compact(['allContents']));
    }

    public function writtenContent(){
        return view('superadmin.content.written');
    }

    public function awaitingFeedbackContent(){
        return view('superadmin.content.feedback');
    }

    public function inProgressContent(){
        return view('superadmin.content.progress');
    }

    public function completedContent(){
        return view('superadmin.content.completed');
    }

    public function newContent(){
        $businesses = Business::where('status', 1)->get();
        return view('superadmin.content.new_content', compact(['businesses']));
    }

    public function createContent(Request $request){
        $business = Business::findOrFail($request->business_id);
        $tmp = new DateTime($request->date);

        $date = $tmp->format('Y-m-d');

        $is_extra = (isset($request->is_extcontent_idra) && $request->is_extra == 'on') ? 1 : 0;
        return view('superadmin.content.create', compact(['business', 'date', 'is_extra']));
    }

    public function show($content_date){
        $contents = Content::with(['business', 'contentUpdates.updator', 'creator', 'contentFiles'])->where('date', $content_date)->orderBy('business_id')->get();


        if($contents->isEmpty()){
            // dd($contents);
            toast('Content Not Found On This Date!!!', 'error');
            return redirect()->route('superadmin.content.all');
        }
        return view('superadmin.content.show', compact(['contents', 'content_date']));
    }

    public function details($content_id){
        $content = Content::with(['business', 'contentUpdates.updator', 'creator', 'contentFiles'])
                            ->where('id', $content_id)
                            ->orderBy('business_id')
                            ->firstOrFail();

        return view('superadmin.content.details', compact(['content']));
    }
}

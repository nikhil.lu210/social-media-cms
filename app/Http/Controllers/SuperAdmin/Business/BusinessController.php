<?php

namespace App\Http\Controllers\SuperAdmin\Business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//import neccesary Models
use App\Models\SocialMedia\SocialMedia;
use App\Models\Business\Business;
use App\Models\Package\Package;
use App\Models\SocialVault\SocialVault;
use App\User;

use Session;

class BusinessController extends Controller
{
    //Superadmin Business
    public function allBusiness(){
        $businesses = Business::with(['client', 'package'])->get();
        return view('superadmin.business.all', compact(['businesses']));
    }

    public function activeBusiness(){

        $businesses = Business::with(['client', 'package'])->where('status', 1)->get();
        return view('superadmin.business.active', compact(['businesses']));
    }

    public function inactiveBusiness(){

        $businesses = Business::with(['client', 'package'])->where('status', 0)->get();
        return view('superadmin.business.inactive', compact(['businesses']));
    }

    public function leftBusiness(){

        $businesses = Business::with(['client', 'package'])->where('status', -1)->get();
        return view('superadmin.business.left', compact(['businesses']));
    }

    public function create(){

        $socials = SocialMedia::where('status', 1)->get();
        $packages = Package::where('status', 1)->get();
        $clients = User::where('role_id', 10)->where('status', 1)->get();
        return view('superadmin.business.create', compact(['socials', 'packages', 'clients']));
    }

    public function show($business_id){

        $business = Business::with(['client', 'package', 'contents.contentFiles', 'invoices','socialVaults.socialMedia'])->where('id', $business_id)->firstOrFail();

        return view('superadmin.business.show', compact(['business']));
    }

    public function edit($business_id){

        $business = Business::with(['socialVaults.socialMedia'])->where('id', $business_id)->firstOrFail();
        $socials = SocialMedia::where('status', 1)->get();
        $packages = Package::where('status', 1)->get();
        $clients = User::where('role_id', 10)->where('status', 1)->get();

        // dd($business);
        return view('superadmin.business.edit', compact(['business', 'socials', 'packages', 'clients']));
    }
}

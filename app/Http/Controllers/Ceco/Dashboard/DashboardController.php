<?php

namespace App\Http\Controllers\Ceco\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //ceco dashboard index
    public function index(){
        return view('ceco.dashboard.index');
    }
}

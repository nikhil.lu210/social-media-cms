<?php

namespace App\Http\Controllers\Campm\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //campm dashboard index
    public function index(){
        return view('campm.dashboard.index');
    }
}

<?php

namespace App\Http\Controllers\Contw\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //contw dashboard index
    public function index(){
        return view('contw.dashboard.index');
    }
}

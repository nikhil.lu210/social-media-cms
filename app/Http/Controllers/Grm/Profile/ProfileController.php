<?php

namespace App\Http\Controllers\Grm\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $profile = User::find(Auth::user()->id);
        // dd($profile);
        return view('grm.profile.index', compact(['profile']));
    }
}

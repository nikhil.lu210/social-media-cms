<?php

namespace App\Http\Controllers\Grm\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //grm dashboard index
    public function index(){
        return view('grm.dashboard.index');
    }
}

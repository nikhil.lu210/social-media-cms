<?php

namespace App\Http\Controllers\Posta\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //posta dashboard index
    public function index(){
        return view('posta.dashboard.index');
    }
}

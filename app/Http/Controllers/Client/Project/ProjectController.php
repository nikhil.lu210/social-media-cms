<?php

namespace App\Http\Controllers\Client\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    public function index()
    {
        return view('client.project.index');
    }

    public function create()
    {
        return view('client.project.create');
    }
    public function store(Request $request)
    {
        dd($request);
    }

    public function show()
    {
        return view('client.project.show');
    }
}

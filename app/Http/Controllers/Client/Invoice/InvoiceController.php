<?php

namespace App\Http\Controllers\Client\Invoice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{
    public function index()
    {
        return view('client.invoice.index');
    }

    public function show($invoice_id, $id)
    {
        return view('client.invoice.show');
    }
}

<?php

namespace App\Http\Controllers\CommonAuth\Invoice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Models\Invoice\Invoice;
use App\Models\InvoiceCart\InvoiceCart;

use Auth;
use DateTime;


class InvoiceController extends Controller
{

    public $role;

    public function __construct(){
        $this->role = ['superadmin', 'admin', 'finance', 'ceco', 'campm', 'designer', 'contw', 'posta', 'grm', 'client'];
    }


    public function show($layout, $invoice_id)
    {
        $invoice = Invoice::with(['client', 'invoiceCarts'])->where('id', $invoice_id)->firstOrFail();

        $pg_link = $this->role[(Auth::user()->role_id-1)].'.invoice.show';
        return view($pg_link, compact(['invoice']));
    }

    public function create($layout)
    {
        $clients = User::where('role_id', 10)->where('status', 1)->get();
        return view('common_auth.invoice.create', compact(['clients']));
    }

    public function downloadInvoice($layout)
    {
        dd($layout);

        $data = [
            'title' => 'First PDF for Medium',
            'heading' => 'Hello from 99Points.info',
            'content' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.'
              ];


          return $pdf->download('medium.pdf');
    }

    /**
     * store invoie method
     */
    public function store(Request $request, $layout){
        // dd($request);

        $this->validate($request, array(
            'client_id'         =>  'required',
            'invoice_number'    =>  'required',
            'expire_date'       =>  'required',
            "items"             =>  'required | array | min:1',
        ));

        if($request->recurring_days != null){
            $this->validate($request, array(
                'recurring_days'    =>  'required | integer | min:1 | max:365',
            ));
        }
        if($request->client_email != null){
            $this->validate($request, array(
                'recurring_days'    =>  'required | integer',
            ));
        }
        // dd($request);

        $invoice = new Invoice();

        $invoice->client_id = $request->client_id;
        $invoice->provider_id = Auth::user()->id;

        $today = Date('Y-m-d');
        $expire_date = new DateTime($request->expire_date);


        $invoice->invoice_number = $request->invoice_number;
        $invoice->invoice_date = $today;
        $invoice->expire_date = $expire_date->format('Y-m-d');

        $sub_total = 0;
        $sub_discount = 0;

        foreach($request->items as $item){
            $sub_total += $item['price'];
            $sub_discount += $item['discount'];
        }

        $invoice->sub_total = $sub_total;
        $invoice->currency = $request->currency;
        $invoice->discount = $sub_discount;
        $invoice->payable_amount = $sub_total - $sub_discount;

        $invoice->invoice_status = 0;
        $invoice->invoice_type = $request->submit;
        $invoice->client_note = $request->note;

        $invoice->save();

        $current_invoice = Invoice::select('id')
                            ->where('client_id', $request->client_id)
                            ->where('provider_id', $invoice->provider_id)
                            ->where('invoice_status', 0)
                            ->orderBy('id', 'DESC')
                            ->firstOrFail();

        foreach($request->items as $item){
            $cart = new InvoiceCart();

            $cart->invoice_id = $current_invoice->id;
            $cart->item = $item['item'];
            $cart->quantity = $item['quantity'];
            $cart->price = $item['price'];
            $cart->discount = $item['discount'];
            // $cart->tax = $item[''];
            // $cart->note = $item[''];
            $cart->total = $item['price'] - $item['discount'];

            $cart->save();
        }

        // dd($request, $invoice);


        //confirmation message
        toast('Invoice has been Created Successfully.', 'success');

        return redirect()->route('common_auth.invoice.show', ['layout' => $layout, 'invoice_id' => $current_invoice->id]);

    }



    /**
     * Change Invoice Status
     */
    public function changeStatus($layout, $invoice_id, $status)
    {
        $invoice = Invoice::findOrFail($invoice_id);

        $invoice->invoice_status = $status;

        $invoice->save();

        //confirmation message
        toast('Invoice Status Changed.', 'warning');

        return redirect()->back();

    }
}








<?php

namespace App\Http\Controllers\CommonAuth\Content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//import neccesary Models
use App\Models\Content\Content;
use App\Models\ContentUpdate\ContentUpdate;
use App\Models\Business\Business;
use Auth;

class ContentController extends Controller
{
    public $role;

    public function __construct(){
        $this->role = ['superadmin', 'admin', 'finance', 'ceco', 'campm', 'designer', 'contw', 'posta', 'grm', 'client'];
    }

    /**
     * Content STORE to Database
     */
    public function store(Request $request, $layout)
    {
        // dd(Auth::user());
        if($request->post_caption != null){
            $this->validate($request, array(
                'post_caption' => 'required | string',
            ));
        }

        if($request->designer_content != null){
            $this->validate($request, array(
                'designer_content' => 'required | string',
            ));
        }

        if($request->designer_instruction != null){
            $this->validate($request, array(
                'designer_instruction' => 'required | string',
            ));
        }

        if($request->note != null){
            $this->validate($request, array(
                'note' => 'required | string',
            ));
        }
        $this->validate($request, array(
            'date' => 'required',
            'business_id' => 'required'
        ));

        $business = Business::findOrFail($request->business_id);

        $content = new Content();

        $content->creator_id = Auth::user()->id;
        $content->business_id = $business->id;
        $content->date = $request->date;

        if($request->post_caption != null) $content->post_caption = $request->post_caption;

        if($request->tags != null) $content->tags = $request->tags;

        if($request->designer_content != null) $content->designer_content = $request->designer_content;

        if($request->designer_instruction != null) $content->designer_instruction = $request->designer_instruction;

        if($request->note != null) $content->note = $request->note;

        $content->content_status = 1;
        $content->status = 1;
        $content->is_extra = (isset($request->is_extra) && $request->is_extra == 'on') ? 1 : 0;
        $content->content_status = 1;

        $content->save();

        $txt = 'Content has been Created for "'.$business->name.'"';

        toast($txt, 'success');

        $route = $this->role[Auth::user()->role_id-1].'.content.all';

        return redirect()->route($route);
    }


    /**
     * Content UPDATE to Database
     */
    public function update(Request $request, $layout, $content_id)
    {
        // dd($request);

        if($request->post_caption != null){
            $this->validate($request, array(
                'post_caption' => 'required | string',
            ));
        }

        if($request->designer_content != null){
            $this->validate($request, array(
                'designer_content' => 'required | string',
            ));
        }

        if($request->designer_instruction != null){
            $this->validate($request, array(
                'designer_instruction' => 'required | string',
            ));
        }

        if($request->note != null){
            $this->validate($request, array(
                'note' => 'required | string',
            ));
        }
        $this->validate($request, array(
            'date' => 'required',
            'business_id' => 'required'
        ));

        $content = Content::findOrFail($content_id);

        if($request->post_caption != null) $content->post_caption = $request->post_caption;

        if($request->tags != null) $content->tags = $request->tags;

        if($request->designer_content != null) $content->designer_content = $request->designer_content;

        if($request->designer_instruction != null) $content->designer_instruction = $request->designer_instruction;

        if($request->note != null) $content->note = $request->note;

        $content->content_status = 1;
        $content->status = 1;
        $content->is_extra = (isset($request->is_extra) && $request->is_extra == 'on') ? 1 : 0;
        $content->content_status = 1;

        $content->save();

        $updator = new ContentUpdate();

        $updator->user_id = Auth::user()->id;
        $updator->content_id = $content_id;
        $updator->save();

        $txt = 'Content has been Updated for  "'.$content->business->name.'"';

        toast($txt, 'success');

        return redirect()->back();
    }


    /**
     * CHANGE STATUS Of a Content
     */
    public function changeStatus($layout, $content_id, $status)
    {
        dd($content_id, $status);
    }

    public function getAllContent($layout){
        $allContents = Content::with(['contentFiles'])->select('id', 'date', 'content_status', 'post_caption')->get()->groupBy('date');
        return response()->json($allContents);
    }
}

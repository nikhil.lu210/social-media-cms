<?php

namespace App\Http\Controllers\CommonAuth\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;
use App\User;

class ProfileController extends Controller
{

    // update password
    public function updatePassword(Request $request, $layout){

        $this->validate($request, [
            'old_password'          => 'required | string | min:8',
            'new_password'          => 'required | string | min:8 | same:confirm_password',
        ]);

        if( !Hash::check($request->old_password, Auth::user()->password) ){
            Session::flash('error', 'Your Old Password Does not Match.');
            return redirect()->back();
        }
        elseif($request->old_password == $request->new_password){
            Session::flash('error', 'Old Password and New Password must be Different.');
            return redirect()->back();
        }
        else{
            $super = Auth::user();

            $super->password = Hash::make($request->new_password);

            $super->save();

            //confirmation messege here
            Session::flash('success', 'Your Password has been Updated Succesfully.');

            return redirect()->back();
        }

    }

    /**
     * user profile update
     */
    public function updateProfile(Request $request, $layout)
    {
        // dd($request);
        // username validate
        $check = Auth::user();
        if($request->username != $check->username){
            $this->validate($request, array(
                "username" => 'required | string | unique:users',
            ));
        }
        //validate all input filed
        $this->validate($request, array(
            "name" => 'required | string',
            "gender" => 'required',
            "title" => 'required | string',
            "contact_email" => 'required | string',
            "contact_number" => 'required | string',
            "address" => 'required | string',
            "whatsapp" => 'required | string',
            "skype" => 'required | string',
        ));

        if(isset($request->facebook)){
            $this->validate($request, array(
                "facebook" => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            ));
        }

        if(isset($request->linkedin)){
            $this->validate($request, array(
                "linkedin" => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            ));
        }

        $user = User::find($check->id);

        $user->username = $request->username;
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->title = $request->title;
        $user->contact_email = $request->contact_email;
        $user->contact_number = $request->contact_number;
        $user->address = $request->address;
        $user->whatsapp = $request->whatsapp;
        $user->facebook = $request->facebook;
        $user->linkedin = $request->linkedin;
        $user->skype = $request->skype;


        if($request->hasFile('avatar')){

            $this->validate($request, array(
                "avatar" => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
            ));

            $image_data=file_get_contents($request->avatar);

            $encoded_image=base64_encode($image_data);

            $user->avatar = 'data:image/png;base64,'.$encoded_image;
        }

        // dd($user);
        $user->save();

        //confirmation messege here
        Session::flash('success', 'Your Profile has been Updated Succesfully.');

        return redirect()->back();
    }
}

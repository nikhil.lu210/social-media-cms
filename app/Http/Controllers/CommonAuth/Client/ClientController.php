<?php

namespace App\Http\Controllers\CommonAuth\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Hash;

class ClientController extends Controller
{
    public function store(Request $request, $layout)
    {
        // dd($request);
        $this->validate($request, [
            'name'              => 'required | string | max:191',
            'email'             => 'required | string | max:60',
            'contact_number'    => 'required | string | max:20',
            'password'          => 'required | string | min:8',
        ]);
        // dd($request);

        $user = new User();


        $user->role_id = 10;
        // dd($user);

        $user->name                 =       $request->name;
        $user->email                =       $request->email;
        $user->contact_number       =       $request->contact_number;
        $user->password             =       Hash::make($request->password);


        if($request->send_mail == 'on'){
            # Mail to user...
        }

        //generate username
        $user->username = $this->usernameGenerate($request->name);

        // dd($user);

        $user->save();

        //confirmation message
        toast('Client has been Created Successfully.', 'success');

        // Success Flash Message Here
        return redirect()->back();
    }

    /**
     * generate username unique
     * @var function
     */
    protected function usernameGenerate($name)
    {
        $name = str_replace(' ', '', $name);
        $username = Str::slug("Sava-".(strlen($name)>= 6) ?substr($name, 0, 6):$name);
        $userRows  = User::whereRaw("username REGEXP '^{$username}(-[0-9]*)?$'")->get();
        $countUser = count($userRows) + 1;

        return ($countUser > 1) ? "{$username}_{$countUser}" : $username;
    }


    public function changeStatus($layout, $client_id, $status)
    {
        // dd($client_id);
        $client = User::findOrFail($client_id);

        $prevStatus = $client->status;

        if($status == 1)
            $client->status = 1;
        elseif($status == -1)
            $client->status = -1;
        else
            $client->status = 0;

        // $client->status = ($client->status == 1) ? 0 : 1;
        $afterStatus = $client->status;

        $client->save();

        if($prevStatus != $afterStatus && $client->status == 0)
            toast('client has been Deactivated.', 'error');
        elseif($prevStatus != $afterStatus && $client->status == -1)
            toast('client has been Left From Savasaachi.', 'warning');
        else
            toast('client has been Ativated.', 'success');

        return redirect()->back();
    }
}

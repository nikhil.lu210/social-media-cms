<?php

namespace App\Http\Controllers\CommonAuth\Business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//import neccesary Models
use App\Models\SocialMedia\SocialMedia;
use App\Models\Business\Business;
use App\Models\Package\Package;
use App\Models\SocialVault\SocialVault;
use App\User;
use Auth;

class BusinessController extends Controller
{

    public $role;

    public function __construct(){
        $this->role = ['superadmin', 'admin', 'finance', 'ceco', 'campm', 'designer', 'contw', 'posta', 'grm', 'client'];
    }

    /**
     * Business Store to Database
     */
    public function store(Request $request, $layout){

        // dd($request);

        $this->validate($request, [
            'name'              => 'required | string | max:60',
            'type'              => 'required | string',
            'package_id'        => 'required',
            'currency'          => 'required',
            'client_id'         => 'required',
            'email'             => 'required | email',
            'mobile'            => 'required | string | max:15',
            'address'           => 'required | string'
        ]);

        $data = new Business();

        if($request->hasFile('logo')){

            $this->validate($request, array(
                "logo" => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            ));

            $image_data=file_get_contents($request->logo);

            $encoded_image=base64_encode($image_data);

            $data->logo = 'data:image/png;base64,'.$encoded_image;
        }

        $data->name  = $request->name;
        $data->type  = $request->type;
        $data->package_id  = $request->package_id;
        $data->currency  = $request->currency;
        $data->client_id  = $request->client_id;
        $data->email  = $request->email;
        $data->mobile  = $request->mobile;
        $data->address  = $request->address;
        $data->moto  = $request->moto;
        $data->tags  = $request->tags;
        $data->status  = (isset($request->status) && $request->status == 'on') ? 1:0;

        $data->save();

        $business = Business::orderBy('id', 'DESC')->firstOrFail();

        foreach($request->auth as $tmp){
            if($tmp['url'] == null) continue;
            $vault = new SocialVault();

            $vault->media_id = $tmp['type'];
            $vault->url = $tmp['url'];

            $vault->business_id = $business->id;

            if(isset($tmp['username']) && isset($tmp['password'])){
                if (filter_var($tmp['username'], FILTER_VALIDATE_EMAIL))
                    $vault->email = $tmp['username'];
                else
                    $vault->username = $tmp['username'];

                $vault->password = $tmp['password'];
            }

            // dd($vault);

            $vault->save();
        }

        //confirmation message
        toast('Business has been Created Successfully.', 'success');

        $page_id = Business::select('id')->orderBy('id', 'desc')->first()->id;
        $route = $this->role[Auth::user()->role_id-1].'.business.show';

        return redirect()->route($route,['business_id' => $page_id]);

    }


    /**
     * Business update to Database
     */
    public function update(Request $request, $layout, $business_id){

        // dd($request);

        $this->validate($request, [
            'name'              => 'required | string | max:60',
            'type'              => 'required | string',
            'package_id'        => 'required',
            'currency'          => 'required',
            'client_id'         => 'required',
            'email'             => 'required | email',
            'mobile'            => 'required | string | max:15',
            'address'           => 'required | string'
        ]);

        $data = Business::findOrFail($business_id);

        if($request->hasFile('logo')){

            $this->validate($request, array(
                "logo" => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            ));

            $image_data=file_get_contents($request->logo);

            $encoded_image=base64_encode($image_data);

            $data->logo = 'data:image/png;base64,'.$encoded_image;
        }

        $data->name  = $request->name;
        $data->type  = $request->type;
        $data->package_id  = $request->package_id;
        $data->currency  = $request->currency;
        $data->client_id  = $request->client_id;
        $data->email  = $request->email;
        $data->mobile  = $request->mobile;
        $data->address  = $request->address;
        $data->moto  = $request->moto;
        $data->tags  = $request->tags;
        $data->status  = (isset($request->status) && $request->status == 'on') ? 1:0;

        $data->save();
        // dd($request->auth);

        foreach($request->auth as $tmp){
            if($tmp['url'] == null) continue;

            $vault = SocialVault::where('media_id', $tmp['type'])->where('business_id', $data->id)->first();

            if(!isset($tmp['url']) && $vault == null) continue;

            else if(!isset($tmp['url']) && $vault){
                $vault->delete();
            } else {

                if($vault == null) $vault = new SocialVault();

                $vault->media_id = $tmp['type'];
                $vault->url = $tmp['url'];

                $vault->business_id = $data->id;

                if(isset($tmp['username']) && isset($tmp['password'])){
                    if (filter_var($tmp['username'], FILTER_VALIDATE_EMAIL))
                        $vault->email = $tmp['username'];
                    else
                        $vault->username = $tmp['username'];

                    $vault->password = $tmp['password'];
                }
            }

            // dd($vault);

            $vault->save();
        }

        //confirmation message
        toast('Business has been Updated Successfully.', 'success');

        $route = $this->role[Auth::user()->role_id-1].'.business.show';

        return redirect()->route($route,['business_id' => $data->id]);

    }


    /**
     * inactive Business method
     */
    public function changeStatus($layout, $business_id, $status)
    {
        // dd($business_id);
        $business = Business::findOrFail($business_id);

        $prevStatus = $business->status;

        if($status == 1)
            $business->status = 1;
        elseif($status == -1)
            $business->status = -1;
        else
            $business->status = 0;

        // $business->status = ($business->status == 1) ? 0 : 1;
        $afterStatus = $business->status;

        $business->save();

        if($prevStatus != $afterStatus && $business->status == 0)
            toast('Business has been Deactivated.', 'error');
        elseif($prevStatus != $afterStatus && $business->status == -1)
            toast('Business has been Left From Savasaachi.', 'warning');
        else
            toast('Business has been Ativated.', 'success');

        return redirect()->back();
    }
}

<?php

namespace App\Models\PaymentReport;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentReport extends Model
{
    //
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'payment_method', 'paid_amount', 'due_amount',
    ];



    /**
     * this function for client relations
     * @var function
     *
     */
    public function client(){
        return $this->belongsTo('App\User', 'client_id', 'id');
    }

    /**
     * this function for business relations
     * @var function
     *
     */
    public function business(){
        return $this->belongsTo('App\Models\Business\Business', 'business_id', 'id');
    }

    /**
     * this function for Invoice relations
     * @var function
     *
     */
    public function invoice(){
        return $this->hasOne('App\Models\Invoice\Invoice', 'invoice_id', 'id');
    }
}

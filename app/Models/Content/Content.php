<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    //
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'creator_id', 'business_id', 'date', 'post_caption', 'tags', 'designer_content', 'designer_instruction',
    ];


    /**
     * this function for creator relations
     * @var function
     *
     */
    public function creator(){
        return $this->belongsTo('App\User', 'creator_id', 'id');
    }

    /**
     * this function for business relations
     * @var function
     *
     */
    public function business(){
        return $this->belongsTo('App\Models\Business\Business', 'business_id', 'id');
    }

    /**
     * this function for contentUpdates relations
     * @var function
     *
     */
    public function contentUpdates(){
        return $this->hasMany('App\Models\ContentUpdate\ContentUpdate', 'content_id', 'id');
    }

    /**
     * this function for contentFile relations
     * @var function
     *
     */
    public function contentfiles(){
        return $this->hasMany('App\Models\ContentFile\ContentFile', 'content_id', 'id');
    }


}

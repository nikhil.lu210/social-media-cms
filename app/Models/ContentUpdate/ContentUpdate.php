<?php

namespace App\Models\ContentUpdate;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContentUpdate extends Model
{
    //
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'content_id',
    ];

    /**
     * this function for updator relations
     * @var function
     *
     */
    public function updator(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * this function for content relations
     * @var function
     *
     */
    public function content(){
        return $this->belongsTo('App\Models\Content\Content', 'content_id', 'id');
    }
}

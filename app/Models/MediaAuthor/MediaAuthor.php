<?php

namespace App\Models\MediaAuthor;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MediaAuthor extends Model
{
    //
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'media_id', 'email', 'password',
    ];

    /**
     * this function for social media relations
     * @var function
     *
     */
    public function socialMedia(){
        return $this->belongsTo('App\Models\SocialMedia\SocialMedia', 'media_id', 'id');
    }


}

<?php

namespace App\Models\Task;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    //
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'creator_id', 'business_id', 'description', 'task_status', 'deadline',
    ];

    /**
     * this function for creator relations
     * @var function
     *
     */
    public function creator(){
        return $this->belongsTo('App\User', 'creator_id', 'id');
    }

    /**
     * this function for business relations
     * @var function
     *
     */
    public function business(){
        return $this->belongsTo('App\Models\Business\Business', 'business_id', 'id');
    }

    /**
     * this function for task files relations
     * @var function
     *
     */
    public function files(){
        return $this->hasMany('App\Models\TaskFile\TaskFile', 'task_id', 'id');
    }

    /**
     * this function for task comments relations
     * @var function
     *
     */
    public function comments(){
        return $this->hasMany('App\Models\TaskComment\TaskComment', 'task_id', 'id');
    }


}

<?php

namespace App\Models\TeamName;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamName extends Model
{
    //
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * this function for team relations
     * @var function
     *
     */
    public function teams(){
        return $this->hasMany('App\Models\Team\Team', 'team_id', 'id');
    }

    /**
     * this function for business relations
     * @var function
     *
     */
    public function businesses(){
        return $this->hasMany('App\Models\Business\Business', 'team_id', 'id');
    }
}

<?php

namespace App\Models\TaskComment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskComment extends Model
{
    //
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'task_id', 'comment',
    ];


    /**
     * this function for user relations
     * @var function
     *
     */
    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * this function for Task relations
     * @var function
     *
     */
    public function task(){
        return $this->belongsTo('App\Models\Task\Task', 'task_id', 'id');
    }
}

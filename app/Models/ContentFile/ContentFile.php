<?php

namespace App\Models\ContentFile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContentFile extends Model
{
    //
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'content_id', 'file',
    ];

    /**
     * this function for user relations
     * @var function
     *
     */
    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }


    /**
     * this function for contentFile relations
     * @var function
     *
     */
    public function content(){
        return $this->belongsTo('App\Models\Content\Content', 'content_id', 'id');
    }
}

<?php

namespace App\Models\Invoice;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    //
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'provider_id', 'invoice_number', 'invoice_date', 'expire_date', 'tax', 'total', 'sub_total', 'currency', 'discount_type', 'discount', 'payable_amount',
    ];



    /**
     * this function for client relations
     * @var function
     *
     */
    public function client(){
        return $this->belongsTo('App\User', 'client_id', 'id');
    }


    /**
     * this function for provider relations
     * @var function
     *
     */
    public function provider(){
        return $this->belongsTo('App\User', 'provider_id', 'id');
    }

    /**
     * this function for business relations
     * @var function
     *]
     */
    public function business(){
        return $this->belongsTo('App\Models\Business\Business', 'business_id', 'id');
    }

    /**
     * this function for payment report relations
     * @var function
     *
     */
    public function paymentReport(){
        return $this->hasOne('App\Models\PaymentReport\PaymentReport', 'invoice_id', 'id');
    }

    /**
     * this function for invoice carts relations
     * @var function
     *
     */
    public function invoiceCarts(){
        return $this->hasMany('App\Models\InvoiceCart\InvoiceCart', 'invoice_id', 'id');
    }


}

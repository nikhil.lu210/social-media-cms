<?php

namespace App\Models\InvoiceCart;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceCart extends Model
{
    //
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'invoice_id', 'item', 'quantity', 'price', 'discount', 'tax', 'total',
    ];


    /**
     * this function for Invoice relations
     * @var function
     *
     */
    public function invoice(){
        return $this->belongsTo('App\Models\Invoice\Invoice', 'invoice_id', 'id');
    }
}

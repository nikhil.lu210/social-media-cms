<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Business extends Model
{
    //
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'package_id', 'client_id', 'type', 'name', 'email', 'mobile', 'address', 'currency',
    ];


    /**
     * this function for User relations
     * @var function
     *
     */
    public function client(){
        return $this->belongsTo('App\User', 'client_id', 'id');
    }

    /**
     * this function for package relations
     * @var function
     *
     */
    public function package(){
        return $this->belongsTo('App\Models\Package\Package', 'package_id', 'id');
    }

    /**
     * this function for assigned Employees relations
     * @var function
     *
     */
    public function assignedEmployees(){
        return $this->hasMany('App\Models\AssignedEmployee\AssignedEmployee', 'business_id', 'id');
    }

    /**
     * this function for socialVaults relations
     * @var function
     *
     */
    public function socialVaults(){
        return $this->hasMany('App\Models\SocialVault\SocialVault', 'business_id', 'id');
    }


    /**
     * this function for contents relations
     * @var function
     *
     */
    public function contents(){
        return $this->hasMany('App\Models\Content\Content', 'business_id', 'id');
    }


    /**
     * this function for tasks relations
     * @var function
     *
     */
    public function tasks(){
        return $this->hasMany('App\Models\Task\Task', 'business_id', 'id');
    }


    /**
     * this function for paymentReports relations
     * @var function
     *
     */
    public function paymentReports(){
        return $this->hasMany('App\Models\PaymentReport\PaymentReport', 'business_id', 'id');
    }

    /**
     * this function for invoices relations
     * @var function
     *
     */
    public function invoices(){
        return $this->hasMany('App\Models\Invoice\Invoice', 'business_id', 'id');
    }

    /**
     * this function for team relations
     * @var function
     *
     */
    public function teamName(){
        return $this->belongsTo('App\Models\TeamName\TeamName', 'team_id', 'id');
    }









}

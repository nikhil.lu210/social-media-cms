<?php

namespace App\Models\AssignedEmployee;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignedEmployee extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id', 'business_id',
    ];

    /**
     * this function for User relations
     * @var function
     *
     */
    public function employee(){
        return $this->belongsTo('App\User', 'employee_id', 'id');
    }

    /**
     * this function for User relations
     * @var function
     *
     */
    public function business(){
        return $this->belongsTo('App\Models\Business\Business', 'business_id', 'id');
    }


}

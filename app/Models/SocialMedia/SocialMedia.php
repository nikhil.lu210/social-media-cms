<?php

namespace App\Models\SocialMedia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SocialMedia extends Model
{
    //
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'url',
    ];


    /**
     * this function for social Vaults relations
     * @var function
     *
     */
    public function socialVaults(){
        return $this->hasMany('App\Models\SocialVault\SocialVault', 'media_id', 'id');
    }

    /**
     * this function for media authors relations
     * @var function
     *
     */
    public function mediaAuthors(){
        return $this->hasMany('App\Models\MediaAuthor\MediaAuthor', 'media_id', 'id');
    }
}

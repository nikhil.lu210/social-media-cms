<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    //
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'to_id', 'notifiable', 'notification',
    ];


    /**
     * this function for send_by for Project relations
     * @var function
     *
     */
    public function sendBy(){
        return $this->belongsTo('App\User', 'from_id', 'id');
    }

    /**
     * this function for received_by for Project relations
     * @var function
     *
     */
    public function receivedBy(){
        return $this->belongsTo('App\User', 'to_id', 'id');
    }

}

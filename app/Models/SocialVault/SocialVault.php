<?php

namespace App\Models\SocialVault;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SocialVault extends Model
{
    //
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'media_id', 'business_id', 'url',
    ];

    /**
     * this function for business relations
     * @var function
     *
     */
    public function business(){
        return $this->belongsTo('App\Models\Business\Business', 'business_id', 'id');
    }

    /**
     * this function for socialMedia relations
     * @var function
     *
     */
    public function socialMedia(){
        return $this->belongsTo('App\Models\SocialMedia\SocialMedia', 'media_id', 'id');
    }



}

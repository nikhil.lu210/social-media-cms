<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'contact_number',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //relation with role one to many relationship
    public function role(){
        return $this->belongsTo('App\Role', 'role_id', 'id');
    }

    //relation with team one to many relationship
    public function teams(){
        return $this->hasMany('App\Models\Team\Team', 'user_id', 'id');
    }

    //relation with business one to many relationship
    public function businesses(){
        return $this->hasMany('App\Models\Business\Business', 'client_id', 'id');
    }

    //relation with assigned_employee one to many relationship
    public function assignedEmployees(){
        return $this->hasMany('App\Models\AssignedEmployee\AssignedEmployee', 'employee_id', 'id');
    }

    //relation with content one to many relationship
    public function contents(){
        return $this->hasMany('App\Models\Content\Content', 'creator_id', 'id');
    }

    //relation with content File one to many relationship
    public function contentFiles(){
        return $this->hasMany('App\Models\ContentFile\ContentFile', 'user_id', 'id');
    }

    //relation with content_update one to many relationship
    public function contentUpdates(){
        return $this->hasMany('App\Models\ContentUpdate\ContentUpdate', 'user_id', 'id');
    }

    //relation with tasks one to many relationship
    public function tasks(){
        return $this->hasMany('App\Models\Task\Task', 'creator_id', 'id');
    }

    //relation with task_file one to many relationship
    public function taskFiles(){
        return $this->hasMany('App\Models\TaskFile\TaskFile', 'user_id', 'id');
    }

    //relation with task_comment one to many relationship
    public function taskComments(){
        return $this->hasMany('App\Models\TaskComment\TaskComment', 'user_id', 'id');
    }

    //relation with invoice one to many relationship
    public function clientInvoices(){
        return $this->hasMany('App\Models\Invoice\Invoice', 'client_id', 'id');
    }

    //relation with invoice one to many relationship
    public function providerInvoices(){
        return $this->hasMany('App\Models\Invoice\Invoice', 'provider_id', 'id');
    }

    //relation with paymentReport one to many relationship
    public function paymentReports(){
        return $this->hasMany('App\Models\PaymentReport\PaymentReport', 'client_id', 'id');
    }

    /**
     * this function for sender_notifications relations
     * (How many notification has been sent by this user.)
     * @var function
     *
     */
    public function senderNotifications(){
        return $this->hasMany('App\Models\Notification\Notification', 'from_id', 'id');
    }

    /**
     * this function for receiver_notifications relations
     * (How many notification has been received by this user.)
     * @var function
     *
     */
    public function receiverNotifications(){
        return $this->hasMany('App\Models\Notification\Notification', 'to_id', 'id');
    }

    /**
     * this function for sender_Messages relations
     * (How many Message has been sent by this user.)
     * @var function
     *
     */
    public function senderMessages(){
        return $this->hasMany('App\Models\Message\Message', 'from_id', 'id');
    }

    /**
     * this function for receiver_Messages relations
     * (How many Message has been received by this user.)
     * @var function
     *
     */
    public function receiverMessages(){
        return $this->hasMany('App\Models\Message\Message', 'to_id', 'id');
    }




}

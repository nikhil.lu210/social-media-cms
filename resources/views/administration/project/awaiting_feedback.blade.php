@extends('layouts.administration.app')

@section('page_title', 'Projects')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */

    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>
            Awaiting Feedback Projects <br>
            <span class="font-size-12">All the projects I was assigned & has been Completed but still now <strong class="text-danger">Waiting for Feedback.</strong></span>
        </h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-block">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-responsive table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">Sl.</th>
                                    <th>Project ID.</th>
                                    <th>Project Name</th>
                                    <th>Status</th>
                                    <th>Starting Date</th>
                                    <th>Ending Date</th>
                                    <th>Amount</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>01</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b><a href="{{ route('developer.project.show', ['client_id'=> 1, 'project_id' => 1]) }}" class="text-info">VT-PROJ000001</a></b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b><a href="{{ route('developer.project.show', ['client_id'=> 1, 'project_id' => 1]) }}" class="text-info">Project_Name</a></b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="relative mrg-top-15">
                                            <span class="status online"> </span>
                                            <span class="pdd-left-20 text-success"><b>Completed</b></span>
                                        </div>
                                        {{-- <div class="relative mrg-top-15">
                                            <span class="status away"> </span>
                                            <span class="pdd-left-20 text-warning"><b>Processing</b></span>
                                        </div>
                                        <div class="relative mrg-top-15">
                                            <span class="status no-disturb"> </span>
                                            <span class="pdd-left-20 text-danger"><b>Canceled</b></span>
                                        </div> --}}
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>6 Nov 2019</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>6 Dec 2019</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <b class="text-dark font-size-16">£168.00</b>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-10 dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('developer.project.show', ['client_id'=> 1, 'project_id' => 1]) }}">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>See Details</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a href="#" onclick="return confirm('If you Cancel This project, the payment you paid us, it will not be refund to you anymore. \n\n\nAre You Sure Want To Cancel This Project..?');">
                                                        <i class="ti-close pdd-right-10 text-danger"></i>
                                                        <span>Cancel Project</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>02</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b><a href="{{ route('developer.project.show', ['client_id'=> 1, 'project_id' => 1]) }}" class="text-info">VT-PROJ000002</a></b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b><a href="{{ route('developer.project.show', ['client_id'=> 1, 'project_id' => 1]) }}" class="text-info">Project_Name</a></b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        {{-- <div class="relative mrg-top-15">
                                            <span class="status online"> </span>
                                            <span class="pdd-left-20 text-success"><b>Completed</b></span>
                                        </div> --}}
                                        {{-- <div class="relative mrg-top-15">
                                            <span class="status away"> </span>
                                            <span class="pdd-left-20 text-warning"><b>Processing</b></span>
                                        </div> --}}
                                        <div class="relative mrg-top-15">
                                            <span class="status no-disturb"> </span>
                                            <span class="pdd-left-20 text-danger"><b>Canceled</b></span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>6 Nov 2019</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>6 Dec 2019</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <b class="text-dark font-size-16">£168.00</b>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-10 dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('developer.project.show', ['client_id'=> 1, 'project_id' => 1]) }}">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>See Details</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a href="#" onclick="return confirm('If you Cancel This project, the payment you paid us, it will not be refund to you anymore. \n\n\nAre You Sure Want To Cancel This Project..?');">
                                                        <i class="ti-close pdd-right-10 text-danger"></i>
                                                        <span>Cancel Project</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

    </script>
@endsection

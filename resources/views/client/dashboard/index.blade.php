@extends('layouts.client.app')

@section('page_title', 'Dashboard')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/fullcalendar/dist/fullcalendar.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */

    </style>
@endsection

@section('main_content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <div class="card">
                    <div class="card-block">
                        <p class="mrg-btm-5 text-center">Total Projects</p>
                        <h1 class="no-mrg-vertical font-size-35 text-center">10</h1>
                        <hr>
                        <div class="mrg-top-10">
                            <h2 class="no-mrg-btm">
                                <sup>06</sup>
                                <strong>/</strong>
                                <sub>10</sub>
                            </h2>
                            <span class="inline-block mrg-btm-10 font-size-13 text-semibold">Project Completed</span>
                            <span class="pull-right pdd-right-10 font-size-13">60%</span>
                            <div class="progress progress-info">
                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="card">
                    <div class="card-block">
                        <p class="mrg-btm-5 text-center">Total Deal</p>
                        <h1 class="no-mrg-vertical font-size-35 text-center">
                            $1000<b class="font-size-22">.00</b>
                        </h1>
                        <hr>
                        <div class="mrg-top-10">
                            <h2 class="no-mrg-btm">
                                <sup>$240<b class="font-size-10">.93</b></sup>
                                <strong>/</strong>
                                <sub>$1000<b class="font-size-10">.00</b></sub>
                            </h2>
                            <span class="inline-block mrg-btm-10 font-size-13 text-semibold">Total Paid</span>
                            <span class="pull-right pdd-right-10 font-size-13">24.09%</span>
                            <div class="progress progress-warning">
                                <div class="progress-bar" role="progressbar" aria-valuenow="24.09" aria-valuemin="0" aria-valuemax="100" style="width:24.09%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="card">
                    <div class="card-block">
                        <p class="mrg-btm-5 text-center">Total Websites</p>
                        <h1 class="no-mrg-vertical font-size-35 text-center">
                            07
                        </h1>
                        <hr>
                        <div class="mrg-top-10">
                            <h2 class="no-mrg-btm">
                                <sup>05</sup>
                                <strong>/</strong>
                                <sub>07</sub>
                            </h2>
                            <span class="inline-block mrg-btm-10 font-size-13 text-semibold">Live Websites</span>
                            <span class="pull-right pdd-right-10 font-size-13">71.43%</span>
                            <div class="progress progress-success">
                                <div class="progress-bar" role="progressbar" aria-valuenow="71.43" aria-valuemin="0" aria-valuemax="100" style="width:71.43%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="card">
                    <div class="card-block">
                        <p class="mrg-btm-5 text-center">Total Systems / Softwares</p>
                        <h1 class="no-mrg-vertical font-size-35 text-center">
                            03
                        </h1>
                        <hr>
                        <div class="mrg-top-10">
                            <h2 class="no-mrg-btm">
                                <sup>01</sup>
                                <strong>/</strong>
                                <sub>02</sub>
                            </h2>
                            <span class="inline-block mrg-btm-10 font-size-13 text-semibold">Total Completed</span>
                            <span class="pull-right pdd-right-10 font-size-13">66.67%</span>
                            <div class="progress progress-primary">
                                <div class="progress-bar" role="progressbar" aria-valuenow="66.67" aria-valuemin="0" aria-valuemax="100" style="width:66.67%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Calender --}}
        <div class="row">
            <div class="col-md-4">
                <div class="card calendar-event">
                    <div class="card-block overlay-dark bg" style="background: #333;">
                        <div class="text-center">
                            <h1 class="font-size-40 text-light mrg-btm-5 lh-1 text-warning"><b>Monthly Events</b></h1>
                            <h2 class="font-size-24 no-mrg-top"><b>November - 2019</b></h2>
                        </div>
                    </div>
                    <div class="card-block">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#calendar-add" class="add-event btn-primary">
                            <i class="ti-plus"></i>
                        </a>
                        <ul class="event-list">
                            <li class="event-items">
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#calendar-edit">
                                    <span class="bullet warning"></span>
                                    <span class="event-name">event_title</span>
                                    <div class="event-detail">
                                        <span>Lmet, consectetur adipisicing elit. Excepturi, blanditi...</span>
                                    </div>
                                </a>
                                <a href="#" class="remove" onclick="return confirm('Are You Sure Want To Delete...?');">
                                    <i class="ti-trash text-danger"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div id='full-calendar'></div>
            </div>
        </div>



        @include('client.dashboard.modals')
    </div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/fullcalendar/dist/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/fullcalendar/dist/gcal.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script src="{{ asset('assets/js/apps/calendar.js') }}"></script>
    <script>
        // Custom Script Here
    </script>
@endsection

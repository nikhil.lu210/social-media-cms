{{-- Add Calender Modal --}}
<div class="modal fade" id="calendar-add">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="border btm padding-15">
                <h4 class="no-mrg">Add New Event</h4>
            </div>
            <div class="modal-body">
                <form action="#" method="post">
                    @csrf
                    <div class="form-group">
                        <label>Event Title</label>
                        <input class="form-control" name="event_title" id="event_title" placeholder="Event Title" required>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Start Date</label>
                            <div class="timepicker-input input-icon form-group">
                                <i class="ti-calendar"></i>
                                <input type="text" name="start_date" id="start_date" class="form-control start-date" placeholder="mm/dd/yyyy" data-provide="datepicker" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>End Date</label>
                            <div class="timepicker-input input-icon form-group">
                                <i class="ti-calendar"></i>
                                <input type="text" name="end_date" id="end_date" class="form-control end-date" placeholder="mm/dd/yyyy" data-provide="datepicker">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Event Details</label>
                        <textarea class="form-control" name="event_details" id="event_details" placeholder="Event Details" required></textarea>
                    </div>
                    <div class="text-right">
                        <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-primary btn-sm" type="submit" >Add Event</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



{{-- Edit Calender Modal --}}
<div class="modal fade" id="calendar-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="border btm padding-15">
                <h4 class="no-mrg">Edit Event Details</h4>
            </div>
            <div class="modal-body">
                <form action="#" method="post">
                    @csrf
                    <div class="form-group">
                        <label>Event Title</label>
                        <input class="form-control" name="event_title" id="event_title" placeholder="Event Title" value="Event Title" required>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Start Date</label>
                            <div class="timepicker-input input-icon form-group">
                                <i class="ti-calendar"></i>
                                <input type="text" name="start_date" id="start_date" class="form-control start-date" placeholder="mm/dd/yyyy" value="11/25/2019" data-provide="datepicker" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>End Date</label>
                            <div class="timepicker-input input-icon form-group">
                                <i class="ti-calendar"></i>
                                <input type="text" name="end_date" id="end_date" class="form-control end-date" placeholder="mm/dd/yyyy" value="11/25/2019" data-provide="datepicker">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Event Details</label>
                        <textarea class="form-control" name="event_details" id="event_details" placeholder="Event Details" required>Event Details</textarea>
                    </div>
                    <div class="text-right">
                        <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-primary btn-sm" type="submit">Update Event</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

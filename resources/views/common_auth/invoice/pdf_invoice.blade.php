<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,JavaScript">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ $title }}</title>
</head>

<body style="font-size:16px;margin:0;font-family:Roboto,sans-serif;padding:0">
    <main>
        <!--=======================================Header Area=======================================-->
        <header>
            <div class="header" style="padding:10px 0 10px 0">
                <div class="container" style="width:1170px;margin:0 auto">
                    <div class="header_wrap" style="display:flex;justify-content:space-between;align-items: center">
                        <div class="logo">
                            <h2 style="font-size:30px;color:#83257d;font-weight:600; margin: 0">Logo</h2>
                        </div>
                        <div class="header_invoice">
                            <h5 style="font-size:30px;text-transform:uppercase;color:#515365; margin: 0">invoice: <span style="color: #83257d">vt-inv-000001</span></h5>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--=======================================About Area=======================================-->
        <section class="about" style="margin-top:30px">
            <div class="container" style="width:1170px;margin:0 auto">
                <div class="about_content">
                    <h4 style="color:#565c70;font-size:20px;padding: 0; margin: 0">AUS holidng LTD</h4>
                    <ul style="list-style:none;padding: 0; margin: 0; margin-top: 12px">
                        <li style="color:#6c6c6c;font-size:17px; margin-top:5px">20-22 Wenlock Road</li>
                        <li style="color:#6c6c6c;font-size:17px; margin-top:5px">London England, N1 7GU</li>
                        <li style="color:#6c6c6c;font-size:17px; margin-top:5px">Contact: 07542987483</li>
                    </ul>
                </div>
            </div>
        </section>

        <!--=======================================Invoice Area=======================================-->
        <section class="invoice_area" style="overflow:hidden">
            <div class="container" style="width:1170px;margin:0 auto">
                <div class="invoice_wrap" style="display:flex;justify-content:space-between;margin-top:40px;align-items:end">
                    <div class="single_incoivce">
                        <h3 style="color:#565c70;font-size:20px;margin:0;margin-bottom: 15px;">Invoice To:</h3>
                        <h5 style="color:#83257d;font-size:18px;font-weight:600;margin:0;">Mr. Client Name</h5>
                        <p style="color:#6c6c6c;font-size:15px; margin:0;">8626 Maiden Dr.</p>
                        <p style="color:#6c6c6c;font-size:15px; margin:0;">Niagara Fails, New York 14304</p>
                    </div>
                    <div class="incoivce_date">
                        <ul style="list-style:none">
                            <li style="margin-bottom:5px;text-transform:uppercase;font-weight:600;color:#444654c9;margin-right: 10px;">Invoice no:<span style="color:#83257d;margin-right:-2px; display: inline-block;float: right;">vt-inv-000001</span></li>
                            <li style="margin-bottom:5px;text-transform:uppercase;font-weight:600;color:#444654c9">Invoice Date:<span style="margin-left:34px;display:inline-block;float:right;font-weight:600">17/11/2019</span></li>
                            <li style="margin-bottom:5px;text-transform:uppercase;font-weight:600;color:#444654c9">Due Date:<span style="margin-left:34px;display:inline-block;float:right;font-weight:600">25/11/2019</span></li>
                        </ul>
                    </div>
                </div>
                <div class="about_table" style="margin-top:30px">
                    <table width="100%">
                        <thead>
                            <tr style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#444654;text-transform:capitalize">
                                <th style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#444654;text-transform:capitalize">SI</th>
                                <th style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#444654;text-transform:capitalize">Items</th>
                                <th style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#444654;text-transform:capitalize">Quantity</th>
                                <th style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#444654;text-transform:capitalize">price</th>
                                <th style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#444654;text-transform:capitalize">Discount</th>
                                <th style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#444654;text-transform:capitalize">total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#8c8d9b;text-transform:capitalize">01</td>
                                <td style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#8c8d9b;text-transform:capitalize">5 Page eCommerce Website</td>
                                <td style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#8c8d9b;text-transform:capitalize">1</td>
                                <td style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#8c8d9b;text-transform:capitalize">$450.00</td>
                                <td style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#8c8d9b;text-transform:capitalize">10%</td>
                                <td style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#8c8d9b;text-transform:capitalize">$450.00</td>
                            </tr>
                            <tr>
                                <td style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#8c8d9b;text-transform:capitalize">02</td>
                                <td style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#8c8d9b;text-transform:capitalize">5GB Hosting</td>
                                <td style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#8c8d9b;text-transform:capitalize">1</td>
                                <td style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#8c8d9b;text-transform:capitalize">$100.00</td>
                                <td style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#8c8d9b;text-transform:capitalize">0%</td>
                                <td style="border-top:1px solid #e6ecf5;text-align:left;padding:8px;color:#8c8d9b;text-transform:capitalize">$100.00</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="amount_tb" style="float:right;border:1px solid #e6ecf5;text-align:right;margin-top:27px;padding:12px 0;border-left:0;border-right:0">
                        <div class="footer_tb">
                            <div class="subTotal" style="border-bottom:1px dashed #e0e0e0">
                                <h5 style="color:#444654 !important;font-size:15px;padding:10px 0;font-weight:600; margin:0;">Sub Total: $450.00</h5>
                                <h5 style="color:#444654 !important;font-size:15px;padding:10px 0;font-weight:600; margin:0;">Discount: $45.00</h5>
                            </div>
                            <div class="subTotal" style="border-bottom:1px dashed #e0e0e0">
                                <h5 class="" style="font-size:15px;padding:10px 0;font-weight:600; margin:0;color: #83257D;">Total Amount To Pay: $450.00</h5>
                            </div>
                            <div class="subTotal" style="border-bottom:1px dashed #e0e0e0">
                                <h5 class="" style="font-size:15px;padding:10px 0;font-weight:600; margin:0; color:#7abc56;">Total Paid: $450.00</h5>
                            </div>
                            <div class="subTotal" style="border-bottom:0px">
                                <h5 class="" style="font-size:15px;padding:10px 0;font-weight:600;color:#e30a0a!important; margin:0;">Due Amount: $450.00</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="footer" style="padding:5px 0 50px 0;margin-top:20px">
            <div class="container " style="width:1170px;margin:0 auto;border-top:1px solid #e6ecf5">
                <div class="single_footer" style="display:flex;justify-content:space-between;align-items:center">
                    <div class="logo">
                        <h2>Logo</h2>
                        <img src="" alt="">
                    </div>
                    <div class="aside_footer" style="text-align:right">
                        <a style="color:#24a3f0;text-decoration:none;padding: 0; margin: 0; margin-top: 5px !important;display: block;" href="www.veechitechnologies.com" target="_blank">www.veechitechnologies.com</a>
                        <p style="color:#6b6c77;padding:0;margin-top:5px !important;margin:0">Phone: (123) 41340324</p>
                        <p style="color:#6b6c77;padding:0;margin-top:5px !important;margin:0">info@veechitechnologies.com</p>
                    </div>
                </div>

            </div>
        </section>
    </main>

</body>

</html>

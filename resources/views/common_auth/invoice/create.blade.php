@php
    $role = ['superadmin', 'admin', 'finance', 'ceco', 'campm', 'designer', 'contw', 'posta', 'grm', 'client'];
    $layout = 'layouts.'.$role[Auth::user()->role_id-1].'.app';
@endphp
@extends($layout)

@section('page_title', 'Invoices | Create')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .btn-xs {
            padding: 5px 6px 4px 6px;
        }
        .form-control{
            padding: 0.660rem 0.75rem;
        }
        .selectize-input {
            padding: 0.600rem 0.75rem;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
        .option.selected.active {
            background: #7e297f;
            color: #ffffff;
        }

        .form-wizard .nav-pills > li > a.active .step {
            background-color: #7e297f;
            border-color: #7e197f;
            color: #ffffff;
        }

        .button-next.disabled{
            display: none !important;
        }
        .inv-btn-grp{
            position: absolute;
            right: -30px;
            bottom: -97px;
        }
        .btn-group .btn-rounded{
            margin-left: 5px;
            margin-right: 5px;
            border-radius: 50px !important;
        }
        .btn-group .btn-default{
            border: 1px solid #d4deee !important;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>Create New Invoice</h4>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-block">
                    <div class="row relative">
                    <form id="invoiceFormCreate" class="width-100" action="{{ route('common_auth.invoice.store', ['layout' => 'superadmin']) }}" method="post">
                            @csrf
                            <div id="rootwizard" class="form-wizard col-md-10 ml-auto mr-auto">
                                <ul class="nav nav-pills nav-fill">
                                    <li class="nav-item">
                                        <a href="#invoiceTab" data-toggle="tab">
                                            <span class="step">1</span>
                                            <span class="title">Invoice Info</span>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="#customerTab" data-toggle="tab">
                                            <span class="step">2</span>
                                            <span class="title">Customer Info</span>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="#confirmationTab" data-toggle="tab">
                                            <span class="step">3</span>
                                            <span class="title">Confirmation</span>
                                        </a>
                                    </li>
                                </ul>
                                <div id="bar" class="progress progress-primary" style="width: 65%">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                </div>
                                <div class="tab-content">

                                    {{-- tab 1 costomer tab --}}
                                    <div id="invoiceTab" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-block p-b-0">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Select Currency <span class="required">*</span></label>
                                                                    <div class="mrg-top-0" id="invoice_currency_selector">
                                                                        <select class="@error('currency') is-invalid @enderror" name="currency" id="selectize-dropdown5" required>
                                                                            <option value="" disabled selected>Select Currency</option>
                                                                            <option value="1">£ GBP</option>
                                                                            <option value="2">$ USD</option>
                                                                            <option value="3">৳ BDT</option>
                                                                        </select>
                                                                    </div>

                                                                    @error('currency')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                    @enderror
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Invoice Number <span class="required">*</span></label>
                                                                    <input type="text" class="form-control" name="invoice_number" value="{{ old('invoice_number') }}" placeholder="SAVA-INV-000001" required>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Recurring Invoice <sup>(Day / Day's)</sup></label>
                                                                    <input type="number" class="form-control" name="recurring_days" value="{{ old('recurring_days') }}" placeholder="30" min="1" max="365" minlength="1" maxlength="3">

                                                                    <small>Input Days in <span class="text-primary text-bold">Number</span>.</small>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Expire Date <span class="required">*</span></label>
                                                                    <div class="timepicker-input input-icon form-group">
                                                                        <i class="ti-time m-t-5"></i>
                                                                        <input type="text" value="{{ old('expire_date') }}" name="expire_date" class="form-control datepicker-1" placeholder="mm/dd/yyyy" data-provide="datepicker" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="card">
                                                    <div class="card-heading border bottom">
                                                        <h4 class="card-title float-left p-t-3 text-uppercase text-bold">All Items</h4>

                                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#add_item" class="btn btn-primary btn-sm float-right m-b-0">Add Item</a>
                                                    </div>
                                                    <div class="card-block p-3 p-b-0">
                                                        <table class="table table-hover table-bordered product-table table-responsive m-b-0">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center">Item</th>
                                                                    <th class="text-center">Quantity</th>
                                                                    <th class="text-center">Price</th>
                                                                    <th class="text-center">Subtotal</th>
                                                                    <th class="text-center"><i class="ti-arrow-down text-danger"></i></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="item_body">
                                                                {{-- <tr class="id">
                                                                    <td>
                                                                        <div class="m-6 text-center">
                                                                            <span class="text-dark">
                                                                                <b>Lorem ipsum...</b>
                                                                            </span>
                                                                        </div>
                                                                        <input type="hidden" name="items[id][item]" value="item_name">
                                                                    </td>
                                                                    <td>
                                                                        <div class="m-6 text-center">
                                                                            <span class="text-dark">
                                                                                <b>01</b>
                                                                            </span>
                                                                            <input type="hidden" name="items[id][quantity]" value="item_quantity">
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="m-6 text-center">
                                                                            <span class="text-dark">
                                                                                <b>$250</b>
                                                                            </span>
                                                                        </div>
                                                                        <input type="hidden" name="items[id][price]" value="item_price">
                                                                    </td>
                                                                    <td>
                                                                        <div class="m-6 text-center">
                                                                            <span class="text-dark">
                                                                                <b>$200</b>
                                                                                <sup class="text-bold text-danger"><del>($250)</del></sup>
                                                                            </span>
                                                                        </div>
                                                                        <input type="hidden" name="items[id][discount]" value="item_discount">
                                                                    </td>
                                                                    <td>
                                                                        <div class="m-6 text-center">
                                                                            <a href="javascript:void(0);" class="m-0 text-dange delete_item"><i class="ti-close"></i></a>
                                                                        </div>
                                                                    </td>
                                                                </tr> --}}
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card">
                                                    <div class="card-block">
                                                        <h4 class="card-title mrg-btm-30 text-center text-uppercase text-bold">Summary</h4>
                                                        <div class="border bottom">
                                                            <input type="hidden" id="cart_total" value="0">
                                                            <input type="hidden" id="cart_discount" value="0">
                                                            <input type="hidden" id="cart_grand_total" value="0">
                                                            <p>Total Price: <span class="pull-right" id="show_total_price">00.00</span><span class="pull-right currency_icon">£</span></p>
                                                            <p class="mrg-top-20">Total Discount: <span class="pull-right" id="show_total_discount">00.00</span><span class="pull-right currency_icon">£</span></p>
                                                        </div>
                                                        <p class="mrg-top-20">Grand Total: <span class="pull-right text-dark font-size-18"><b class="currency_icon">£</b><b id="show_grand_price">00.00</b></span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- close tab 1 invoice tab --}}

                                    {{-- tab 2 costomer tab --}}
                                    <div id="customerTab" class="tab-pane fade">
                                        <div class="row">
                                            <div class="col-md-10 mr-auto ml-auto">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Select Customer <span class="required">*</span></label>
                                                            <div class="mrg-top-0">
                                                                <select name="client_id" id="selectize-dropdown" required>
                                                                    {{-- <option disabled>Select a Client</option> --}}
                                                                    <option selected disabled>Select The Invoice Client</option>
                                                                    @foreach($clients as $client)
                                                                    <option value="{{ $client->id }}">{{ $client->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Email</label>
                                                            <input type="email" class="form-control" name="client_email" value="{{ old('client_email') }}" placeholder="client@mail.com">
                                                            {{-- <small>sammllllllllllllll</small> --}}
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Mobile Number</label>
                                                            <input type="text" class="form-control" name="mobile_number" value="{{ old('mobile_number') }}" placeholder="+880 1712 145678">
                                                            {{-- <small>sammllllllllllllll</small> --}}
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Note</label>
                                                            <textarea maxlength="100" class="form-control" name="note" oninput="setNote(this);" rows="4" placeholder="Simple note for client...">{{ old('note') }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- close tab 2 customer tab --}}

                                    {{-- tab 3 confirm tab --}}
                                    <div id="confirmationTab" class="tab-pane fade">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="pdd-horizon-30">
                                                            <div class="mrg-top-15">
                                                                <div class="inline-block">
                                                                    <img class="img-responsive" src="{{ asset('assets/images/logo/logo.png') }}" alt="">
                                                                    <address class="pdd-left-10 mrg-top-20">
                                                                        <b class="text-dark">AUS holdings LTD</b><br>
                                                                        <span>20-22 Wenlock Road</span><br>
                                                                        <span>London, England, N1 7GU</span><br>
                                                                        <span><span class="text-bold">Contact:</span> 0754 2987 483</span><br>
                                                                    </address>
                                                                </div>
                                                                <div class="pull-right">
                                                                    <h2>
                                                                        <b>
                                                                            INVOICE:
                                                                            <a href="#" class="text-primary">VT-INV-000001</a>
                                                                        </b>
                                                                    </h2>
                                                                </div>
                                                            </div>
                                                            <div class="row mrg-top-20">
                                                                <div class="col-md-9 col-sm-9">
                                                                    <h3 class="pdd-left-10 mrg-top-10">Invoice To:</h3>
                                                                    <address class="pdd-left-10 mrg-top-10">
                                                                        <b class="text-dark">
                                                                            <a href="#" class="customer-name text-primary">Mr. Client Name</a>
                                                                        </b><br>
                                                                        <span>8626 Maiden Dr. </span><br>
                                                                        <span>Niagara Falls, New York 14304</span>
                                                                    </address>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3">
                                                                    <div class="mrg-top-50">
                                                                        <div class="text-dark text-uppercase inline-block"><b>Invoice No :</b></div>
                                                                        <b><a href="#" class="pull-right text-primary">VT-INV-000001</a></b>
                                                                    </div>
                                                                    <div class="mrg-top-0">
                                                                        <div class="text-dark text-uppercase inline-block">
                                                                            <b>Invoice Date :</b>
                                                                        </div>
                                                                        <div class="pull-right">17/11/2019</div>
                                                                    </div>
                                                                    <div class="mrg-top-0">
                                                                        <div class="text-dark text-uppercase inline-block">
                                                                            <b>Due Date :</b>
                                                                        </div>
                                                                        <div class="pull-right">25/11/2019</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row mrg-top-20">
                                                                <div class="col-md-12">
                                                                    <table class="table table-hover table-responsive">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Items</th>
                                                                                <th>Quantity</th>
                                                                                <th>Price</th>
                                                                                <th>Discount</th>
                                                                                <th class="text-right">Total</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="invoice_item">
                                                                            {{-- <tr>
                                                                                <td>5 Page eCommerce Website</td>
                                                                                <td>1</td>
                                                                                <td>£450.00</td>
                                                                                <td>10%</td>
                                                                                <td class="text-right">£405.00</td>
                                                                            </tr> --}}
                                                                        </tbody>
                                                                    </table>
                                                                    <div class="row mrg-top-30">
                                                                        <div class="col-md-4 offset-md-8">
                                                                            <hr>
                                                                            <table class="table table-borderless table-responsive">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <th class="text-right"><b>Sub Total:</b></th>
                                                                                        <td class="text-right"><b class="currency_icon">£</b><b id="invoice_sub_total">00.00</b></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th class="text-right"><b>Discount:</b></th>
                                                                                        <td class="text-right"><b class="currency_icon">£</b><b id="invoice_total_discount">00.00</b></td>
                                                                                    </tr>
                                                                                    <tr class="text-primary">
                                                                                        <th class="text-right border-top-1">
                                                                                            <b>Total Amount To Pay:</b>
                                                                                        </th>
                                                                                        <td class="text-right border-top-1"><b class="currency_icon">£</b><b id="invoice_grand_total">505.00</b></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <hr>
                                                                        </div>
                                                                    </div>

                                                                    {{-- Payment Method --}}
                                                                    <div class="row mrg-top-30">
                                                                        <div class="col-md-6 border right">
                                                                            <hr>
                                                                            <div class="pdd-vertical-20">
                                                                                <p class="text-dark">
                                                                                    [[ <b class="text-info">NOTE:</b>
                                                                                    <b>If you choose <span class="text-primary text-capitalize">Online Payment</span> then use the Links Below...</b> ]]
                                                                                </p>

                                                                                <div class="row">
                                                                                    <div class="col-xl-12 text-center">
                                                                                        <ul class="list-unstyled list-inline">
                                                                                            <li class="list-inline-item no-pdd-horizon">
                                                                                                <a href="javascript:void(0);" class="btn btn-default btn-icon btn-sm m-10">
                                                                                                    <b class="text-bold text-uppercase">Stripe</b>
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="list-inline-item no-pdd-horizon">
                                                                                                <a href="javascript:void(0);" class="btn btn-default btn-icon btn-sm m-10">
                                                                                                    <b class="text-bold text-uppercase">Visa</b>
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="list-inline-item no-pdd-horizon">
                                                                                                <a href="javascript:void(0);" class="btn btn-default btn-icon btn-sm m-10">
                                                                                                    <b class="text-bold text-uppercase">Master</b>
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="list-inline-item no-pdd-horizon">
                                                                                                <a href="javascript:void(0);" class="btn btn-default btn-icon btn-sm m-10">
                                                                                                    <b class="text-bold text-uppercase">bKash</b>
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="list-inline-item no-pdd-horizon">
                                                                                                <a href="javascript:void(0);" class="btn btn-default btn-icon btn-sm m-10">
                                                                                                    <b class="text-bold text-uppercase">DBBL</b>
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="list-inline-item no-pdd-horizon">
                                                                                                <a href="javascript:void(0);" class="btn btn-default btn-icon btn-sm m-10">
                                                                                                    <b class="text-bold text-uppercase">Rocket</b>
                                                                                                </a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 text-right border left">
                                                                            <hr>
                                                                            <div class="pdd-vertical-20">
                                                                                <p class="text-dark text-left">
                                                                                    [[ <b class="text-info">NOTE:</b>
                                                                                    <b>If you choose <span class="text-primary text-capitalize">bank payment</span> then use the bank details below. <span id="client_note"></span></b> ]]
                                                                                </p>

                                                                                <address class="pdd-left-10 mrg-top-10">
                                                                                    <b class="text-dark">AUS holdings LTD</b><br>
                                                                                    <span class="text-dark">Bank Of Scotland</span><br>
                                                                                    <span class="text-primary"><span class="text-dark">SC:</span> 80-22-60</span><br>
                                                                                    <span class="text-primary"><span class="text-dark">Account:</span> 18003761</span><br>
                                                                                    <span class="text-primary"><span class="text-dark">Ref:</span> VT-INV-000001</span>
                                                                                </address>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mrg-vertical-20 border top">
                                                                        <div class="col-md-6">
                                                                            <img class="img-responsive text-opacity mrg-top-0" width="150" src="{{ asset('assets/images/logo/logo.png') }}" alt="">
                                                                        </div>
                                                                        <div class="col-md-6 text-right">
                                                                            <small>
                                                                                <b>
                                                                                    <a href="https://www.veechitechnologies.com" target="_blank">www.veechitechnologies.com</a>
                                                                                </b>
                                                                            </small><br>
                                                                            <small><b>Phone:</b> (123) 456-7890</small>
                                                                            <br>
                                                                            <small>info@veechitechnologies.com</small>
                                                                        </div>
                                                                    </div>
                                                                    <div class="btn-group inv-btn-grp">
                                                                        <input type="submit" name="submit" class="btn btn-default button-draft pull-right btn-rounded border right" value="Save To Draft">

                                                                        <input type="submit" name="submit" class="btn btn-primary button-submit pull-right btn-rounded" value="Send Invoice"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- close tab 3 confirm tab --}}

                                    <div class="wizard-pager">
                                        <div class="">
                                            <button type="button" class="btn btn-default button-previous btn-rounded">Previous</button>
                                            <button type="button" class="btn btn-primary button-next pull-right btn-rounded">Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('common_auth.invoice.modals.add_item')
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>

    {{-- <script src="{{ asset('assets/js/forms/form-wizard.js') }}"></script> --}}
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        (function ($) {
            'use strict';

            var $validator = $("#invoiceFormCreate").validate({
                rules: {
                    invoice_number: {
                        required: true,
                        // minlength: 3
                    },
                    recurring_days: {
                        // required: true,
                        min: 1,
                        max:365
                    },
                    expire_date: {
                        required: true,
                        date: true
                    },
                    client_id: {
                        required: true
                    },
                    client_email: {
                        email: true
                    },
                    // mobile_number: {
                    //     required: true
                    // },
                }
            });

            function validationChecking() {
                var $valid = $('#invoiceFormCreate').valid();
                if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                }
            }

            $('#rootwizard').bootstrapWizard({
                tabClass: '',
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onNext: validationChecking,
                onLast: validationChecking,
                onTabClick: validationChecking,
                onTabShow: function(tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index+0;
                    var $percent = ($current/$total) * 133;
                    $('#rootwizard .progress-bar').css({width:$percent+'%'});
                }});
        })(jQuery);
    </script>

    {{-- custom data binding --}}
    <script>

        $("#add_item_from").submit(function(event){
            event.preventDefault(); //event prevent

            //get value from input field
            var item_name = $('#new_item_name').val();
            var quantity = $('#new_quantity').val();
            var price = $('#new_price').val();
            var discount = $('#new_discount').val();

            //set null value in this fields
            $('#new_item_name').val(null);
            $('#new_price').val(null);
            $('#new_discount').val(null);
            $('#new_quantity').val(null);

            //close modal
            $("#add_item").modal("hide");

            // set date in view field and hidden input field
            setItem(item_name, quantity, price, discount);
        });


        /*
        / function for delte item
        */
        function deleteItem(that){
            var select_item = $(that);
            var select_item_class = '.'+ select_item.parent().parent().parent().attr('class');

            var select_tr = $(select_item_class);

            var price = select_tr.find(".hidden_price").val();
            var discount = select_tr.find(".hidden_discount").val();
            deleteValue(price, discount);

            select_tr.remove();
        }




        /*
        / unique id generator method
        */
        function generateID() {
            // Math.random should be unique because of its seeding algorithm.
            // Convert it to base 36 (numbers + letters), and grab the first 9 characters
            // after the decimal.
            return '_' + Math.random().toString(36).substr(2, 9);
        };

        function setItem(item_name, quantity, price, discount){

            // call method for taking new unique id
            var id = generateID();

            var dis_str = "";
            if(discount != 0) dis_str = "<sup class='text-bold text-danger'><del>(<span class='currency_icon'>£</span>"+price+")</del></sup>";

            var str = "<tr class='"+id+"'><td><div class='m-6 text-center'><span class='text-dark'><b>"+item_name+"</b></span></div><input type='hidden' name='items["+id+"][item]' value='"+item_name+"'></td><td><div class='m-6 text-center'><span class='text-dark'><b>"+quantity+"</b></span><input type='hidden' name='items["+id+"][quantity]' value='"+quantity+"'></div></td><td><div class='m-6 text-center'><span class='text-dark'><b class='currency_icon'>$</b><b>"+price+"</b></span></div><input type='hidden' class='hidden_price' name='items["+id+"][price]' value='"+price+"'></td><td><div class='m-6 text-center'><span class='text-dark'><b class='currency_icon'>$"+(price-discount)+"</b>"+dis_str+"</span></div><input type='hidden' name='items["+id+"][discount]' value='"+discount+"' class='hidden_discount'></td><td><div class='m-6 text-center'><a href='javascript:void(0);' class='m-0 text-dange' onclick='deleteItem(this);'><i class='ti-close'></i></a></div></td></tr>";

            $('#item_body').append(str);

            var inv_str = "<tr class='"+id+"'><td>"+item_name+"</td><td>"+quantity+"</td><td><span class='currency_icon'>£</span>"+price+"</td><td>"+discount+"</td><td class='text-right'><span class='currency_icon'>£</span>"+(price-discount)+"</td></tr>";
            $('#invoice_item').append(inv_str);


            addCartValue(price, discount);
        }

        //change cart value function
        function addCartValue(price, discount)
        {
            var total_price = $("#cart_total");
            var total_discount = $("#cart_discount");
            var total_grand_price = $("#cart_grand_total");

            total_price.val(Number(total_price.val()) + Number(price));
            total_discount.val(Number(total_discount.val()) + Number(discount));

            total_grand_price.val((Number(total_price.val()) - Number(total_discount.val())));


            $("#show_total_price").html(total_price.val());
            $("#show_total_discount").html(total_discount.val());
            $("#show_grand_price").html(total_grand_price.val());


            $("#invoice_sub_total").html(total_price.val());
            $("#invoice_total_discount").html(total_discount.val());
            $("#invoice_grand_total").html(total_grand_price.val());

        }

        //delete from cart value
        function deleteValue(price, discount)
        {
            var total_price = $("#cart_total");
            var total_discount = $("#cart_discount");
            var total_grand_price = $("#cart_grand_total");

            total_price.val(Number(total_price.val()) - Number(price));
            total_discount.val(Number(total_discount.val()) - Number(discount));

            total_grand_price.val((Number(total_price.val()) - Number(total_discount.val())));


            $("#show_total_price").html(total_price.val());
            $("#show_total_discount").html(total_discount.val());
            $("#show_grand_price").html(total_grand_price.val());


            $("#invoice_sub_total").html(total_price.val());
            $("#invoice_total_discount").html(total_discount.val());
            $("#invoice_grand_total").html(total_grand_price.val());
        }


        $("#invoice_currency_selector select").change(function(){
            var value = $(this).val();

            if(value == 1){
                $(".currency_icon").html("£");
            } else if(value == 2){
                $(".currency_icon").html("$");
            } else{
                $(".currency_icon").html("৳");
            }
        });

        function setNote(that){
            var field = $(that);

            var value = field.val();

            $('#client_note').html(value);
            $('#checkcheck ').html(value);
        }

    </script>
@endsection

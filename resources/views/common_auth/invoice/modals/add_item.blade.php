{{-- Upload File Modal --}}
<div class="modal fade" id="add_item">
    <form action="" method="get" id="add_item_from">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="pull-left"><b><span id="social_name">Add New Item</span></b></h4>
                    <button class="btn btn-default btn-icon btn-rounded p-l-8 p-r-7 p-t-6 p-b-4" data-dismiss="modal">
                        <i class="ti-close"></i>
                    </button>
                </div>
                <div class="modal-body media-details-modal">
                    <div class="form-group">
                        <label class="text-bold">Item <span class="required">*</span></label>
                        <input autocomplete="off" type="text" id="new_item_name" name="item_name" class="form-control" placeholder="Facebook Boost" required>
                    </div>

                    <div class="form-group">
                        <label class="text-bold">Quantity <span class="required">*</span></label>
                        <input autocomplete="off" type="number" id="new_quantity" min="0" name="new_quantity" class="form-control" placeholder="02" required>
                    </div>

                    <div class="form-group">
                        <label class="text-bold">Total Price <span class="required">*</span></label>
                        <input autocomplete="off" type="number" step="0.1" min="0" id="new_price" name="price" class="form-control" placeholder="500" required>
                    </div>

                    <div class="form-group">
                        <label class="text-bold">Total Discount</label>
                        <input autocomplete="off" type="number" step="0.1" min="0" value="0" id="new_discount" name="discount" class="form-control" placeholder="100">
                    </div>
                </div>
                <div class="modal-footer border top">
                    <ul class="list-unstyled list-inline pull-right">
                        <li class="list-inline-item">
                            <button type="submit" id="new_item_submit" class="btn btn-primary btn-sm text-bold">
                                <i class="ti-save"></i>
                                Add Item
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>

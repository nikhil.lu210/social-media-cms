<!-- Side Nav START -->
<div class="side-nav">
    <div class="side-nav-inner">
        <div class="side-nav-logo">
            <a href="{{ route('client.dashboard.index') }}">
                <div class="logo logo-dark" style="background-image: url('{{ asset('assets/images/logo/logo.png') }}')"></div>
                <div class="logo logo-white" style="background-image: url('{{ asset('assets/images/logo/logo-white.png') }}')"></div>
            </a>
            <div class="mobile-toggle side-nav-toggle">
                <a href="#"><i class="ti-arrow-circle-left"></i></a>
            </div>
        </div>
        <ul class="side-nav-menu scrollable">
            <li class="nav-item {{ Request::is('client/dashboard*') ? 'active' : '' }}">
                <a class="mrg-top-30" href="{{ route('client.dashboard.index') }}">
                    <span class="icon-holder"><i class="ti-home"></i></span>
                    <span class="title">Dashboard</span>
                </a>
            </li>

            <li class="nav-item {{ Request::is('client/invoice*') ? 'active' : '' }}">
                <a href="{{ route('client.invoice.index') }}">
                    <span class="icon-holder"><i class="ti-notepad"></i></span>
                    <span class="title">Invoices</span>
                </a>
            </li>

            <li class="nav-item dropdown {{ Request::is('client/project*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder"><i class="ti-vector"></i></span>
                    <span class="title">My Projects</span>
                    <span class="arrow"><i class="ti-angle-right"></i></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('client/project/all_projects*') ? 'active' : '' }}">
                        <a href="{{ route('client.project.index') }}">My All Projects</a>
                    </li>
                    <li class="{{ Request::is('client/project/apply_new_project*') ? 'active' : '' }}">
                        <a href="{{ route('client.project.create') }}">Apply New Project</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item dropdown {{ Request::is('client/support*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder"><i class="ti-headphone-alt"></i></span>
                    <span class="title">Supports</span>
                    <span class="arrow"><i class="ti-angle-right"></i></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="nav-item {{ Request::is('client/support/faq*') ? 'active' : '' }}">
                        <a href="{{ route('client.support.faq.index') }}">
                            <span>FAQ</span>
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('client/support/contact*') ? 'active' : '' }}">
                        <a href="{{ route('client.support.contact.index') }}">
                            <span>Contact</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- Side Nav END -->

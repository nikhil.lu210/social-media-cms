<div class="modal fade" id="developer_modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="padding-15">
                    <div class="row">
                        <div class="ml-auto col-12 text-center">
                            <h1 class="text-dark text-bold p-b-20 m-b-30 border bottom">PiNikk</h1>
                        </div>
                        <div class="ml-auto col-md-6 text-center border right">
                            <img class="img-fluid mrg-horizon-auto p-b-20" src="{{ asset('assets\images\others\img-10.jpg') }}" alt="" width="50%">
                            <h1 class="text-dark text-uppercase text-bold m-b-0">Pias Das</h1>
                            <p class="text-semibold">Laravel Developer</p>
                            <div class="p-10 p-b-5 border top">
                                <div class="m-t-5 text-center">
                                    <ul class="list-unstyled list-inline">
                                        <li class="list-inline-item no-pdd-horizon">
                                            <a href="#" target="_blank" class="btn btn-default btn-icon btn-sm">
                                                <b class="text-bold"><i class="fa fa-github"></i></b>
                                            </a>
                                        </li>
                                        <li class="list-inline-item no-pdd-horizon">
                                            <a href="#" target="_blank" class="btn btn-default btn-icon btn-sm">
                                                <b class="text-bold"><i class="fa fa-linkedin"></i></b>
                                            </a>
                                        </li>
                                        <li class="list-inline-item no-pdd-horizon">
                                            <a href="#" target="_blank" class="btn btn-default btn-icon btn-sm">
                                                <b class="text-bold"><i class="fa fa-facebook"></i></b>
                                            </a>
                                        </li>
                                        <li class="list-inline-item no-pdd-horizon">
                                            <a href="#" target="_blank" class="btn btn-default btn-icon btn-sm">
                                                <b class="text-bold"><i class="fa fa-envelope-o"></i></b>
                                            </a>
                                        </li>
                                        <li class="list-inline-item no-pdd-horizon">
                                            <a href="#" target="_blank" class="btn btn-default btn-icon btn-sm">
                                                <b class="text-bold"><i class="fa fa-whatsapp"></i></b>
                                            </a>
                                        </li>
                                        <li class="list-inline-item no-pdd-horizon">
                                            <a href="#" target="_blank" class="btn btn-default btn-icon btn-sm">
                                                <b class="text-bold"><i class="fa fa-phone"></i></b>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="border top"></div>
                        </div>
                        <div class="ml-auto col-md-6 text-center">
                            <img class="img-fluid mrg-horizon-auto p-b-20" src="{{ asset('assets\images\others\img-10.jpg') }}" alt="" width="50%">
                            <h1 class="text-dark text-uppercase text-bold m-b-0">Nikhil Kurmi</h1>
                            <p class="text-semibold">Laravel Developer</p>
                            <div class="p-10 p-b-5 border top">
                                <div class="m-t-5 text-center">
                                    <ul class="list-unstyled list-inline">
                                        <li class="list-inline-item no-pdd-horizon">
                                            <a href="#" target="_blank" class="btn btn-default btn-icon btn-sm">
                                                <b class="text-bold"><i class="fa fa-github"></i></b>
                                            </a>
                                        </li>
                                        <li class="list-inline-item no-pdd-horizon">
                                            <a href="#" target="_blank" class="btn btn-default btn-icon btn-sm">
                                                <b class="text-bold"><i class="fa fa-linkedin"></i></b>
                                            </a>
                                        </li>
                                        <li class="list-inline-item no-pdd-horizon">
                                            <a href="#" target="_blank" class="btn btn-default btn-icon btn-sm">
                                                <b class="text-bold"><i class="fa fa-facebook"></i></b>
                                            </a>
                                        </li>
                                        <li class="list-inline-item no-pdd-horizon">
                                            <a href="#" target="_blank" class="btn btn-default btn-icon btn-sm">
                                                <b class="text-bold"><i class="fa fa-envelope-o"></i></b>
                                            </a>
                                        </li>
                                        <li class="list-inline-item no-pdd-horizon">
                                            <a href="#" target="_blank" class="btn btn-default btn-icon btn-sm">
                                                <b class="text-bold"><i class="fa fa-whatsapp"></i></b>
                                            </a>
                                        </li>
                                        <li class="list-inline-item no-pdd-horizon">
                                            <a href="#" target="_blank" class="btn btn-default btn-icon btn-sm">
                                                <b class="text-bold"><i class="fa fa-phone"></i></b>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="border top"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
    {{-- CSRF Token--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--  Page Title  --}}
    <title> SUPER ADMIN | @yield('page_title') </title>


    @include('layouts.superadmin.partials.stylesheet')
</head>

<body>
    <div class="app">
    {{-- <div class="app side-nav-dark header-dark"> --}}
        <div class="layout">
            {{-- Side Nav --}}
            @include('layouts.superadmin.partials.sidenav')

            {{-- Page Container START --}}
            <div class="page-container">
                {{-- Top Nav --}}
                @include('layouts.superadmin.partials.topnav')

                {{-- Main Content --}}
                <div class="main-content">
                    @yield('main_content')
                </div>

                {{-- Footer --}}
                @include('layouts.superadmin.partials.footer')
            </div>
            {{-- Page Container END --}}
        </div>
    </div>

    {{-- Sweet Alert --}}
    @include('sweetalert::alert')

    @include('layouts.superadmin.partials.scripts')

</body>
</html>

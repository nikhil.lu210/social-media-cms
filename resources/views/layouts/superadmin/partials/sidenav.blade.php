<!-- Side Nav START -->
<div class="side-nav">
    <div class="side-nav-inner">
        <div class="side-nav-logo">
            <a href="{{ route('superadmin.dashboard.index') }}">
                <div class="logo logo-dark" style="background-image: url('{{ asset('assets/images/logo/logo.png') }}')"></div>
                <div class="logo logo-white" style="background-image: url('{{ asset('assets/images/logo/logo-white.png') }}')"></div>
            </a>
            <div class="mobile-toggle side-nav-toggle">
                <a href="#">
                    <i class="ti-arrow-circle-left"></i>
                </a>
            </div>
        </div>
        <ul class="side-nav-menu scrollable">
            <li class="nav-item active">
                <a class="mrg-top-30" href="{{ route('superadmin.dashboard.index') }}">
                    <span class="icon-holder">
                            <i class="ti-home"></i>
                        </span>
                    <span class="title">Dashboard</span>
                </a>
            </li>

            <li class="nav-item dropdown {{ Request::is('superadmin/business*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-layout-media-overlay"></i>
                    </span>
                    <span class="title">Business</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('superadmin/business/all_business*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.business.all') }}">All Businesses</a>
                    </li>
                    <li class="{{ Request::is('superadmin/business/active_business*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.business.active') }}">Active Businesses</a>
                    </li>
                    <li class="{{ Request::is('superadmin/business/inactive_business*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.business.inactive') }}">Inactive Businesses</a>
                    </li>
                    <li class="{{ Request::is('superadmin/business/left_business*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.business.left') }}">Left Businesses</a>
                    </li>
                    <li class="{{ Request::is('superadmin/business/create*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.business.create') }}">Add New Business</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item dropdown {{ Request::is('superadmin/content*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-agenda"></i>
                    </span>
                    <span class="title">Content</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('superadmin/content/all*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.content.all') }}">All Contents</a>
                    </li>
                    <li class="{{ Request::is('superadmin/content/written*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.content.written') }}">Written Contents</a>
                    </li>
                    <li class="{{ Request::is('superadmin/content/awaiting_feedback*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.content.awaiting.feedback') }}">Awaiting Feedback</a>
                    </li>
                    <li class="{{ Request::is('superadmin/content/in_progress*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.content.progress') }}">In-Progress Contents</a>
                    </li>
                    <li class="{{ Request::is('superadmin/content/completed*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.content.completed') }}">Completed Contents</a>
                    </li>
                    <li class="{{ Request::is('superadmin/content/create*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.content.new.content') }}">Create New Content</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item dropdown {{ Request::is('superadmin/invoice*') || Request::is('common_auth/invoice*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-notepad"></i>
                    </span>
                    <span class="title">Invoices</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                @php
                    $role = ['superadmin', 'admin', 'finance', 'ceco', 'campm', 'designer', 'contw', 'posta', 'grm', 'client'];
                    $layout = $role[Auth::user()->role_id-1];
                @endphp
                <ul class="dropdown-menu">
                    <li class="{{ Request::is($layout.'/invoice/all_invoice*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.invoice.all') }}">All Invoices</a>
                    </li>
                    <li class="{{ Request::is($layout.'/invoice/paid*') ? 'active' : '' }}">
                        <a href="#">Paid</a>
                    </li>
                    <li class="{{ Request::is($layout.'/invoice/unpaid*') ? 'active' : '' }}">
                        <a href="#">Unpaid</a>
                    </li>
                    <li class="{{ Request::is($layout.'/invoice/overdue*') ? 'active' : '' }}">
                        <a href="#">Overdue</a>
                    </li>
                    <li class="{{ Request::is($layout.'/invoice/cancelled*') ? 'active' : '' }}">
                        <a href="#">Cancelled</a>
                    </li>
                    <li class="{{ Request::is($layout.'/invoice/draft*') ? 'active' : '' }}">
                        <a href="#">Draft</a>
                    </li>
                    <li class="{{ Request::is($layout.'/invoice/create*') ? 'active' : '' }}">
                        <a href="{{ route('common_auth.invoice.create', ['layout' => $layout]) }}">Create New Invoice</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item dropdown {{ Request::is('superadmin/client*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-user"></i>
                    </span>
                    <span class="title">Client</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('superadmin/client/all_client*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.client.all') }}">All Clients</a>
                    </li>
                    <li class="{{ Request::is('superadmin/client/active_client*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.client.active') }}">Active Clients</a>
                    </li>
                    <li class="{{ Request::is('superadmin/client/inactive_client*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.client.inactive') }}">Inactive Clients</a>
                    </li>
                    <li class="{{ Request::is('superadmin/client/left_client*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.client.left') }}">Left Clients</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item dropdown {{ Request::is('superadmin/setting*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ei-aircraft"></i>
                    </span>
                    <span class="title">User Settings</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('superadmin/setting/superadmin*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.setting.superadmin') }}">Super Admin</a>
                    </li>
                    <li class="{{ Request::is('superadmin/setting/administration*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.setting.administration') }}">Administration</a>
                    </li>
                    <li class="{{ Request::is('superadmin/setting/finance*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.setting.finance') }}">Finance</a>
                    </li>
                    <li class="{{ Request::is('superadmin/setting/ceco*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.setting.ceco') }}">CECO</a>
                    </li>
                    <li class="{{ Request::is('superadmin/setting/campm*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.setting.campm') }}">Campaign Manager</a>
                    </li>
                    <li class="{{ Request::is('superadmin/setting/contw*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.setting.contw') }}">Content Writer</a>
                    </li>
                    <li class="{{ Request::is('superadmin/setting/designer*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.setting.designer') }}">Designer</a>
                    </li>
                    <li class="{{ Request::is('superadmin/setting/posta*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.setting.posta') }}">Posting Assistant</a>
                    </li>
                    <li class="{{ Request::is('superadmin/setting/grm*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.setting.grm') }}">Guest Relation Manager</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item dropdown {{ Request::is('superadmin/system*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-panel"></i>
                    </span>
                    <span class="title">System Settings</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('superadmin/system/team*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.system.team') }}">Savasaachi Team</a>
                    </li>
                    <li class="{{ Request::is('superadmin/system/package*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.system.package') }}">Savasaachi Packages</a>
                    </li>
                    <li class="{{ Request::is('superadmin/system/social*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.system.social') }}">Social Medias</a>
                    </li>
                    <li class="{{ Request::is('superadmin/system/vault*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.system.vault') }}">Authors Vault</a>
                    </li>
                    {{-- <hr> --}}
                    <li class="{{ Request::is('superadmin/system/message*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.system.message') }}">Message Settings</a>
                    </li>
                    <li class="{{ Request::is('superadmin/system/email*') ? 'active' : '' }}">
                        <a href="{{ route('superadmin.system.email') }}">Email Settings</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- Side Nav END -->

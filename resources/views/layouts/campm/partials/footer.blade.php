<!-- Footer START -->
<footer class="content-footer">
    <div class="footer">
        <div class="copyright go-right">
            <span>Copyright © 2020 <b class="text-primary">Social Media Content Management System</b>. All rights reserved || Developed By <b><a href="javascript:void(0);" class="text-danger" data-toggle="modal" data-target="#developer_modal">PiNikk</a></b></span>
            {{-- <span class="go-right">
                <a href="#" class="text-primary mrg-right-10">Term &amp; Conditions</a>
                <a class="mrg-right-10">||</a>
                <a href="#" class="text-primary">Privacy &amp; Policy</a>
            </span> --}}
        </div>

        @include('layouts.developer')
    </div>
</footer>
<!-- Footer END -->

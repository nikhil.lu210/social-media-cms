@extends('layouts.superadmin.app')

@section('page_title', 'Content | Create Content')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .form-control{
            padding: 0.700rem 0.75rem;
        }
        .selectize-input {
            padding: 0.640rem 0.75rem;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
        .input-icon i {
            top: 15px;
            color: #7e297f59;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-4">
            <form action="{{ route('superadmin.content.create', ['business_id' => 1,'date' => 1]) }}" method="POST">
                @csrf
                <div class="card">
                    <div class="card-heading border bottom">
                        <h4 class="card-title text-bold text-center text-uppercase">New Content</h4>
                    </div>
                    <div class="card-block p-25">
                        <div class="row">
                            {{-- business name start --}}
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Select Business <span class="required">*</span></label>
                                    <div class="mrg-top-0">
                                        <select class="@error('business_id') is-invalid @enderror" name="business_id" id="selectize-dropdown" required>
                                            <option value="" disabled selected>Select Business</option>
                                            @foreach($businesses as $business)
                                            <option value="{{ $business->id }}">{{ $business->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    @error('business_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            {{-- business name end --}}

                            {{-- Date start --}}
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Date <span class="required">*</span></label>
                                    <div class="timepicker-input input-icon form-group">
                                        <i class="ti-calendar"></i>
                                        <input type="text" autocomplete="off" name="date" id="start_date" class="form-control start-date @error('date') is-invalid @enderror" placeholder="mm/dd/yyyy" value="{{ old('date') }}" data-provide="datepicker" required>
                                    </div>

                                    @error('date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            {{-- Date close --}}

                            {{-- Is Extra Content start --}}
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="checkbox checkbox-primary font-size-12 pull-left">
                                        <input id="is_extra" name="is_extra" class="form-control" type="checkbox">
                                        <label for="is_extra">Is Extra content?</label>
                                    </div>

                                    @error('is_extra')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            {{-- Is Extra Content close --}}
                        </div>
                    </div>
                    <div class="card-footer border top">
                        <ul class="list-unstyled list-inline pull-right">
                            <li class="list-inline-item">
                                <button type="submit" class="btn btn-primary btn-sm text-bold">
                                    <i class="ti-pencil"></i>
                                    Create Content
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- @include('superadmin.system.modals.team.update_modal') --}}
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection

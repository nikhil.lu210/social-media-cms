@extends('layouts.superadmin.app')

@section('page_title', 'Content | Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.1/emojionearea.css" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .form-control{
            padding: 0.700rem 0.75rem;
        }
        .selectize-input {
            padding: 0.640rem 0.75rem;
        }

        .responsive-image{
            border-radius: 5px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 80px;
            width: 100%;
        }
        .card-block.task-file.p-0 {
            height: 80px;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
        a.deactive{
            cursor: pointer;
        }
        li.social-media-list{
            border: 0px !important;
            border-bottom: 1px solid #e6ecf5 !important;
            margin-bottom: 10px;
        }
        li.social-media-list:last-child{
            border: 0px !important;
            border-bottom: 0px solid #e6ecf5 !important;
            margin-bottom: 0px;
        }
        .form-control:disabled, .form-control[readonly] {
            background-color: #ffffff;
            opacity: 1;
            color: #555;
            /* border-color: #efefef; */
        }

        .emojionearea .emojionearea-editor {
            min-height: 6.1em;
        }
        .emojionearea.form-control {
            border: 1px solid #e6ecf5;
            outline: none !important;
            box-shadow: none !important;
        }
        .emojionearea.form-control:focus,
        .emojionearea.form-control:active,
        .emojionearea.form-control.focused {
            border-color: #7e297f;
        }

        .d-none{
            transition: 0.5s all ease-in-out !important;
        }
    </style>
@endsection

@section('main_content')

@php
    $business = $content->business;
    $date = $content->date;
    $is_extra = $content->is_extra;
@endphp

<div class="container-fluid">
    <div class="page-title">
        <h4>Content for {{ $business->name }} <sup class="text-bold">({{ $date }})</sup></h4>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('common_auth.content.update', ['layout' => 'superadmin', 'content_id' => $content->id]) }}" method="post">
                @csrf
                <div class="card">
                    <div class="card-block p-25">
                        <div class="row">
                            <input type="hidden" name="business_id" value="{{ $business->id }}">
                            <input type="hidden" name="date" value="{{ $date }}">


                            <div class="col-lg-6">
                                <div class="form-group emoji-hide-div">
                                    <label>Post Caption <span class="required">*</span></label>
                                    <textarea autocomplete="off" class="form-control @error('post_caption') is-invalid @enderror read-only" placeholder="Post Caption Here..." rows="3" readonly>{{ $content->post_caption }}</textarea>

                                    <small>Write the <span class="text-primary text-bold">Post Caption</span> here</small>
                                </div>
                                <div class="form-group d-none">
                                    <label>Post Caption <span class="required">*</span></label>
                                    <textarea autocomplete="off" name="post_caption" class="form-control @error('post_caption') is-invalid @enderror" placeholder="Post Caption Here..." rows="3" id="emojionearea1">{{ $content->post_caption }}</textarea>

                                    <small>Write the <span class="text-primary text-bold">Post Caption</span> here</small>

                                    @error('post_caption')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Business Tags <span class="required">*</span></label>
                                    <textarea autocomplete="off" name="tags" class="form-control @error('tags') is-invalid @enderror read-only" placeholder="#tag_one #tag_two #tag_three" rows="3" required readonly>{{ $content->tags }}</textarea>

                                    <small>Please type all the Tags using <span class="text-primary text-bold">Hastag (#)</span>. <span class="required">[eg: <span class="text-info">#tag_one #tag_two</span>]</span></small>

                                    @error('tags')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Designer Contents <span class="required">*</span></label>
                                    <textarea autocomplete="off" name="designer_content" class="form-control @error('designer_content') is-invalid @enderror read-only" placeholder="Designer Contents Here..." rows="3" readonly>{{ $content->designer_content }}</textarea>

                                    <small>Write the <span class="text-primary text-bold">Contents For Designer</span> that he/she will keep in design.</small>

                                    @error('designer_content')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Designer Instruction <span class="required">*</span></label>
                                    <textarea autocomplete="off" name="designer_instruction" class="form-control @error('designer_instruction') is-invalid @enderror read-only" placeholder="Designer Instruction Here..." rows="3" readonly>{{ $content->designer_instruction }}</textarea>

                                    <small>Write the <span class="text-primary text-bold">Design Instruction</span> that will be followed by Designer for creating design for this content.</small>

                                    @error('designer_instruction')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Extra Note</label>
                                    <textarea autocomplete="off" name="note" class="form-control @error('note') is-invalid @enderror read-only" placeholder="Simple Note Here..." rows="3" readonly>{{ $content->note }}</textarea>

                                    <small>Write any <span class="text-primary text-bold">Extra Note</span> if you want to keep or show in the system.</small>

                                    @error('note')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer border top">
                        <ul class="list-unstyled list-inline pull-left">
                            <li class="list-inline-item">
                                <div class="checkbox checkbox-primary font-size-12 pull-left">
                                    <input id="is_extra" name="is_extra" class="form-control disable-input" type="checkbox" @if($is_extra == 1) checked @endif disabled>
                                    <label for="is_extra" class="m-b-0">Is Extra content?</label>
                                </div>

                                @error('is_extra')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </li>
                        </ul>
                        <ul class="list-unstyled list-inline pull-right">
                            <li class="list-inline-item">
                                <button type="button" id="btnEdit" class="btn btn-default btn-sm text-bold">
                                    <i class="ti-pencil"></i>
                                    Edit Content
                                </button>
                            </li>
                            <li class="list-inline-item d-none">
                                <button type="button" id="btnClose" class="btn btn-dark btn-sm text-bold">
                                    <i class="ti-close"></i>
                                    Cancel
                                </button>
                            </li>
                            <li class="list-inline-item d-none">
                                <button type="submit" id="btnUpdate" class="btn btn-primary btn-sm text-bold">
                                    <i class="ti-save"></i>
                                    Update This Content
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row share-div">
        <div class="col-xl-12 text-center">
            <div class="card">
                <div class="card-block">
                    <ul class="list-unstyled list-inline">
                        @foreach($business->socialVaults as $social)
                        <li class="list-inline-item no-pdd-horizon">
                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-html="true" title="<b>Click to Share on <br> <u class='text-uppercase'>{{ $social->socialMedia->name }}</u></br>" class="btn btn-primary btn-icon btn-sm btn-rounded m-10">
                                <b class="text-bold">{{ $social->socialMedia->name }}</b>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.1/emojionearea.min.js"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#img-preview').css('background-image', 'url('+e.target.result +')');
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').hide();
                    $('#img-preview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img-upload").change(function() {
            readURL(this);
        });
    </script>

    <script>
        // Custom Script Here

        $(document).ready(function() {
            $("#emojionearea1").emojioneArea({

                pickerPosition: "right",
                tonesStyle: "bullet",
                events: {
                    keyup: function (editor, event) {
                        console.log(editor.html());
                        console.log(this.getText());
                    }
                }
            });
        });

        $(document).ready(function() {
            var edit = $('#btnEdit');
            var close = $('#btnClose');
            var update = $('#btnUpdate');

            var read = $('.read-only');
            var emoji = $('#emojionearea1').parent();
            var hide_emoji = $('.emoji-hide-div');
            var disable = $('.disable-input');

            var share = $('.share-div');

            edit.click(function(){
                edit.parent().fadeOut(500).addClass('d-none');
                close.parent().fadeIn(500).removeClass('d-none').css('display', 'inline-block');
                update.parent().fadeIn(500).removeClass('d-none').css('display', 'inline-block');

                share.fadeOut(500).addClass('d-none');

                read.removeAttr('readonly');
                hide_emoji.addClass('d-none');
                emoji.removeClass('d-none');
                disable.removeAttr('disabled');
            });

            close.click(function(){
                edit.parent().fadeIn(500).removeClass('d-none').css('display', 'inline-block');
                close.parent().fadeOut(500).addClass('d-none');
                update.parent().fadeOut(500).addClass('d-none');

                share.fadeIn(500).removeClass('d-none');

                read.attr('readonly', true);
                hide_emoji.removeClass('d-none');
                emoji.addClass('d-none');
                disable.attr('disabled', 'disabled');
            });
        });

    </script>
@endsection

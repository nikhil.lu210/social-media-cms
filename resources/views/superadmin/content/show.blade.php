@extends('layouts.superadmin.app')

@section('page_title', 'Content | All Contents')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .password-generate{
            position: absolute;
            right: 35px;
            font-weight: bold;
            text-transform: uppercase;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        @php
            $date = new DateTime($content_date);
        @endphp
        <h4>All Contents of {{ $date->format('d M Y') }}</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                {{-- <div class="card-heading">
                    <h4 class="card-title float-left">{{ $date->format('d M Y') }} (Content Lists)</h4>
                </div>
                <hr class="m-t-0"> --}}
                <div class="card-block">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">Sl.</th>
                                    <th class="text-center">Business Name</th>
                                    <th class="text-center">Content</th>
                                    <th class="text-center">Last Updated By</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($contents as $item => $content)
                                <tr>
                                    <td>
                                        <div class="m-6 text-center">
                                            <span class="text-dark">
                                                <b>{{ $item+1 }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="m-6 text-center">
                                            <span class="text-dark">
                                                <a href="{{ route('superadmin.business.show', ['business_id' => $content->business_id]) }}" target="_blank" class="text-primary text-bold">{{ $content->business->name }}</a>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="m-6 text-center">
                                            <span class="text-dark">
                                                @php
                                                    if($content->post_caption == null) $short = "Not Written...";
                                                    else $short = substr($content->post_caption, 0, 35);
                                                @endphp
                                                <a class="text-primary text-bold">{!! $short !!} ...</a>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="m-6 text-center">
                                            <span class="text-dark">
                                                @if(!$content->contentUpdates->isEmpty())
                                                    <a href="#" target="_blank" class="text-primary text-bold">{{ $content->contentUpdates[count($content->contentUpdates)-1]->updator->name }}</a>
                                                @else
                                                    <a href="#" target="_blank" class="text-primary text-bold">{{ $content->creator->name }}</a>
                                                @endif
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="m-6 text-center">
                                            <span class="text-dark">
                                                @if($content->content_status == 1)
                                                    @if($content->post_caption != null && !$content->contentFiles->isEmpty())
                                                        <b class="text-dark">Awaiting Feedback</b>
                                                    @elseif($content->post_caption != null && $content->contentFiles->isEmpty())
                                                        <b class="text-info">Written</b>
                                                    @elseif($content->post_caption == null && $content->contentFiles->isEmpty())
                                                        <b class="text-info">Designed</b>
                                                    @endif
                                                @elseif($content->content_status == 2)
                                                    <b class="text-warning">In Proccess</b>
                                                @elseif($content->content_status == 3)
                                                    <b class="text-success">Completed</b>
                                                @endif
                                            </span>
                                        </div>
                                    </td>

                                    <td>
                                        <div class="dropdown m-6 text-center">
                                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle p-0" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('superadmin.content.details', ['content_id' => $content->id]) }}" target="_blank">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>See Details</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="confirmation">
                                                        <i class="ei-pencil-alt pdd-right-10 text-dark"></i>
                                                        <span>Editable</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="confirmation">
                                                        <i class="ti-close pdd-right-10 text-danger"></i>
                                                        <span>Deactivate</span>
                                                    </a>
                                                </li>
                                                {{-- <li>
                                                    <a href="#" class="confirmation">
                                                        <i class="ti-check pdd-right-10 text-success"></i>
                                                        <span>Completed</span>
                                                    </a>
                                                </li> --}}
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

    </script>
@endsection

@extends('layouts.superadmin.app')

@section('page_title', 'Content | Written Contents')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/fullcalendar/dist/fullcalendar.min.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
        .fc-month-button, .fc-agendaWeek-button, .fc-agendaDay-button{
            display: none;
        }
        .fc-event-container .fc-day-grid-event {
            margin: 5px 5px 5px;
        }
        .fc-event-container .fc-event {
            text-align: center;
        }
    </style>
@endsection

@section('main_content')
    <div class="container-fluid">
        <div class="page-title">
            <h4>Written Contents</h4>
        </div>

        {{-- Calender --}}
        <div class="row">
            <div class="col-md-12">
                <div id="full-calendar"></div>
            </div>
        </div>
    </div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/fullcalendar/dist/fullcalendar.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/js/apps/calendar.js') }}"></script> --}}
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        // Calendar JavaScripts
        (function () {
            'use strict';
            // ------------------------------------------------------- //
            // Calendar
            // ------------------------------------------------------ //
            jQuery(function() {
                // page is ready
                var ajaxdata;
                var url = "{{ route('common_auth.content.get.content', ['layout' => 'superadmin']) }}";
                $.ajax({
                    method: 'GET',
                    url: url,
                    datatype: "JSON",
                    success: function(data) {

                        // console.log(data);
                        var events = [];
                        var i = 0;
                        for (var date in data) {
                            for(var sl in data[date]){
                                // console.log(data[date][sl]);
                                if(data[date][sl].content_status == 2){
                                    var url = document.location.origin + '/superadmin/content/show/' + date;
                                    events[i] =
                                                {
                                                    title: 'Total Contents',
                                                    start: date, //yyyy-mm-dd
                                                    url: url,
                                                    icon : "book",
                                                    // urlTarget: "_blank",
                                                    total: data[date].length,
                                                };
                                    i++;
                                }
                            }
                        }

                        jQuery('#full-calendar').fullCalendar({
                            height: 700,
                            header: {
                                left: 'title',
                                center: 'month,agendaWeek,agendaDay',
                                right: 'today prev,next'
                            },
                            events: events,
                            eventRender: function(event, element) {
                                if(event.icon){
                                    element.find(".fc-title").prepend("<i class='fa fa-"+event.icon+"'></i> ");
                                }
                                if(event.total){
                                    element.find(".fc-title").append(" - <b>"+event.total+ "</b>");
                                }
                                if(event.url && event.urlTarget){
                                    element.find(".fc-title").parent().parent().attr("target", event.urlTarget);
                                }
                            },
                        });

                    },
                    error: function() {
                        jQuery('#full-calendar').fullCalendar({
                            height: 700,
                            header: {
                                left: 'title',
                                center: 'month,agendaWeek,agendaDay',
                                right: 'today prev,next'
                            },
                            eventRender: function(event, element) {
                                if(event.icon){
                                    element.find(".fc-title").prepend("<i class='fa fa-"+event.icon+"'></i> ");
                                }
                                if(event.total){
                                    element.find(".fc-title").append(" - <b>"+event.total+ "</b>");
                                }
                                if(event.url && event.urlTarget){
                                    element.find(".fc-title").parent().parent().attr("target", event.urlTarget);
                                }
                            },
                        });
                    }
                });
            });

        })(jQuery);
    </script>
@endsection

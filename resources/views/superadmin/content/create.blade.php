@extends('layouts.superadmin.app')

@section('page_title', 'Content | Create Content')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.1/emojionearea.css" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .form-control{
            padding: 0.700rem 0.75rem;
        }
        .selectize-input {
            padding: 0.640rem 0.75rem;
        }

        .responsive-image{
            border-radius: 5px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 80px;
            width: 100%;
        }
        .card-block.task-file.p-0 {
            height: 80px;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
        a.deactive{
            cursor: pointer;
        }
        li.social-media-list{
            border: 0px !important;
            border-bottom: 1px solid #e6ecf5 !important;
            margin-bottom: 10px;
        }
        li.social-media-list:last-child{
            border: 0px !important;
            border-bottom: 0px solid #e6ecf5 !important;
            margin-bottom: 0px;
        }
        .form-control:disabled, .form-control[readonly] {
            background-color: #ffffff;
            opacity: 1;
            color: #555;
            /* border-color: #efefef; */
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>Create New Content</h4>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('common_auth.content.store', ['layout' => 'superadmin']) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-block p-25">
                        {{-- <div class="row justify-content-center m-b-20">
                            <div class="col-lg-4 text-center">
                                <div>
                                    <label for="img-upload" class="pointer">
                                        <img id="img-preview" src="{{ asset('assets/images/others/img-10.jpg') }}"  width="117" alt="">

                                        <span class="btn btn-default display-block no-mrg-btm">Choose Logo<span class="required">*</span></span>
                                        <input class="d-none @error('logo') is-invalid @enderror" type="file" name="logo" accept=".png, .jpg, .jpeg" id="img-upload" value="{{ old('logo') }}">
                                    </label>
                                    @error('logo')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div> --}}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Business <span class="required">*</span></label>
                                    <input autocomplete="off" type="text" class="form-control text-bold @error('name') is-invalid @enderror" value="{{ $business->name }}" placeholder="Business Name Here" readonly required>
                                    <input type="hidden" name="business_id" value="{{ $business->id }}">

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Date <span class="required">*</span></label>
                                    <input autocomplete="off" type="text" name="date" class="form-control text-bold @error('date') is-invalid @enderror" value="{{ $date }}" placeholder="25-12-2019" readonly required>

                                    @error('date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Post Caption <span class="required">*</span></label>
                                    <textarea autocomplete="off" name="post_caption" class="form-control @error('post_caption') is-invalid @enderror" placeholder="Post Caption Here..." rows="3" id="emojionearea1">{{ old('post_caption') }}</textarea>

                                    <small>Write the <span class="text-primary text-bold">Post Caption</span> here</small>

                                    @error('post_caption')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Business Tags <span class="required">*</span></label>
                                    <textarea autocomplete="off" name="tags" class="form-control @error('tags') is-invalid @enderror" placeholder="#tag_one #tag_two #tag_three" rows="3" required>{{ $business->tags }}</textarea>

                                    <small>Please type all the Tags using <span class="text-primary text-bold">Hastag (#)</span>. <span class="required">[eg: <span class="text-info">#tag_one #tag_two</span>]</span></small>

                                    @error('tags')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Designer Contents <span class="required">*</span></label>
                                    <textarea autocomplete="off" name="designer_content" class="form-control @error('designer_content') is-invalid @enderror" placeholder="Designer Contents Here..." rows="3">{{ old('designer_content') }}</textarea>

                                    <small>Write the <span class="text-primary text-bold">Contents For Designer</span> that he/she will keep in design.</small>

                                    @error('designer_content')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Designer Instruction <span class="required">*</span></label>
                                    <textarea autocomplete="off" name="designer_instruction" class="form-control @error('designer_instruction') is-invalid @enderror" placeholder="Designer Instruction Here..." rows="3">{{ old('designer_instruction') }}</textarea>

                                    <small>Write the <span class="text-primary text-bold">Design Instruction</span> that will be followed by Designer for creating design for this content.</small>

                                    @error('designer_instruction')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Extra Note</label>
                                    <textarea autocomplete="off" name="note" class="form-control @error('note') is-invalid @enderror" placeholder="Simple Note Here..." rows="3">{{ old('note') }}</textarea>

                                    <small>Write any <span class="text-primary text-bold">Extra Note</span> if you want to keep or show in the system.</small>

                                    @error('note')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer border top">
                        <ul class="list-unstyled list-inline pull-left">
                            <li class="list-inline-item">
                                <div class="checkbox checkbox-primary font-size-12 pull-left">
                                    <input id="is_extra" name="is_extra" class="form-control" type="checkbox" @if($is_extra == 1) checked @endif>
                                    <label for="is_extra">Is Extra content?</label>
                                </div>

                                @error('is_extra')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </li>
                        </ul>
                        <ul class="list-unstyled list-inline pull-right">
                            <li class="list-inline-item">
                                <button type="submit" class="btn btn-primary btn-sm text-bold">
                                    <i class="ti-save"></i>
                                    Save This Content
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- @include('superadmin.system.modals.team.update_modal') --}}
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>

    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#img-preview').css('background-image', 'url('+e.target.result +')');
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').hide();
                    $('#img-preview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img-upload").change(function() {
            readURL(this);
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.1/emojionearea.min.js"></script>

    <script>
        // Custom Script Here

        $(document).ready(function() {
            $("#emojionearea1").emojioneArea({

                pickerPosition: "right",
                tonesStyle: "bullet",
                events: {
                    keyup: function (editor, event) {
                        console.log(editor.html());
                        console.log(this.getText());
                    }
                }
            });
        });

    </script>
@endsection

<div class="modal slide-in-right modal-right fade " id="add_designer">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4>New Designer</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="{{ route('superadmin.setting.create_user', ['role' => 'designer']) }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Role <span class="required">*</span></label>
                                    <input autocomplete="off" type="text" name="role_name" readonly class="form-control" value="Designer" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Full Name <span class="required">*</span></label>
                                    <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="Ahad Ullah Shah" required>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Email Address <span class="required">*</span></label>
                                    <input autocomplete="off" type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="ahad@veechitechnologies.com" required>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Contact No. <span class="required">*</span></label>
                                    <input autocomplete="off" type="text" name="contact_number" class="form-control @error('contact_number') is-invalid @enderror" value="{{ old('contact_number') }}" placeholder="+44 7542 987483" required>

                                    @error('contact_number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Password. <span class="required">*</span> <a href="#" class="password-generate text-primary" id="passwordGenerate">Generate</a></label>
                                    <input autocomplete="off" type="text" name="password" id="password_generator" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" placeholder="123ABCabc!@#$%" required>

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="checkbox checkbox-primary font-size-12">
                                    <input autocomplete="off" id="send_mail" name="send_mail" type="checkbox" checked required>
                                    <label for="send_mail">Send Mail With Login Informations</label>
                                </div>
                                <button class="btn btn-primary btn-sm btn-block" type="submit">Assign As <span class="text-bold">DESIGNER</span></button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal slide-in-right modal-right fade " id="update_social">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4>Update Social Media</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="{{ route('superadmin.system.social.update') }}" method="post">
                                @csrf

                                <input type="hidden" id="social_id" name="social_id">
                                <div class="form-group">
                                    <label>Website Name <span class="required">*</span></label>
                                    <input autocomplete="off" id="social_name" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="Facebook" placeholder="Facebook" required>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Website URL <span class="required">*</span></label>
                                    <input autocomplete="off" type="url" id="social_url" name="url" class="form-control @error('url') is-invalid @enderror" value="https://www.facebook.com" placeholder="https://www.facebook.com" required>

                                    @error('url')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea autocomplete="off" name="description" id="social_description" class="form-control @error('description') is-invalid @enderror" placeholder="Short Description Here...">Short Description Here</textarea>

                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="checkbox checkbox-primary font-size-12">
                                    <input id="social_status" name="status" type="checkbox">
                                    <label for="social_status">Activate This Social Media ?</label>
                                </div>
                                <button class="btn btn-primary btn-sm btn-block" type="submit">Update <span class="text-bold">SOCIAL MEDIA</span></button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

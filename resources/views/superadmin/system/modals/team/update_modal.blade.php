{{-- Upload File Modal --}}
<div class="modal fade" id="team_update">
    {{-- <div class="modal-dialog modal-lg" role="document"> --}}
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4><b>Team Details Update</b></h4>
            </div>
            <form action="{{ route('superadmin.system.team.update', ['id' => $team->id]) }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Team Name <span class="required">*</span></label>
                        <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ $team->name }}" placeholder="SAVA-TEAM-01" required>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="checkbox checkbox-primary font-size-12">
                        <input id="status" name="status" type="checkbox" @if ($team->status == 1) checked @endif>
                        <label for="status">Activate This Team ?</label>
                    </div>
                </div>
                <hr class="m-0">
                <div class="modal-footer no-border">
                    <div class="text-right">
                        <button class="btn btn-dark btn-sm" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-primary btn-sm" type="submit">Update Now</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

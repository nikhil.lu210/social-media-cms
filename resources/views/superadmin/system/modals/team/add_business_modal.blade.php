{{-- Upload File Modal --}}
<div class="modal fade" id="add_business">
    {{-- <div class="modal-dialog modal-lg" role="document"> --}}
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4><b>Add New Business in team_name_here</b></h4>
            </div>
            <form action="{{ route('superadmin.system.team.business.add', ['team_id' => $team->id]) }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Select Business <span class="required">*</span></label>
                        <div class="mrg-top-0">
                            <select class="@error('business_id') is-invalid @enderror" name="business_id" id="selectize-dropdown" required>
                                <option value="" disabled selected>Select a Category</option>
                                @foreach($allBusinesses as $tmpBusiness)
                                <option value="{{ $tmpBusiness->id }}">{{ $tmpBusiness->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        @error('business_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <hr class="m-0">
                <div class="modal-footer no-border">
                    <div class="text-right">
                        <button class="btn btn-dark btn-sm" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-primary btn-sm" type="submit">Add Now</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

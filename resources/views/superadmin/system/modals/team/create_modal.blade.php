<div class="modal slide-in-right modal-right fade " id="add_team">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4>New Team</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="{{ route('superadmin.system.team.create') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Team Name <span class="required">*</span></label>
                                    <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="SAVA-TEAM-01" required>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="checkbox checkbox-primary font-size-12">
                                    <input id="status" name="status" type="checkbox" checked required>
                                    <label for="status">Activate This Team ?</label>
                                </div>
                                <button class="btn btn-primary btn-sm btn-block" type="submit">Assign As <span class="text-bold">NEW TEAM</span></button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

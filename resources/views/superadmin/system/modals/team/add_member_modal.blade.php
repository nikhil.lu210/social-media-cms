{{-- Upload File Modal --}}
<div class="modal fade" id="add_member">
    {{-- <div class="modal-dialog modal-lg" role="document"> --}}
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4><b>Add New Member in team_name_here</b></h4>
            </div>
            <form action="{{ route('superadmin.system.team.member.create', ['team_id' => $team->id]) }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Select Member <span class="required">*</span></label>
                        <div class="mrg-top-0">
                            <select class="@error('user_id') is-invalid @enderror" name="user_id" id="selectize-dropdown2" required>
                                <option value="" disabled selected>Select a Member</option>
                                @foreach ($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ $employee->name }} <span class="text-primary">( {{ $employee->role->name }} )</span></option>
                                @endforeach
                            </select>
                        </div>

                        @error('user_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <hr class="m-0">
                <div class="modal-footer no-border">
                    <div class="text-right">
                        <button class="btn btn-dark btn-sm" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-primary btn-sm" type="submit">Add Now</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal slide-in-right modal-right fade " id="update_package">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4>Update Package</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="{{ route('superadmin.system.package.update', ['package_id' => $package->id]) }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Package Name <span class="required">*</span></label>
                                    <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ $package->name }}" placeholder="Platinum +" required>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea autocomplete="off" name="description" class="form-control @error('description') is-invalid @enderror" placeholder="Short Description Here...">{{ $package->description }}</textarea>

                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="checkbox checkbox-primary font-size-12">
                                    <input id="status" name="status" type="checkbox" @if($package->status == 1) checked @endif>
                                    <label for="status">Activate This Package ?</label>
                                </div>
                                <button class="btn btn-primary btn-sm btn-block" type="submit">Update <span class="text-bold text-uppercase">Package</span></button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal slide-in-right modal-right fade " id="add_package">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4>New Savasaachi Package</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="{{ route('superadmin.system.package.create') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Package Name <span class="required">*</span></label>
                                    <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="Platinum +" required>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea autocomplete="off" name="description" class="form-control @error('description') is-invalid @enderror" placeholder="Short Description Here...">{{ old('description') }}</textarea>

                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="checkbox checkbox-primary font-size-12">
                                    <input id="status" name="status" type="checkbox" checked>
                                    <label for="status">Activate This Package ?</label>
                                </div>
                                <button class="btn btn-primary btn-sm btn-block" type="submit">Assign As <span class="text-bold text-uppercase">NEW Package</span></button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

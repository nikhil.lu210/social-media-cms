@extends('layouts.superadmin.app')

@section('page_title', 'Systems | Package | Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .responsive-image{
            border-radius: 5px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 80px;
            width: 100%;
        }
        .card-block.task-file.p-0 {
            height: 80px;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
        a.deactive{
            cursor: pointer;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ strtoupper($package->name) }}</h4>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="portlet">
                            <ul class="portlet-item navbar">
                                <li>
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#update_package" class="btn btn-primary">
                                        <i class="ti-pencil-alt"></i>
                                        <b>Update</b>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-heading p-b-10">
                            <h4 class="card-title"><b>Package Details</b></h4>
                        </div>
                        <hr class="m-t-0">
                        <div class="card-block p-26 p-t-5">
                            <table class="table table-bordered table-responsive no-mrg-btm">
                                <tbody>
                                    <tr>
                                        <th>Package Name</th>
                                        <td>{{ $package->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Description</th>
                                        <td>{{ $package->description }}</td>
                                    </tr>

                                    @php
                                        // dd($package);
                                        $disBuiness = App\Models\Business\Business::where('package_id', $package->id)->where('status', 0)->count();
                                        $activeBusiness = sizeof($package->busniesses) - $disBuiness;
                                    @endphp
                                    <tr>
                                        <th>Total Business</th>
                                        <td>{{ $activeBusiness }}</td>
                                    </tr>
                                    <tr>
                                        @php
                                            $date = new DateTime($package->created_at);
                                            $update_date = new DateTime($package->updated_at);
                                        @endphp
                                        <th>Package Creation Date</th>
                                        <td>{{ $date->format('d M Y') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Package Deactivate Date</th>
                                        @if($package->status == 1)
                                            <td class="text-success text-bold">Active</td>
                                        @else
                                            <td class="text-danger">{{ $update_date->format('d M Y') }}</td>
                                        @endif
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="card">
                <div class="portlet">
                    <ul class="portlet-item navbar">
                        <li>
                            <a href="#" class="btn btn-primary">
                                <i class="ti-plus"></i>
                                <b>Add Business</b>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="card-heading p-b-10">
                    <h4 class="card-title"><b>Businesses Of package_name</b></h4>
                </div>
                <hr class="m-t-0">
                <div class="card-block p-26 p-t-5">
                    <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th class="text-center">Sl.</th>
                                <th class="text-center">Business Name</th>
                                <th class="text-center">Client</th>
                                <th class="text-center">Assigned Date</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="m-6 text-center">
                                        <span class="text-dark">
                                            <b>01</b>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <div class="m-6 text-center">
                                        <span class="text-dark">
                                            <a href="#" target="_blank" class="text-primary text-bold">business_name</a>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <div class="m-6 text-center">
                                        <span class="text-dark">
                                            <a href="#" target="_blank" class="text-primary text-bold">client_name</a>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <div class="m-6 text-center">
                                        <span class="text-dark">
                                            <b>01 Nov 2018</b>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <div class="m-6 text-center">
                                        <span class="text-dark">
                                            <b class="text-success">Active</b>
                                            {{-- <b class="text-danger">01 Dec 2019</b> --}}
                                        </span>
                                    </div>
                                </td>

                                <td>
                                    <div class="dropdown m-6 text-center">
                                        <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle p-0" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="#">
                                                    <i class="ti-eye pdd-right-10 text-info"></i>
                                                    <span>See Details</span>
                                                </a>
                                            </li>

                                            {{-- @if ($teamName->status == 1) --}}
                                                <li>
                                                    <a href="#" class="confirmation">
                                                        <i class="ti-close pdd-right-10 text-danger"></i>
                                                        <span>Deactivate</span>
                                                    </a>
                                                </li>
                                                {{-- <li>
                                                    <a href="#" onclick="return confirm('If you Deactivate This Business, The Team Members Will Not Be Able to access the Business anymore. \n\n\nAre You Sure Want To Deactivate This Business For This Team..?');">
                                                        <i class="ti-close pdd-right-10 text-danger"></i>
                                                        <span>Deactivate</span>
                                                    </a>
                                                </li> --}}
                                            {{-- @else --}}
                                                <li>
                                                    <a href="#" class="confirmation">
                                                        <i class="ti-check pdd-right-10 text-success"></i>
                                                        <span>Activate</span>
                                                    </a>
                                                </li>
                                                {{-- <li>
                                                    <a href="#" onclick="return confirm('Are You Sure Want To Activate This Business For This Team..?');">
                                                        <i class="ti-check pdd-right-10 text-success"></i>
                                                        <span>Activate</span>
                                                    </a>
                                                </li> --}}
                                            {{-- @endif --}}
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('superadmin.system.modals.package.show_modal')
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

    </script>
@endsection

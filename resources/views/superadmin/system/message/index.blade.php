@extends('layouts.superadmin.app')

@section('page_title', 'Systems | Teams')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .password-generate{
            position: absolute;
            right: 35px;
            font-weight: bold;
            text-transform: uppercase;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>Savasaachi Teams</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-left">All Teams</h4>

                    <a href="javascript:void(0);" data-toggle="modal" data-target="#add_team" class="btn btn-primary btn-sm float-right m-b-0">Add New Team</a>
                </div>
                <hr class="m-t-0">
                <div class="card-block">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">Sl.</th>
                                    <th class="text-center">Team Name</th>
                                    <th class="text-center">Total Business</th>
                                    <th class="text-center">Created Date</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- @foreach ($teamNames as $item => $teamName) --}}
                                <tr>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>01</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>dfsgdf</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>sdfsd</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        {{-- @php
                                            $date = new DateTime($teamName->created_at);
                                        @endphp --}}
                                        <div class="mrg-top-15">
                                            {{-- <span>{{ $date->format('d M Y') }}</span> --}}
                                            <span>sdfsd</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            {{-- @if ($teamName->status == 1) --}}
                                                <b class="text-success">Active</b>
                                            {{-- @else
                                                <b class="text-danger">Deactivate</b>
                                            @endif --}}
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-10 dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="#">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>See Details</span>
                                                    </a>
                                                </li>

                                                {{-- @if ($teamName->status == 1) --}}
                                                    <li>
                                                        <a href="#" onclick="return confirm('If you Deactivate This Team, The Team Members Will Not Be Able to access the Businesses anymore. \n\n\nAre You Sure Want To Deactivate This Team..?');">
                                                            <i class="ti-close pdd-right-10 text-danger"></i>
                                                            <span>Deactivate</span>
                                                        </a>
                                                    </li>
                                                {{-- @else
                                                    <li>
                                                        <a href="#" onclick="return confirm('Are You Sure Want To Activate This Team..?');">
                                                            <i class="ti-check pdd-right-10 text-success"></i>
                                                            <span>Activate</span>
                                                        </a>
                                                    </li>
                                                @endif --}}
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                {{-- @endforeach --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('superadmin.system.modals.team.create_modal')
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

    </script>
@endsection

@extends('layouts.superadmin.app')

@section('page_title', 'Systems | Social Medias')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>Social Medias</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-left">All Social Medias</h4>

                    <a href="javascript:void(0);" data-toggle="modal" data-target="#add_social" class="btn btn-primary btn-sm float-right m-b-0">Add New Social Media</a>
                </div>
                <hr class="m-t-0">
                <div class="card-block">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">Sl.</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">URL</th>
                                    <th class="text-center">Created Date</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($socials as $item => $social)
                                <tr>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>{{ $item+1 }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>{{ $social->name }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <a href="{{ $social->url }}" data-toggle="tooltip" data-html="true" title="https://www.facebook.com" target="_blank" class="text-bold text-primary">Go To Website</a>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        @php
                                            $date = new DateTime($social->created_at);
                                        @endphp
                                        <div class="mrg-top-15">
                                            <span>{{ $date->format('d M Y') }}</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            @if($social->status == 1)
                                                <b class="text-success">Active</b>
                                            @else
                                                <b class="text-danger">Deactivate</b>
                                            @endif
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-10 dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="javascript:void(0);" data-toggle="modal" data-todo="{{ $social }}" data-target="#update_social">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>See Details</span>
                                                    </a>
                                                </li>

                                                @if ($social->status == 1)
                                                    <li>
                                                        <a href="{{ route('superadmin.system.social.change_status', ['social_id' => $social->id]) }}" class="confirmation">
                                                            <i class="ti-close pdd-right-10 text-danger"></i>
                                                            <span>Deactivate</span>
                                                        </a>
                                                    </li>
                                                    {{-- <li>
                                                        <a href="{{ route('superadmin.system.social.change_status', ['social_id' => $social->id]) }}" onclick="return confirm('If you Deactivate This Team, The Team Members Will Not Be Able to access the Businesses anymore. \n\n\nAre You Sure Want To Deactivate This Team..?');">
                                                            <i class="ti-close pdd-right-10 text-danger"></i>
                                                            <span>Deactivate</span>
                                                        </a>
                                                    </li> --}}
                                                @else
                                                    <li>
                                                        <a href="{{ route('superadmin.system.social.change_status', ['social_id' => $social->id]) }}" class="confirmation">
                                                            <i class="ti-check pdd-right-10 text-success"></i>
                                                            <span>Activate</span>
                                                        </a>
                                                    </li>
                                                    {{-- <li>
                                                        <a href="{{ route('superadmin.system.social.change_status', ['social_id' => $social->id]) }}" onclick="return confirm('Are You Sure Want To Activate This Team..?');">
                                                            <i class="ti-check pdd-right-10 text-success"></i>
                                                            <span>Activate</span>
                                                        </a>
                                                    </li> --}}
                                                @endif
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('superadmin.system.modals.social.create_modal')
    @include('superadmin.system.modals.social.show_modal')
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

        $('#update_social').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var data = button.data('todo');
            var modal = $(this);

            // console.log(data);
            modal.find('.pdd-horizon-15 #social_id').val(data.id);
            modal.find('.pdd-horizon-15 #social_name').val(data.name);
            modal.find('.pdd-horizon-15 #social_url').val(data.url);
            modal.find('.pdd-horizon-15 #social_description').html(data.description);

            if(data.status == 1)    modal.find('.pdd-horizon-15 #social_status').prop('checked', true);
            else    modal.find('.pdd-horizon-15 #social_status').prop('checked', false);

        });


    </script>
@endsection

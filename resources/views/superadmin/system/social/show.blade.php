@extends('layouts.superadmin.app')

@section('page_title', 'Systems | Team | Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .responsive-image{
            border-radius: 5px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 80px;
            width: 100%;
        }
        .card-block.task-file.p-0 {
            height: 80px;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
        a.deactive{
            cursor: pointer;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ strtoupper($team->name) }}</h4>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="portlet">
                            <ul class="portlet-item navbar">
                                <li>
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#team_update" class="btn btn-primary">
                                        <i class="ti-pencil-alt"></i>
                                        <b>Update</b>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-heading p-b-10">
                            <h4 class="card-title"><b>Team Details</b></h4>
                        </div>
                        <hr class="m-t-0">
                        <div class="card-block p-26 p-t-5">
                            {{-- row php code start --}}
                            @php
                                $member = sizeof($team->teams)-$disMembers;
                                $business = sizeof($team->businesses) - $disBusinesses;
                                $create_date = new DateTime($team->created_at);
                                $update_date = new DateTime($team->updated_at);
                            @endphp
                            {{-- row php code end --}}

                            <table class="table table-bordered table-responsive no-mrg-btm">
                                <tbody>
                                    <tr>
                                        <th>Team Name</th>
                                        <td>{{ $team->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Total Member</th>
                                        <td>{{ $member }}</td>
                                    </tr>
                                    <tr>
                                        <th>Total Business</th>
                                        <td>{{ $business }}</td>
                                    </tr>
                                    <tr>
                                        <th>Team Creation Date</th>
                                        <td>{{ $create_date->format('d M Y') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Team Deactivate Date</th>
                                        @if($team->status == 1)
                                            <td class="text-success text-bold">Active</td>
                                        @else
                                            <td class="text-danger">{{ $update_date->format('d M Y') }}</td>
                                        @endif
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        @if($team->status == 1)
                        <div class="portlet">
                            <ul class="portlet-item navbar">
                                <li>
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#add_member" class="btn btn-primary">
                                        <i class="ti-plus"></i>
                                        <b>Add Member</b>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        @endif
                        <div class="card-heading p-b-10">
                            <h4 class="card-title"><b>Active Team Members</b></h4>
                        </div>
                        <hr class="m-t-0">
                        <div class="card-block p-26 p-t-5">

                            @php
                                $teams = $team->teams;
                            @endphp
                            @foreach ($roles as $role)
                                @php
                                    $key = 1;
                                @endphp
                                <table class="table table-bordered table-responsive m-b-20">
                                    <thead>
                                        <tr>
                                            <th colspan="4" class="text-center text-bold font-size-20 p-10 text-primary">{{ $role->name }}</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center">Sl.</th>
                                            <th class="text-center">Name</th>
                                            <th class="text-center">Assigned Date</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($teams as $data)
                                            @php
                                                if($role->id != $data->user->role->id || $data->status != 1) continue;
                                                $date = new DateTime($data->created_at);
                                            @endphp
                                            <tr>
                                                <th class="text-center text-dark">{{ $key++ }}</th>
                                                <th class="text-center">
                                                    <a href="#" target="_blank" class="member-name text-primary">{{ $data->user->name }}</a>
                                                </th>
                                                <td class="text-center">{{ $date->format('d M Y') }}</td>
                                                <td>
                                                    <div class="dropdown text-center">
                                                        <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle p-0" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                                        <ul class="dropdown-menu">
                                                            <li>
                                                                <a href="#" target="_blank">
                                                                    <i class="ti-eye pdd-right-10 text-info"></i>
                                                                    <span>View Profile</span>
                                                                </a>
                                                            </li>

                                                            <li>
                                                                <a href="{{ route('superadmin.system.team.update.employee_status', ['user_id' => $data->user->id, 'team_id' => $team->id]) }}" class="confirmation">
                                                                    <i class="ti-close pdd-right-10 text-danger"></i>
                                                                    <span>Deactivate</span>
                                                                </a>
                                                                {{-- <a href="{{ route('superadmin.system.team.update.employee_status', ['user_id' => $data->user->id, 'team_id' => $team->id]) }}" onclick="return confirm('If you Deactivate This Member, He/She Will Not Be Able to access the Businesses of this Team anymore. \n\n\nAre You Sure Want To Deactivate This Member for this Team..?');">
                                                                    <i class="ti-close pdd-right-10 text-danger"></i>
                                                                    <span>Deactivate</span>
                                                                </a> --}}
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-7">
            <div class="card">
                <div class="portlet">
                    <ul class="portlet-item navbar">
                        <li>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#add_business" class="btn btn-primary">
                                <i class="ti-plus"></i>
                                <b>Add Business</b>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="card-heading p-b-10">
                    <h4 class="card-title"><b>Business List</b></h4>
                </div>
                <hr class="m-t-0">
                <div class="card-block p-26 p-t-5">
                    <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th class="text-center">Sl.</th>
                                <th class="text-center">Business Name</th>
                                <th class="text-center">Client</th>
                                <th class="text-center">Assigned Date</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="m-6 text-center">
                                        <span class="text-dark">
                                            <b>01</b>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <div class="m-6 text-center">
                                        <span class="text-dark">
                                            <a href="#" target="_blank" class="text-primary text-bold">business_name</a>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <div class="m-6 text-center">
                                        <span class="text-dark">
                                            <a href="#" target="_blank" class="text-primary text-bold">client_name</a>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <div class="m-6 text-center">
                                        <span class="text-dark">
                                            <b>01 Nov 2018</b>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <div class="m-6 text-center">
                                        <span class="text-dark">
                                            <b class="text-success">Active</b>
                                            {{-- <b class="text-danger">01 Dec 2019</b> --}}
                                        </span>
                                    </div>
                                </td>

                                <td>
                                    <div class="dropdown m-6 text-center">
                                        <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle p-0" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="#">
                                                    <i class="ti-eye pdd-right-10 text-info"></i>
                                                    <span>See Details</span>
                                                </a>
                                            </li>

                                            {{-- @if ($teamName->status == 1) --}}
                                                <li>
                                                    <a href="#" onclick="return confirm('If you Deactivate This Business, The Team Members Will Not Be Able to access the Business anymore. \n\n\nAre You Sure Want To Deactivate This Business For This Team..?');">
                                                        <i class="ti-close pdd-right-10 text-danger"></i>
                                                        <span>Deactivate</span>
                                                    </a>
                                                </li>
                                            {{-- @else --}}
                                                {{-- <li>
                                                    <a href="#" onclick="return confirm('Are You Sure Want To Activate This Business For This Team..?');">
                                                        <i class="ti-check pdd-right-10 text-success"></i>
                                                        <span>Activate</span>
                                                    </a>
                                                </li> --}}
                                            {{-- @endif --}}
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('superadmin.system.modals.team.update_modal')
    @include('superadmin.system.modals.team.add_business_modal')
    @include('superadmin.system.modals.team.add_member_modal')
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>

    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

    </script>
@endsection

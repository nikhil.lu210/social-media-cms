@extends('layouts.superadmin.app')

@section('page_title', 'Invoices')

@section('css_links')
    {{--  External CSS  --}}

@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        a{
            text-decoration: none !important;
        }
        table.table.table-borderless tbody tr th,
        table.table.table-borderless tbody tr td{
            line-height: 0.5;
            border-color: #ffffff;
        }
        .border-top-1{
            border-top: 1px dashed #dddddd !important;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="container">
        <div class="card">
            <div class="pdd-vertical-5 pdd-horizon-10 border bottom print-invisible">
                <ul class="list-unstyle list-inline text-right">
                    <li class="list-inline-item">
                        <a href="#" class="btn text-gray text-hover display-block padding-10 no-mrg-btm" onclick="window.print();">
                            <i class="ti-printer text-info pdd-right-5"></i>
                            <b>Print</b>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{ route('common_auth.invoice.download.pdf',['layout'=> 'superadmin']) }}" class="text-gray text-hover display-block padding-10 no-mrg-btm">
                            <i class="fa fa-file-pdf-o text-danger pdd-right-5"></i>
                            <b>Export PDF</b>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="pdd-horizon-30">
                    <div class="mrg-top-15">
                        <div class="inline-block">
                            <img class="img-responsive" src="{{ asset('assets/images/logo/logo.png') }}" alt="">
                            <address class="pdd-left-10 mrg-top-20">
                                <b class="text-dark">AUS holdings LTD</b><br>
                                <span>20-22 Wenlock Road</span><br>
                                <span>London, England, N1 7GU</span><br>
                                <span><span class="text-bold">Contact:</span> 0754 2987 483</span><br>
                            </address>
                        </div>
                        <div class="pull-right">
                            <h2>
                                <b>
                                    INVOICE:
                                    <a href="#" class="text-primary">{{ $invoice->invoice_number }}</a>
                                </b>
                            </h2>
                        </div>
                    </div>
                    <div class="row mrg-top-20">
                        <div class="col-md-9 col-sm-9">
                            <h3 class="pdd-left-10 mrg-top-10">Invoice To:</h3>
                            <address class="pdd-left-10 mrg-top-10">
                                <b class="text-dark">
                                    <a href="#" class="customer-name text-primary">{{ $invoice->client->name }}</a>
                                </b><br>
                                <span>{{ $invoice->client->contact_number }}</span><br>
                                <span>{{ $invoice->client->address }}</span>
                            </address>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="mrg-top-50">
                                <div class="text-dark text-uppercase inline-block"><b>Invoice No :</b></div>
                                <b><a href="#" class="pull-right text-primary">{{ $invoice->invoice_number }}</a></b>
                            </div>
                            <div class="mrg-top-0">
                                <div class="text-dark text-uppercase inline-block">
                                    <b>Invoice Date :</b>
                                    @php
                                        $date_invoice = new DateTime($invoice->invoice_date);
                                        $expire_date = new DateTime($invoice->expire_date);
                                        $carts = $invoice->invoiceCarts;
                                        $currency = ($invoice->currency == 1) ? '£':($invoice->currency == 2)? '$':'৳';
                                        $total = 0;
                                        $discount = 0;
                                    @endphp
                                </div>
                                <div class="pull-right">{{ $date_invoice->format('d M Y') }}</div>
                            </div>
                            <div class="mrg-top-0">
                                <div class="text-dark text-uppercase inline-block">
                                    <b>Due Date :</b>
                                </div>
                                <div class="pull-right">{{ $expire_date->format('d M Y') }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row mrg-top-20">
                        <div class="col-md-12">
                            <table class="table table-hover table-responsive">
                                <thead>
                                    <tr>
                                        <th>Sl.</th>
                                        <th>Items</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Discount</th>
                                        <th class="text-right">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($carts as $key=>$cart)
                                    <tr>
                                        @php
                                            $total += $cart->price;
                                            $discount += $cart->discount;
                                        @endphp
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $cart->item }}</td>
                                        <td>{{ $cart->quantity }}</td>
                                        <td>{{ $currency." ".$cart->price}}</td>
                                        <td>{{ $currency." ".$cart->discount}}</td>
                                        <td class="text-right">{{ $currency." ".$cart->discount}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="row mrg-top-30">
                                <div class="col-md-4 offset-md-8">
                                    <hr>
                                    <table class="table table-borderless table-responsive">
                                        <tbody>
                                            <tr>
                                                <th class="text-right"><b>Sub Total:</b></th>
                                                <td class="text-right"><b>{{ $currency." ".$total}}</b></td>
                                            </tr>
                                            <tr>
                                                <th class="text-right"><b>Discount:</b></th>
                                                <td class="text-right"><b>{{ $currency." ".$discount}}</b></td>
                                            </tr>
                                            <tr class="text-primary">
                                                <th class="text-right border-top-1">
                                                    <b>Total Amount To Pay:</b>
                                                </th>
                                                <td class="text-right border-top-1"><b>{{ $currency." ".($total-$discount) }}</b></td>
                                            </tr>
                                            <tr class="text-success">
                                                <th class="text-right border-top-1">
                                                    <b>Total Paid:</b>
                                                </th>
                                                <td class="text-right border-top-1"><b>£300.00</b></td>
                                            </tr>
                                            <tr class="text-danger">
                                                <th class="text-right border-top-1">
                                                    <b>Due Amount:</b>
                                                </th>
                                                <td class="text-right border-top-1"><b>£205.00</b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                </div>
                            </div>

                            {{-- Payment Method --}}
                            <div class="row mrg-top-30">
                                <div class="col-md-6 border right">
                                    <hr>
                                    <div class="pdd-vertical-20">
                                        <p class="text-dark">
                                            [[ <b class="text-info">NOTE:</b>
                                            <b>If you choose <span class="text-primary text-capitalize">Online Payment</span> then use the Links Below...</b> ]]
                                        </p>

                                        <div class="row">
                                            <div class="col-xl-12 text-center">
                                                <ul class="list-unstyled list-inline">
                                                    <li class="list-inline-item no-pdd-horizon">
                                                        <a href="javascript:void(0);" class="btn btn-default btn-icon btn-sm m-10">
                                                            <b class="text-bold text-uppercase">Stripe</b>
                                                        </a>
                                                    </li>
                                                    <li class="list-inline-item no-pdd-horizon">
                                                        <a href="javascript:void(0);" class="btn btn-default btn-icon btn-sm m-10">
                                                            <b class="text-bold text-uppercase">Visa</b>
                                                        </a>
                                                    </li>
                                                    <li class="list-inline-item no-pdd-horizon">
                                                        <a href="javascript:void(0);" class="btn btn-default btn-icon btn-sm m-10">
                                                            <b class="text-bold text-uppercase">Master</b>
                                                        </a>
                                                    </li>
                                                    <li class="list-inline-item no-pdd-horizon">
                                                        <a href="javascript:void(0);" class="btn btn-default btn-icon btn-sm m-10">
                                                            <b class="text-bold text-uppercase">bKash</b>
                                                        </a>
                                                    </li>
                                                    <li class="list-inline-item no-pdd-horizon">
                                                        <a href="javascript:void(0);" class="btn btn-default btn-icon btn-sm m-10">
                                                            <b class="text-bold text-uppercase">DBBL</b>
                                                        </a>
                                                    </li>
                                                    <li class="list-inline-item no-pdd-horizon">
                                                        <a href="javascript:void(0);" class="btn btn-default btn-icon btn-sm m-10">
                                                            <b class="text-bold text-uppercase">Rocket</b>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 text-right border left">
                                    <hr>
                                    <div class="pdd-vertical-20">
                                        <p class="text-dark">
                                            [[ <b class="text-info">NOTE:</b>
                                            <b>If you choose <span class="text-primary text-capitalize">bank payment</span> then use the bank details below...</b> ]]
                                        </p>

                                        <address class="pdd-left-10 mrg-top-10">
                                            <b class="text-dark">AUS holdings LTD</b><br>
                                            <span class="text-dark">Bank Of Scotland</span><br>
                                            <span class="text-primary"><span class="text-dark">SC:</span> 80-22-60</span><br>
                                            <span class="text-primary"><span class="text-dark">Account:</span> 18003761</span><br>
                                            <span class="text-primary"><span class="text-dark">Ref:</span> VT-INV-000001</span>
                                        </address>
                                    </div>
                                </div>
                            </div>
                            <div class="row mrg-vertical-20 border top">
                                <div class="col-md-6">
                                    <img class="img-responsive text-opacity mrg-top-0" width="150" src="{{ asset('assets/images/logo/logo.png') }}" alt="">
                                </div>
                                <div class="col-md-6 text-right">
                                    <small>
                                        <b>
                                            <a href="https://www.veechitechnologies.com" target="_blank">www.veechitechnologies.com</a>
                                        </b>
                                    </small><br>
                                    <small><b>Phone:</b> (123) 456-7890</small>
                                    <br>
                                    <small>info@veechitechnologies.com</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

    </script>
@endsection

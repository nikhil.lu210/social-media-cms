@extends('layouts.superadmin.app')

@section('page_title', 'Invoices')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */

    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>All My Invoices</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-block">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-bordered table-lg table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th>Sl.</th>
                                    <th>Invoice No.</th>
                                    <th>Amount</th>
                                    <th>Sent Date</th>
                                    <th>Expire Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($invoices as $sl => $data)
                                    <tr>
                                        <td>
                                            <div class="mrg-top-15">
                                                <span class="text-dark">
                                                    <b>{{ $sl+1 }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-15">
                                                <span>
                                                    <b><a href="{{ route('common_auth.invoice.show', ['layout' => 'superadmin', 'invoice_id' => $data->id]) }}" class="text-info">{{ $data->invoice_number }}</a></b>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-15">
                                                <b class="text-dark font-size-16">{{ $data->payable_amount }}</b>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-15">
                                                @php
                                                    $date = new DateTime($data->created_at);
                                                @endphp
                                               <span>{{ $date->format('d M Y') }}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-15">
                                                @php
                                                    $date = new DateTime($data->invoice_date);
                                                @endphp
                                               <span>{{ $date->format('d M Y') }}</span>
                                            </div>
                                        </td>
                                        <td>
                                            @if($data->invoice_status == 1)
                                                <div class="relative mrg-top-15">
                                                    <span class="status online"> </span>
                                                    <span class="pdd-left-20 text-success"><b>Confirmed</b></span>
                                                </div>
                                            @elseif($data->invoice_status == 0)
                                                <div class="relative mrg-top-15">
                                                    <span class="status away"> </span>
                                                    <span class="pdd-left-20 text-warning"><b>Pending</b></span>
                                                </div>
                                            @elseif($data->invoice_status == -1)
                                                <div class="relative mrg-top-15">
                                                    <span class="status no-disturb"> </span>
                                                    <span class="pdd-left-20 text-danger"><b>Rejected</b></span>
                                                </div>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="mrg-top-10 dropdown">
                                                <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="{{ route('common_auth.invoice.show', ['layout' => 'superadmin', 'invoice_id' => $data->id]) }}">
                                                            <i class="ti-eye pdd-right-10 text-info"></i>
                                                            <span>See Invoice</span>
                                                        </a>
                                                    </li>

                                                    @if ($data->invoice_status == 0)
                                                        <li>
                                                            <a href="{{ route('common_auth.invoice.change.status', ['layout' => 'superadmin', 'invoice_id' => $data->id, 'status' => 1]) }}" onclick="return confirm('Are You Sure Want To Mark This Payment as Paid...?');">
                                                                <i class="ti-check pdd-right-10 text-success"></i>
                                                                <span>Mark As Paid</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ route('common_auth.invoice.change.status', ['layout' => 'superadmin', 'invoice_id' => $data->id, 'status' => -1]) }}" onclick="return confirm('Are You Sure Want To Mark This Payment as Rejected...?');">
                                                                <i class="ti-close pdd-right-10 text-danger"></i>
                                                                <small>Mark As Rejected</small>
                                                            </a>
                                                        </li>
                                                    @elseif ($data->invoice_status == 1)
                                                        <li>
                                                            <a href="{{ route('common_auth.invoice.change.status', ['layout' => 'superadmin', 'invoice_id' => $data->id, 'status' => 0]) }}" onclick="return confirm('Are You Sure Want To Mark This Payment as Pending...?');">
                                                                <i class="ti-exchange-vertical pdd-right-10 text-primary"></i>
                                                                <small>Mark As Pending</small>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ route('common_auth.invoice.change.status', ['layout' => 'superadmin', 'invoice_id' => $data->id, 'status' => -1]) }}" onclick="return confirm('Are You Sure Want To Mark This Payment as Rejected...?');">
                                                                <i class="ti-close pdd-right-10 text-danger"></i>
                                                                <small>Mark As Rejected</small>
                                                            </a>
                                                        </li>
                                                    @elseif ($data->invoice_status == -1)
                                                        <li>
                                                            <a href="{{ route('common_auth.invoice.change.status', ['layout' => 'superadmin', 'invoice_id' => $data->id, 'status' => 1]) }}" onclick="return confirm('Are You Sure Want To Mark This Payment as Paid...?');">
                                                                <i class="ti-check pdd-right-10 text-success"></i>
                                                                <span>Mark As Paid</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ route('common_auth.invoice.change.status', ['layout' => 'superadmin', 'invoice_id' => $data->id, 'status' => 0]) }}" onclick="return confirm('Are You Sure Want To Mark This Payment as Pending...?');">
                                                                <i class="ti-exchange-vertical pdd-right-10 text-primary"></i>
                                                                <small>Mark As Pending</small>
                                                            </a>
                                                        </li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

    </script>
@endsection

@extends('layouts.superadmin.app')

@section('page_title', 'Business | All Businesses')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .password-generate{
            position: absolute;
            right: 35px;
            font-weight: bold;
            text-transform: uppercase;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>Savasaachi Businesses</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-left">Inactive Businesses</h4>

                    <a href="{{ route('superadmin.business.create') }}" class="btn btn-primary btn-sm float-right m-b-0">Add New Business</a>
                </div>
                <hr class="m-t-0">
                <div class="card-block">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">Sl.</th>
                                    <th class="text-center">Business Name</th>
                                    <th class="text-center">Client</th>
                                    <th class="text-center">Package</th>
                                    <th class="text-center">Assigned Date</th>
                                    <th class="text-center">Renewal Date</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($businesses as $item => $business)
                                <tr>
                                    <td>
                                        <div class="m-6 text-center">
                                            <span class="text-dark">
                                                <b>{{ $item+1 }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="m-6 text-center">
                                            <span class="text-dark">
                                                <a href="#" target="_blank" class="text-primary text-bold">{{ $business->name }}</a>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="m-6 text-center">
                                            <span class="text-dark">
                                                <a href="#" target="_blank" class="text-primary text-bold">{{ $business->client->name }}</a>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="m-6 text-center">
                                            <span class="text-dark">
                                                <a href="#" target="_blank" class="text-primary text-bold">{{ $business->package->name }}</a>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="m-6 text-center">
                                            @php
                                                $date = new DateTime($business->created_at);
                                                $d_date = new DateTime($business->updated_at);
                                            @endphp
                                            <span class="text-dark">
                                                <b>{{ $date->format('d M Y') }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="m-6 text-center">
                                            <span class="text-dark">
                                                <b class="text-info">বাদে বউয়ানি হইব</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="m-6 text-center">
                                            <span class="text-dark">
                                                @if($business->status == 1)
                                                    <b class="text-success">Active</b>
                                                @elseif($business->status == -1)
                                                    <b class="text-warning">Left</b>
                                                @else
                                                    <b class="text-danger">Deactivate</b>
                                                @endif
                                            </span>
                                        </div>
                                    </td>

                                    <td>
                                        <div class="dropdown m-6 text-center">
                                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle p-0" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('superadmin.business.show', ['business_id' => $business->id]) }}">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>See Details</span>
                                                    </a>
                                                </li>

                                                @if ($business->status == 1) {{-- Business Active --}}
                                                    <li>
                                                        <a href="{{ route('common_auth.business.change.status', ['layout' => 'superadmin', 'business_id' => $business->id, 'status' => 0]) }}" class="confirmation">
                                                            <i class="ti-close pdd-right-10 text-danger"></i>
                                                            <span>Deactivate</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('common_auth.business.change.status', ['layout' => 'superadmin', 'business_id' => $business->id, 'status' => -1]) }}" class="confirmation">
                                                            <i class="ei-sad pdd-right-10 text-dark"></i>
                                                            <span>Left</span>
                                                        </a>
                                                    </li>
                                                @elseif($business->status == -1) {{-- Business Left --}}
                                                    <li>
                                                        <a href="{{ route('common_auth.business.change.status', ['layout' => 'superadmin', 'business_id' => $business->id, 'status' => 1]) }}" class="confirmation">
                                                            <i class="ti-check pdd-right-10 text-success"></i>
                                                            <span>Activate</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('common_auth.business.change.status', ['layout' => 'superadmin', 'business_id' => $business->id, 'status' => 0]) }}" class="confirmation">
                                                            <i class="ti-close pdd-right-10 text-danger"></i>
                                                            <span>Deactivate</span>
                                                        </a>
                                                    </li>
                                                @else {{-- Business Inactive --}}
                                                    <li>
                                                        <a href="{{ route('common_auth.business.change.status', ['layout' => 'superadmin', 'business_id' => $business->id, 'status' => 1]) }}" class="confirmation">
                                                            <i class="ti-check pdd-right-10 text-success"></i>
                                                            <span>Activate</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('common_auth.business.change.status', ['layout' => 'superadmin', 'business_id' => $business->id, 'status' => -1]) }}" class="confirmation">
                                                            <i class="ei-sad pdd-right-10 text-dark"></i>
                                                            <span>Left</span>
                                                        </a>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

    </script>
@endsection

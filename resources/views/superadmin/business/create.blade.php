@extends('layouts.superadmin.app')

@section('page_title', 'Business | New Business')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .form-control{
            padding: 0.700rem 0.75rem;
        }
        .selectize-input {
            padding: 0.640rem 0.75rem;
        }

        .responsive-image{
            border-radius: 5px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 80px;
            width: 100%;
        }
        .card-block.task-file.p-0 {
            height: 80px;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
        a.deactive{
            cursor: pointer;
        }
        li.social-media-list{
            border: 0px !important;
            border-bottom: 1px solid #e6ecf5 !important;
            margin-bottom: 10px;
        }
        li.social-media-list:last-child{
            border: 0px !important;
            border-bottom: 0px solid #e6ecf5 !important;
            margin-bottom: 0px;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>Add New Business</h4>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('common_auth.business.store', ['layout' => 'superadmin']) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-block p-25">
                        <div class="row justify-content-center m-b-20">
                            <div class="col-lg-4 text-center">
                                <div>
                                    <label for="img-upload" class="pointer">
                                        <img id="img-preview" src="{{ asset('assets/images/others/img-10.jpg') }}"  width="117" alt="">

                                        <span class="btn btn-default display-block no-mrg-btm">Choose Logo<span class="required">*</span></span>
                                        <input class="d-none @error('logo') is-invalid @enderror" type="file" name="logo" accept=".png, .jpg, .jpeg" id="img-upload" value="{{ old('logo') }}">
                                    </label>
                                    @error('logo')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Business Name <span class="required">*</span></label>
                                    <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="Business Name Here" required>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>Business Type <span class="required">*</span></label>
                                    <input type="text" name="type" class="form-control @error('type') is-invalid @enderror" value="{{ old('type') }}" placeholder="Business Type Here" required>

                                    @error('type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>Select Package <span class="required">*</span></label>
                                    <div class="mrg-top-0">
                                        <select class="@error('package_id') is-invalid @enderror" name="package_id" id="selectize-dropdown" required>
                                            <option value="" disabled selected>Select a Package</option>
                                            @foreach ($packages as $package)
                                                <option value="{{ $package->id }}">{{ $package->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    @error('package_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Select Currency <span class="required">*</span></label>
                                    <div class="mrg-top-0">
                                        <select class="@error('currency') is-invalid @enderror" name="currency" id="selectize-dropdown" required>
                                            <option value="" disabled selected>Select Currency</option>
                                            <option value="1">£ GBP</option>
                                            <option value="2">$ USD</option>
                                            <option value="3">৳ BDT</option>
                                        </select>
                                    </div>

                                    @error('currency')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Business Client <span class="required">*</span></label>
                                    <div class="mrg-top-0">
                                        <select class="@error('client_id') is-invalid @enderror" name="client_id" id="selectize-dropdown1" required>
                                            <option value="" disabled selected>Select Client</option>
                                            @foreach ($clients as $client)
                                                <option value="{{ $client->id }}">{{ $client->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    @error('client_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Email <span class="required">*</span></label>
                                    <input autocomplete="off" type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="business@mail.com" required>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Mobile No. <span class="required">*</span></label>
                                    <input autocomplete="off" type="text" name="mobile" class="form-control @error('mobile') is-invalid @enderror" value="{{ old('mobile') }}" placeholder="+44 0800 689 3176" required>

                                    @error('mobile')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Address <span class="required">*</span></label>
                                    <textarea autocomplete="off" name="address" class="form-control @error('address') is-invalid @enderror" placeholder="Business Address Here..." rows="3" required>{{ old('address') }}</textarea>

                                    @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Business Moto</label>
                                    <textarea autocomplete="off" name="moto" class="form-control @error('moto') is-invalid @enderror" placeholder="Business Moto Here..." rows="3">{{ old('moto') }}</textarea>

                                    @error('moto')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Business Tags</label>
                                    <textarea autocomplete="off" name="tags" class="form-control @error('tags') is-invalid @enderror" placeholder="#tag_one #tag_two #tag_three" rows="3">{{ old('tags') }}</textarea>
                                    <small><i>Please type all the Tags using <span class="text-primary text-bold">Hastag (#)</span>.</i> <span class="required">[eg: <span class="text-info">#tag_one #tag_two</span>]</span></small>

                                    @error('tags')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-heading border bottom">
                                        <h4 class="card-title">Social Medias Info</h4>
                                    </div>
                                    <div class="card-block">
                                        <ul class="list-unstyled">
                                            @foreach($socials as $social)
                                            <li class="social-media-list social-selected">
                                                <div class="checkbox checkbox-primary font-size-12">
                                                    <input class="is_input" id="social_media_id_{{ $social->id }}" name="status" type="checkbox">
                                                    <label for="social_media_id_{{ $social->id }}" class="m-b-10 text-bold text-primary">{{ $social->name }}</label>
                                                </div>
                                                <div id="social_div_{{ $social->id }}" class="row d-none">
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                        <input type="hidden" name="auth[{{$social->id}}][type]" value="{{ $social->id }}">
                                                            <label>{{ $social->name }} URL <span class="required">*</span></label>
                                                            <input autocomplete="off" type="url" name="auth[{{$social->id}}][url]" class="form-control removeDis @error('url') is-invalid @enderror" value="{{ old('url') }}" placeholder="https://socialmediaurl.com">

                                                            @error('url')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <label>{{ $social->name }} Username <span class="required">*</span></label>
                                                            <input autocomplete="off" type="text" id="social_media_id_01_username_input" name="auth[{{$social->id}}][username]" class="form-control removeDis @error('username') is-invalid @enderror social_auth_{{ $social->id }}" value="{{ old('username') }}" placeholder="jhondoe" >

                                                            <div class="checkbox checkbox-primary font-size-12 pull-right">
                                                                <input id="media_checkbox_{{ $social->id }}" class="removeDis media_clicked media_checkbox_{{ $social->id }}" type="checkbox">
                                                                <label for="media_checkbox_{{ $social->id }}" class="m-b-10 text-bold text-dark text-uppercase removeDis"><span class="text-primary">Savasaachi</span> Admin</label>
                                                            </div>

                                                            @error('username')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <label>{{ $social->name }} Password <span class="required">*</span></label>
                                                            <input autocomplete="off" type="text" id="social_media_id_01_password_input" name="auth[{{$social->id}}][password]" class="form-control removeDis @error('password') is-invalid @enderror social_auth_{{ $social->id }}" value="{{ old('password') }}" placeholder="123123sdfsdAFSAD@$^&#@">

                                                            <div class="checkbox checkbox-primary font-size-12 pull-right">
                                                                <input id="media_checkbox_pass_{{ $social->id }}" class="removeDis media_clicked media_checkbox_{{ $social->id }}" type="checkbox">
                                                                <label for="media_checkbox_pass_{{ $social->id }}" class="m-b-10 text-bold text-dark text-uppercase removeDis"><span class="text-primary">Savasaachi</span> Admin</label>
                                                            </div>

                                                            @error('password')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            @endforeach

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer border top">
                        <ul class="list-unstyled list-inline pull-left">
                            <li class="list-inline-item">
                                <div class="checkbox checkbox-primary font-size-12">
                                    <input id="status" name="status" type="checkbox" checked required>
                                    <label for="status" class="m-b-0 text-bold text-primary">Activate This Business?</label>
                                </div>
                            </li>
                        </ul>
                        <ul class="list-unstyled list-inline pull-right">
                            <li class="list-inline-item">
                                <button type="submit" class="btn btn-primary btn-sm text-bold">
                                    <i class="ti-save"></i>
                                    Assign As New Business
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- @include('superadmin.system.modals.team.update_modal') --}}
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>

    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#img-preview').css('background-image', 'url('+e.target.result +')');
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').hide();
                    $('#img-preview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img-upload").change(function() {
            readURL(this);
        });
    </script>

    <script>
        // Custom Script Here
        $(document).ready(function(){


            $('.social-selected input.is_input').click(function(){
                var input_id = $(this).attr('id');
                var arr = input_id.split('_');
                var media_str = '#social_media_id_' + arr[arr.length-1];
                var media_list_str = '#social_div_' + arr[arr.length-1];
                var input_disable_str = media_list_str + ' .removeDis';

                var media = $(media_str);
                var media_list = $(media_list_str);
                var input_disable = $(input_disable_str);


                if($(this).is(":checked")){
                    // alert("Checkbox is checked.");
                    media_list.removeClass('d-none');
                    input_disable.removeAttr('disabled', 'true');
                }
                else if($(this).is(":not(:checked)")){
                    // alert("Checkbox is unchecked.");
                    media_list.addClass('d-none');
                    input_disable.attr('disabled', 'true');
                }
            });


            $('.social-selected .media_clicked').click(function(){
                var click = $(this).attr('class');
                click = click.split(' ');
                click = '.' + click[click.length-1];

                var spClick = click.split('_');
                spClick = spClick[spClick.length-1];

                var social_auth = '.social_auth_' + spClick;

                if($(click).is(":checked")){
                    // alert("Checkbox is checked.");
                    $(social_auth).attr('disabled', 'true');
                    $(social_auth).removeAttr('required', 'required');
                }
                else if($(click).is(":not(:checked)")){
                    // alert("Checkbox is unchecked.");
                    $(social_auth).removeAttr('disabled', 'true');
                    $(social_auth).attr('required', 'required');
                }

            });
        });
    </script>
@endsection

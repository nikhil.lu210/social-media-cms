@extends('layouts.superadmin.app')

@section('page_title', 'Business | Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .form-control {
            padding: 0.700rem 0.75rem;
        }
        .form-control:disabled, .form-control[readonly] {
            background-color: #ffffff;
        }
        .media-details-modal input::-moz-selection { /* Code for Firefox */
            color: #ffffff;
            font-weight: bold;
            background: #7e297f;
        }

        .media-details-modal input::selection {
            color: #ffffff;
            font-weight: bold;
            background: #7e297f;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ strtoupper($business->name) }}</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                {{-- Left Side Starts --}}
                <div class="col-md-5">
                    <div class="widget-profile-1 card">
                        <div class="profile border bottom">
                            @if($business->logo != null)
                                <img class="mrg-top-30" src="{{ $business->logo }}" alt="">
                            @else
                                <img class="mrg-top-30" src="{{ asset('assets/images/others/img-10.jpg') }}" alt="">
                            @endif
                            <h4 class="mrg-top-20 no-mrg-btm text-bold">{{ $business->name }}</h4>
                            <p>{{ $business->package->name }}</p>
                        </div>
                        <div class="p-10">
                            <div class="m-t-5 text-center">
                                <ul class="list-unstyled list-inline">
                                    @foreach($business->socialVaults as $social)
                                    <li class="list-inline-item no-pdd-horizon">
                                        <a  href="javascript:void(0);" data-toggle="modal" data-todo="{{ $social }}" data-target="#social_media_details" class="btn btn-default btn-icon btn-sm btn-rounded">
                                            <b class="text-bold">{{ $social->socialMedia->name }}</b>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-heading border bottom">
                            <h4 class="card-title float-left text-bold p-t-3 text-uppercase">Business Details</h4>

                            <a href="{{ route('superadmin.business.edit', ['business_id'=> $business->id]) }}" class="btn btn-primary btn-sm float-right m-b-0 text-bold text-uppercase">Edit</a>
                        </div>
                        <div class="card-block">
                            <table class="table table-lg table-hover table-bordered table-responsive">
                                <tbody>
                                    <tr>
                                        <th style="width: 30%;">Business Name</th>
                                        <td style="width: 70%;">{{ $business->name }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 30%;">Client Name</th>
                                        <td style="width: 70%;">
                                            <a href="#" target="_blank" class="text-semibold">{{ $business->client->name }}</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width: 30%;">Business Moto</th>
                                        <td style="width: 70%;">{{ $business->moto }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 30%;">Business Type</th>
                                        <td style="width: 70%;">{{ $business->type }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 30%;">Package</th>
                                        <td style="width: 70%;">{{ $business->package->name }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 30%;">Currency</th>
                                        @php
                                            $currency = ($business->currency == 1) ? '$ USD' : ($business->currency == 2) ? '£ GBP':'৳ BDT';
                                        @endphp
                                        <td style="width: 70%;">{{ $currency }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 30%;">Email</th>
                                        <td style="width: 70%;">{{ $business->email }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 30%;">Mobile No.</th>
                                        <td style="width: 70%;">{{ $business->mobile }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 30%;">Address</th>
                                        <td style="width: 70%;">{{ $business->address }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 30%;">Tags</th>
                                        <td style="width: 70%;">{{ $business->tags }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 30%;">Status</th>
                                        <td style="width: 70%;">
                                            @if($business->status == 1)
                                                <p class="m-b-0 text-bold text-success">Active</p>
                                            @elseif($business->status == 0)
                                                <p class="m-b-0 text-bold text-danger">Inative</p>
                                            @else
                                                <p class="m-b-0 text-bold text-warning">Left</p>
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{-- Left Side Ends --}}


                {{-- Right Side Starts --}}
                <div class="col-md-7">

                    {{-- Business Overview Starts --}}
                    <div class="card">
                        <div class="card-heading border bottom">
                            <h4 class="card-title text-bold text-center text-uppercase">Business Overview</h4>
                        </div>
                        <div class="card-block">
                            <div class="row">
                                @php
                                    $regDate = new DateTime($business->created_at);
                                    $contents = $business->contents;
                                @endphp
                                <div class="col-md-3 col-sm-6 col-6 border right border-hide-md">
                                    <div class="text-center pdd-vertical-10">
                                        <h2 class="font-primary no-mrg-top">{{ $regDate->format('d M Y') }}</h2>
                                        <p class="no-mrg-btm">Registerd At</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-6 border right border-hide-md">
                                    <div class="text-center pdd-vertical-10">
                                        <h2 class="font-primary no-mrg-top">12 Nov 2019</h2>
                                        {{-- @if (যদি রিনিওয়াল ডেট যায় গিয়া তাইলে এক্সপায়ার্ড এট বইব) --}}
                                            {{-- <p class="no-mrg-btm">Expired At</p> --}}
                                        {{-- @else --}}
                                            <p class="no-mrg-btm">Nenewal At</p>
                                        {{-- @endif --}}
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-6 border right border-hide-md">
                                    <div class="text-center pdd-vertical-10">
                                        <h2 class="font-primary no-mrg-top">{{ count($contents) }}</h2>
                                        <p class="no-mrg-btm">Total Content</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-6">
                                    <div class="text-center pdd-vertical-10">
                                        <h2 class="font-primary no-mrg-top text-primary">$210</h2>
                                        <p class="no-mrg-btm">Total Income</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Business Overview Ends --}}

                    {{-- Invoices Starts --}}
                    <div class="card">
                        <div class="card-heading">
                            <h4 class="card-title float-left text-uppercase text-bold">Business Invoices</h4>

                            <a href="#" class="btn btn-primary btn-sm float-right m-b-0 text-uppercase text-bold">New Invoice</a>
                        </div>
                        <hr class="m-t-0">
                        <div class="card-block p-t-0">
                            <div class="table-overflow">
                                <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Sl.</th>
                                            <th class="text-center">Invoice No</th>
                                            <th class="text-center">Sent Date</th>
                                            <th class="text-center">Due Date</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="m-6 text-center">
                                                    <span class="text-dark">
                                                        <b>01</b>
                                                    </span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="m-6 text-center">
                                                    <span class="text-dark">
                                                        <a href="#" target="_blank" class="text-primary text-bold">SAVA-INV-0001</a>
                                                    </span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="m-6 text-center">
                                                    <span class="text-dark">
                                                        <b>01 Nov 2018</b>
                                                    </span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="m-6 text-center">
                                                    <span class="text-dark">
                                                        <b>01 Nov 2018</b>
                                                    </span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="m-6 text-center">
                                                    <span class="text-dark">
                                                        <b class="text-success">Active</b>
                                                        {{-- <b class="text-danger">01 Dec 2019</b> --}}
                                                    </span>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="dropdown m-6 text-center">
                                                    <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle p-0" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="{{ route('superadmin.business.show', ['business_id' => 1]) }}">
                                                                <i class="ti-eye pdd-right-10 text-info"></i>
                                                                <span>See Details</span>
                                                            </a>
                                                        </li>

                                                        {{-- @if ($teamName->status == 1) --}}
                                                            <li>
                                                                <a href="#" class="confirmation">
                                                                    <i class="ti-close pdd-right-10 text-danger"></i>
                                                                    <span>Deactivate</span>
                                                                </a>
                                                            </li>
                                                        {{-- @else --}}
                                                            {{-- <li>
                                                                <a href="#" class="confirmation">
                                                                    <i class="ti-check pdd-right-10 text-success"></i>
                                                                    <span>Activate</span>
                                                                </a>
                                                            </li> --}}
                                                        {{-- @endif --}}
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- Invoices Ends --}}

                    {{-- Contents Starts --}}
                    <div class="card">
                        <div class="card-heading">
                            <h4 class="card-title text-bold text-uppercase">Business Contents</h4>
                        </div>
                        <hr class="m-t-0">
                        <div class="card-block p-t-0">
                            <div class="table-overflow">
                                <table id="dt-opt1" class="table table-lg table-hover table-bordered table-responsive">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Sl.</th>
                                            <th class="text-center">Content</th>
                                            <th class="text-center">Date</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($contents as $sl => $content)
                                        <tr>
                                            <td>
                                                <div class="m-6 text-center">
                                                    <span class="text-dark">
                                                        <b>{{ $sl+1 }}</b>
                                                    </span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="m-6 text-center">
                                                    <span class="text-dark">
                                                        <a href="{{ route('superadmin.content.details', ['content_id' => $content->id]) }}" target="_blank" class="text-primary text-bold">{!! substr($content->post_caption, 0, 35) !!}</a>
                                                    </span>
                                                </div>
                                            </td>
                                            <td>
                                                @php
                                                    $conDate = new DateTime($content->date);
                                                @endphp
                                                <div class="m-6 text-center">
                                                    <span class="text-dark">
                                                        <b>{{ $conDate->format('d M Y') }}</b>
                                                    </span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="m-6 text-center">
                                                    <span class="text-dark">
                                                        @if($content->content_status == 1)
                                                            @if($content->post_caption != null && !$content->contentFiles->isEmpty())
                                                                <b class="text-dark">Awaiting Feedback</b>
                                                            @elseif($content->post_caption != null && $content->contentFiles->isEmpty())
                                                                <b class="text-info">Written</b>
                                                            @elseif($content->post_caption == null && $content->contentFiles->isEmpty())
                                                                <b class="text-info">Designed</b>
                                                            @endif
                                                        @elseif($content->content_status == 2)
                                                            <b class="text-warning">In Proccess</b>
                                                        @elseif($content->content_status == 3)
                                                            <b class="text-success">Completed</b>
                                                        @endif
                                                    </span>
                                                </div>
                                            </td>

                                            <td class="text-center">
                                                <a href="{{ route('superadmin.content.details', ['content_id' => $content->id]) }}" class="btn btn-primary btn-xs" target="_blank">
                                                    <i class="ti-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- Contents Ends --}}
                </div>
                {{-- Right Side Ends --}}
            </div>
        </div>
    </div>

    @include('superadmin.business.modals.social_media')
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        (function ($) {
            'use strict';

            $('#dt-opt1').DataTable();

        })(jQuery);
    </script>
    <script>
        // Custom Script Here
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#img-preview').css('background-image', 'url('+e.target.result +')');
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').hide();
                    $('#img-preview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img-upload").change(function() {
            readURL(this);
        });
    </script>

    <script>
        $('#social_media_details').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var data = button.data('todo');
            var modal = $(this);

            // console.log(data);
            modal.find('.modal-dialog #social_name').html(data.social_media.name);
            modal.find('.modal-dialog #copyURL').val(data.url);
            if(data.email == null && data.username != null){
                modal.find('.modal-dialog #copyUsername').attr('hidden', false);
                modal.find('.modal-dialog #copyPassword').attr('hidden', false);
                modal.find('.modal-dialog #copyUsername').val(data.username);
            }
            else if(data.email != null && data.username == null){
                modal.find('.modal-dialog #copyUsername').attr('hidden', false);
                modal.find('.modal-dialog #copyPassword').attr('hidden', false);
                modal.find('.modal-dialog #copyUsername').val(data.email);
            }
            else{
                modal.find('.modal-dialog #copyUsername').attr('hidden', true);
                modal.find('.modal-dialog #copyPassword').attr('hidden', true);
            }

            modal.find('.modal-dialog #copyPassword').val(data.password);

        });
    </script>

    <script>
        $(document).ready(function() {
            $('.js-tooltip').tooltip();

            $('.js-copy').click(function() {
                var text = $(this).val();
                var el = $(this);
                copyToClipboard(text, el);
            });
        });

        function copyToClipboard(text, el) {
            var copyTest = document.queryCommandSupported('copy');
            var elOriginalText = el.attr('data-original-title');

            if (copyTest === true) {
                var copyTextArea = document.createElement("textarea");
                copyTextArea.value = text;
                document.body.appendChild(copyTextArea);
                copyTextArea.select();
                try {
                    var successful = document.execCommand('copy');
                    var msg = successful ? 'Copied!' : 'Whoops, not copied!';
                    el.attr('data-original-title', msg).tooltip('show');
                } catch (err) {
                    console.log('Oops, unable to copy');
                }
                document.body.removeChild(copyTextArea);
                el.attr('data-original-title', elOriginalText);
            } else {
                // Fallback if browser doesn't support .execCommand('copy')
                window.prompt("Copy to clipboard: Ctrl+C or Command+C, Enter", text);
            }
        }
    </script>
@endsection

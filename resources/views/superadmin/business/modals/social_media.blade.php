{{-- Upload File Modal --}}
<div class="modal fade" id="social_media_details">
    {{-- <div class="modal-dialog modal-lg" role="document"> --}}
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="pull-left"><b><span id="social_name">social_media_name</span> Details</b></h4>
                <button class="btn btn-default btn-icon btn-rounded p-l-8 p-r-7 p-t-6 p-b-4" data-dismiss="modal">
                    <i class="ti-close"></i>
                </button>
            </div>
            <div class="modal-body media-details-modal">
                <div class="form-group">
                    <label class="text-bold">Media URL</label>
                    <input autocomplete="off" type="text" id="copyURL" name="url" class="form-control js-copy" value="media_url" placeholder="https://www.facebook.com" readonly data-toggle="tooltip" data-placement="top" data-html="true" title="<b>Click here to Copy</b>">
                </div>
                <div class="form-group">
                    <label class="text-bold">Username / Email</label>
                    <input autocomplete="off" type="text" id="copyUsername" name="username" class="form-control js-copy" value="media_username" placeholder="jhondoe" readonly data-toggle="tooltip" data-placement="top" data-html="true" title="<b>Click here to Copy</b>">
                </div>
                <div class="form-group">
                    <label class="text-bold">Password</label>
                    <input autocomplete="off" type="text" id="copyPassword" name="password" class="form-control js-copy" value="media_password" placeholder="@%#&AFSDF1432" readonly data-toggle="tooltip" data-placement="top" data-html="true" title="<b>Click here to Copy</b>">
                </div>
            </div>
        </div>
    </div>
</div>
